/*
 * Copyright (C) 2019 PPA-Core.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DEF_SHADOWFANG_H
#define DEF_SHADOWFANG_H

constexpr auto DataHeader = "SK";
constexpr auto SKScriptName = "instance_shadowfang_keep";

enum Data
{
    DATA_BARON_ASHBURY = 0,
    DATA_BARON_SILVERLAINE,
    DATA_COMMANDER_SPRINGVALE,
    DATA_LORD_WALDEN,
    DATA_LORD_GODFREY,

    MAX_ENCOUNTER,

    TEAM_IN_INSTANCE
};

enum ShadowfangKeepBoss
{
   BOSS_BARON_ASHBURY                   = 46962,
   BOSS_BARON_SILVERLAINE               =  3887,
   BOSS_COMMANDER_SPRINGVALE            =  4278,
   BOSS_LORD_GODFREY                    = 46964,
   BOSS_LORD_WALDEN                     = 46963,
};

enum Npcs
{
   NPC_TORMENTED_OFFICER                = 50615,
   NPC_WAILING_GUARDSMAN                = 50613,
   NPC_DREAD_SCRYER                     = 47141,
   NPC_DESECRATION_TR                   = 50503,
   NPC_TOXIN_TRIGGER                    = 50439,
   NPC_DEATHSTALKER_COMMANDER_BELMONT   = 47293, // H
   NPC_PACKLEADER_IVAR_BLOODFANG        = 47006, // A
   NPC_VETERAN_FORSAKEN_TROOPER         = 47030, // H
   NPC_FORSAKEN_BLIGHTSPREADER          = 47031, // H
   NPC_BLOODFANG_BERSERKER              = 47027, // A
};

enum GameObjectIds
{
    GO_BARON_ASHBURY_DOOR               = 18895,
    GO_LORD_GODFREY_DOOR                = 18971,
    GO_LORD_WALDEN_DOOR                 = 18972
};

enum Achievements
{
   ACHIEVEMENT_TO_THE_GROUND            = 5504,
};


#endif
