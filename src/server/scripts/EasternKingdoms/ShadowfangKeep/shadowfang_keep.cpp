/*
 * Copyright (C) 2019 PPA-Core.
 * Copyright (C) 2008-2018 TrinityCore <https://www.trinitycore.org/>
 * Copyright (C) 2006-2009 ScriptDev2 <https://scriptdev2.svn.sourceforge.net/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/* ScriptData
SDName: Shadowfang_Keep
SD%Complete: 0%
SDComment:
SDCategory: Shadowfang Keep
EndScriptData */

/* ContentData

EndContentData */

#include "shadowfang_keep.h"
#include "GameObject.h"
#include "GameObjectAI.h"
#include "InstanceScript.h"
#include "ScriptedGossip.h"
#include "ScriptMgr.h"

enum eShadowfangKeep
{

    EVENTS=1000,
    EVENT_CHECK_PLAYER,

};


// 51400
class npc_haunted_stable_hand : public CreatureScript
{
public:
    npc_haunted_stable_hand() : CreatureScript("npc_haunted_stable_hand") { }

    bool OnGossipSelect(Player* player, Creature* /*creature*/, uint32 Sender, uint32 action) override
    {
        player->PlayerTalkClass->ClearMenus();

        if (Sender != GOSSIP_SENDER_MAIN)
            return true;
        if (!player->getAttackers().empty())
            return true;

        switch (action)
        {
        case GOSSIP_ACTION_INFO_DEF + 1:
            player->TeleportTo(33, -225.70f, 2269.67f, 74.999f, 2.76f); // to silverlaine
            CloseGossipMenuFor(player);
            break;
        case GOSSIP_ACTION_INFO_DEF + 2:
            player->TeleportTo(33, -260.66f, 2246.97f, 100.89f, 2.43f); // to springvale
            CloseGossipMenuFor(player);
            break;
        case GOSSIP_ACTION_INFO_DEF + 3:
            player->TeleportTo(33, -171.28f, 2182.020f, 129.255f, 5.92f); // to walden 
            CloseGossipMenuFor(player);
        case GOSSIP_ACTION_INFO_DEF + 4:
            player->TeleportTo(33, -141.59f, 2182.92f, 155.68f, 5.42f); // to godfrey 
            CloseGossipMenuFor(player);
            break;
        }
        return true;
    }

    bool OnGossipHello(Player* player, Creature* creature) override
    {
        InstanceScript* instance = creature->GetInstanceScript();

        if (instance && instance->GetBossState(DATA_BARON_ASHBURY) == DONE)
            AddGossipItemFor(player, 12669, 0, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1);
        if (instance && instance->GetBossState(DATA_BARON_SILVERLAINE) == DONE)
            AddGossipItemFor(player, 12669, 1, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);
        if (instance && instance->GetBossState(DATA_COMMANDER_SPRINGVALE) == DONE)
            AddGossipItemFor(player, 12669, 2, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 3);
        if (instance && instance->GetBossState(DATA_LORD_WALDEN) == DONE)
            AddGossipItemFor(player, 12669, 3, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 4);

        SendGossipMenuFor(player, 2475, creature->GetGUID());
        return true;
    }



};

// 18971
class go_arugals_lair_18971 : public GameObjectScript
{
public:
    go_arugals_lair_18971() : GameObjectScript("go_arugals_lair_18971") {}

    struct go_arugals_lair_18971AI : public GameObjectAI
    {
        go_arugals_lair_18971AI(GameObject* go) : GameObjectAI(go) { }

        EventMap    _events;

        void Reset() override
        {
            _events.RescheduleEvent(EVENT_CHECK_PLAYER, 1000);
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_CHECK_PLAYER:
                    {
                        if (go->GetGoState() == GO_STATE_READY) // closed
                        {
                            if (Creature* godfrey = GetGodfrey())
                            {
                                if (godfrey->isDead()) // if godfrey is dead the door has to be open..
                                    go->SetGoState(GO_STATE_ACTIVE); // open
                                else if (Player* player = go->FindNearestPlayer(3.0f, true)) // sometimes the smart_scripts has wrong open state.. 
                                    go->SetGoState(GO_STATE_ACTIVE); // open
                            }
                        }
                        _events.ScheduleEvent(EVENT_CHECK_PLAYER, 2500);
                        break;
                    }
                }
            }
        }

        Creature* GetGodfrey()
        {
            if (InstanceScript* instance = go->GetInstanceScript())
                return go->GetMap()->GetCreature(instance->GetGuidData(DATA_LORD_GODFREY));
            return nullptr;
        }
    };

    GameObjectAI* GetAI(GameObject* go) const override
    {
        return new go_arugals_lair_18971AI(go);
    }
};



void AddSC_shadowfang_keep()
{
   new npc_haunted_stable_hand();
   new go_arugals_lair_18971();

}
