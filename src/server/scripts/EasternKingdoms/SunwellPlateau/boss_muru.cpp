/*
 * Copyright (C) 2008-2018 TrinityCore <https://www.trinitycore.org/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptMgr.h"
#include "InstanceScript.h"
#include "ObjectAccessor.h"
#include "ScriptedCreature.h"
#include "SpellAuraEffects.h"
#include "SpellScript.h"
#include "sunwell_plateau.h"

enum Spells
{
    SPELL_SUMMON_BLOOD_ELVES_SCRIPT             = 46050,
    SPELL_SUMMON_BLOOD_ELVES_PERIODIC           = 46041,
    SPELL_SUMMON_DARK_FIEND_0                   = 46000,
    SPELL_SUMMON_DARK_FIEND_1                   = 46001,
    SPELL_SUMMON_DARK_FIEND_2                   = 46002,
    SPELL_SUMMON_DARK_FIEND_3                   = 46003,
    SPELL_SUMMON_DARK_FIEND_4                   = 46004,
    SPELL_SUMMON_DARK_FIEND_5                   = 46005,
    SPELL_SUMMON_DARK_FIEND_6                   = 46006,
    SPELL_SUMMON_DARK_FIEND_7                   = 46007,
    SPELL_SUMMON_BERSERKER                      = 46037,
    SPELL_SUMMON_BERSERKER_2                    = 46040,
    SPELL_SUMMON_FURY_MAGE                      = 46038,
    SPELL_SUMMON_FURY_MAGE_2                    = 46039,

    // Myruu's Portal Target
    SPELL_TRANSFORM_VISUAL_MISSILE              = 46205,
    TRANSFORM_VISUAL_MISSILE_1                  = 46208,
    TRANSFORM_VISUAL_MISSILE_2                  = 46178,

    //Dark Fiend Spells
    SPELL_DARKFIEND_VISUAL                      = 45936,
};

enum Misc
{
    MAX_VOID_SPAWNS                             = 6,
    MAX_SUMMON_BLOOD_ELVES                      = 4,
    MAX_SUMMON_DARK_FIEND                       = 8
};

uint32 const SummonDarkFiendSpells[MAX_SUMMON_DARK_FIEND] =
{
    SPELL_SUMMON_DARK_FIEND_0,
    SPELL_SUMMON_DARK_FIEND_1,
    SPELL_SUMMON_DARK_FIEND_2,
    SPELL_SUMMON_DARK_FIEND_3,
    SPELL_SUMMON_DARK_FIEND_4,
    SPELL_SUMMON_DARK_FIEND_5,
    SPELL_SUMMON_DARK_FIEND_6,
    SPELL_SUMMON_DARK_FIEND_7
};

uint32 const SummonBloodElvesSpells[MAX_SUMMON_BLOOD_ELVES] =
{
    SPELL_SUMMON_BERSERKER,
    SPELL_SUMMON_BERSERKER_2,
    SPELL_SUMMON_FURY_MAGE,
    SPELL_SUMMON_FURY_MAGE_2
};

class spell_summon_blood_elves_script : SpellScriptLoader
{
    public:
        spell_summon_blood_elves_script() : SpellScriptLoader("spell_summon_blood_elves_script") { }

        class spell_summon_blood_elves_script_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_summon_blood_elves_script_SpellScript);

            bool Validate(SpellInfo const* /*spell*/) override
            {
                return ValidateSpellInfo(SummonBloodElvesSpells);
            }

            void HandleScript(SpellEffIndex /*effIndex*/)
            {
                for (uint8 i = 0; i < MAX_SUMMON_BLOOD_ELVES; ++i)
                    GetCaster()->CastSpell(nullptr, SummonBloodElvesSpells[urand(0,3)], true);
            }

            void Register() override
            {
                OnEffectHitTarget += SpellEffectFn(spell_summon_blood_elves_script_SpellScript::HandleScript, EFFECT_0, SPELL_EFFECT_SCRIPT_EFFECT);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_summon_blood_elves_script_SpellScript();
        }
};

class spell_muru_darkness : SpellScriptLoader
{
    public:
        spell_muru_darkness() : SpellScriptLoader("spell_muru_darkness") { }

        class spell_muru_darkness_SpellScript : public SpellScript
        {
            PrepareSpellScript(spell_muru_darkness_SpellScript);

            bool Validate(SpellInfo const* /*spell*/) override
            {
                return ValidateSpellInfo(SummonDarkFiendSpells);
            }

            void HandleAfterCast()
            {
                for (uint8 i = 0; i < MAX_SUMMON_DARK_FIEND; ++i)
                    GetCaster()->CastSpell(nullptr, SummonDarkFiendSpells[i], true);
            }

            void Register() override
            {
                AfterCast += SpellCastFn(spell_muru_darkness_SpellScript::HandleAfterCast);
            }
        };

        SpellScript* GetSpellScript() const override
        {
            return new spell_muru_darkness_SpellScript();
        }
};

class spell_dark_fiend_skin : public SpellScriptLoader
{
    public:
        spell_dark_fiend_skin() : SpellScriptLoader("spell_dark_fiend_skin") { }

        class spell_dark_fiend_skin_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_dark_fiend_skin_AuraScript);

            void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (GetTargetApplication()->GetRemoveMode() != AURA_REMOVE_BY_ENEMY_SPELL)
                    return;

                if (Creature* target = GetTarget()->ToCreature())
                {
                    target->SetReactState(REACT_PASSIVE);
                    target->AttackStop();
                    target->StopMoving();
                    target->CastSpell(target, SPELL_DARKFIEND_VISUAL, true);
                    target->DespawnOrUnsummon(3000);
                }
            }

            void Register() override
            {
                AfterEffectRemove += AuraEffectRemoveFn(spell_dark_fiend_skin_AuraScript::OnRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_dark_fiend_skin_AuraScript();
        }
};

class spell_transform_visual_missile_periodic : public SpellScriptLoader
{
    public:
        spell_transform_visual_missile_periodic() : SpellScriptLoader("spell_transform_visual_missile_periodic") { }

        class spell_transform_visual_missile_periodic_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_transform_visual_missile_periodic_AuraScript);

            void OnPeriodic(AuraEffect const* /*aurEff*/)
            {
                GetTarget()->CastSpell(nullptr, RAND(TRANSFORM_VISUAL_MISSILE_1, TRANSFORM_VISUAL_MISSILE_2), true);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_transform_visual_missile_periodic_AuraScript::OnPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_transform_visual_missile_periodic_AuraScript();
        }
};

class spell_summon_blood_elves_periodic : public SpellScriptLoader
{
    public:
        spell_summon_blood_elves_periodic() : SpellScriptLoader("spell_summon_blood_elves_periodic") { }

        class spell_summon_blood_elves_periodic_AuraScript : public AuraScript
        {
            PrepareAuraScript(spell_summon_blood_elves_periodic_AuraScript);

            void OnPeriodic(AuraEffect const* /*aurEff*/)
            {
                GetTarget()->CastSpell(nullptr, SPELL_SUMMON_BLOOD_ELVES_SCRIPT, true);
            }

            void Register() override
            {
                OnEffectPeriodic += AuraEffectPeriodicFn(spell_summon_blood_elves_periodic_AuraScript::OnPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new spell_summon_blood_elves_periodic_AuraScript();
        }
};

void AddSC_boss_muru()
{
    new spell_summon_blood_elves_script();
    new spell_muru_darkness();
    new spell_dark_fiend_skin();
    new spell_transform_visual_missile_periodic();
    new spell_summon_blood_elves_periodic();
}
