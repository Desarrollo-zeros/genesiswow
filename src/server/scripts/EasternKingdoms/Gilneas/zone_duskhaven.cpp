/*
 * Copyright (C) 2019 PPA-Core.
 * Copyright (C) 2011-2016 ArkCORE <http://www.arkania.net/>
 * Copyright (C) 2008-2014 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2006-2009 ScriptDev2 <https://scriptdev2.svn.sourceforge.net/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "CombatAI.h"
#include "CreatureTextMgr.h"
#include "GameObject.h"
#include "GameObjectAI.h"
#include "MoveSplineInit.h"
#include "MotionMaster.h"
#include "ObjectAccessor.h"
#include "PassiveAI.h"
#include "Player.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedEscortAI.h"
#include "ScriptedFollowerAI.h"
#include "SpellAuraEffects.h"
#include "SpellMgr.h"
#include "SpellScript.h"
#include "TemporarySummon.h"
#include "Vehicle.h"
#include "WorldSession.h"
#include "zone_gilneas.h"

enum eDuskHaven
{
    NPC_GENERIC_TRIGGER_LAB_MP                  = 35374, // target on ship
    NPC_LORD_GODFREY_36170                      = 36170,
    NPC_TRIGGER                                 = 36198,
    NPC_HORRID_ABOMINATION                      = 36231,
    NPC_QUEST_14348_KILL_CREDIT                 = 36233,
    NPC_FORSAKEN_CATAPULT                       = 36283,
    NPC_GENERIC_TRIGGER_LAB_AOI                 = 36286, // target on land
    NPC_CYNTIA_CREDIT                           = 36287,
    NPC_JAMES_CREDIT                            = 36288,
    NPC_ASHLEY_CREDIT                           = 36289,
    NPC_FORSAKEN_MACHINIST                      = 36292,
    NPC_DARK_RANGER_THYALA                      = 36312,
    NPC_LORD_GODFREY_36330                      = 36330,
    NPC_KRENNAN_ARANAS_36331                    = 36331,
    NPC_KING_GENN_GREYMANE_36332                = 36332,
    NPC_MASTIFF                                 = 36405,
    NPC_DROWNING_WATCHMANN_CREDIT               = 36450,
    NPC_PRINCE_LIAM_GREYMANE                    = 36451,
    NPC_LORNA_CROWLEY                           = 36457,
    NPC_GRANDMA_WAHL                            = 36458,
    NPC_CAT_FELIX                               = 36459,
    NPC_LUCIUS                                  = 36461,
    NPC_MOUNTAIN_HORSE_36555                    = 36555,
    NPC_SWIFT_MOUNTAIN_HORSE                    = 36741,
    NPC_LORD_DARIUS_CROWLEY                     = 37195,
    NPC_ENSLAVED_VILLAGER                       = 37694,
    NPC_KOROTH                                  = 37808,
    NPC_LORD_GODFREY                            = 37875,
    NPC_DARK_SCOUT                              = 37953,
    NPC_HARNESS_38755                           = 38755,
    NPC_HARNESS_43336                           = 43336,
    NPC_CARRIAGE_43337                          = 43337,
    NPC_STAGECOACH_CARRIAGE                     = 44928,
    NPC_LORNA_CRAWLEY                           = 51409,

    GO_BALL_AND_CHAIN                           = 201775,

    QUEST_INVASION                              = 14321,
    QUEST_LAST_CHANCE_AT_HUMANITY               = 14375,
    QUEST_LAST_STAND                            = 14222,
    QUEST_KILL_OR_BE_KILLED                     = 14336,
    QUEST_YOU_CANT_TAKE_EM_ALONE                = 14348,
    QUEST_SAVE_THE_CHILDREN                     = 14368,
    QUEST_LEADER_OF_THE_PACK                    = 14386,
    QUEST_GASPING_FOR_BREATH                    = 14395,
    QUEST_AS_THE_LAND_SHATTERS                  = 14396,
    QUEST_GRANDMAS_CAT                          = 14401,
    QUEST_TO_GREYMANE_MANOR                     = 14465,
    QUEST_THE_KINGS_OBSERVATORY                 = 14466,
    QUEST_ALAS_GILNEAS                          = 14467,
    QUEST_EXODUS                                = 24438,
    QUEST_INTRODUCTIONS_ARE_IN_ORDER            = 24472,
    QUEST_STORMGLEN                             = 24483,
    QUEST_LIBERATION_DAY                        = 24575,
    QUEST_BETRAYAL_AT_TEMPESTS_REACH            = 24592,
    QUEST_LOSING_YOUR_TAIL                      = 24616,
    QUEST_AT_OUR_DOORSTEP                       = 24627,
    QUEST_PUSH_THEM_OUT                         = 24676,
    QUEST_FLANK_THE_FORSAKEN                    = 24677,
    QUEST_THE_HUNGRY_ETTIN                      = 14416,

    SPELL_RANDOM_POINT_POISON                   = 42266,
    SPELL_RANDOM_POINT_BONE                     = 42267,
    SPELL_RANDOM_POINT_BONE_2                   = 42274,
    SPELL_SELF_ROOT                             = 42716,
    SPELL_CORPSE_EXPLOSION                      = 43999,
    SPELL_PARACHUTE                             = 45472,
    SPELL_DANS_EJECT_ALL_PASSENGERS             = 51254,
    SPELL_FORCE_REACTION_1                      = 61899,
    SPELL_LAUNCH3                               = 66227,
    SPELL_LAUNCH2                               = 66251,
    SPELL_FORCECAST_SUMMON_FORSAKEN_ASSASSIN    = 68492,
    SPELL_BARREL_KEG_PLACED                     = 68555,
    SPELL_ABOMINATION_KILL_ME                   = 68558,
    SPELL_FIERY_BOULDER                         = 68591,
    SPELL_HORRID_ABOMINATION_EXPLOSION          = 68560,
    SPELL_CURSE_OF_THE_WORGEN                   = 68630,
    SPELL_LAUNCH1                               = 68659,
    SPELL_RESCUE_DROWNING_WATCHMANN             = 68735,
    SPELL_SAVE_DROWNING_MILITIA_EFFECT          = 68737,
    SPELL_EXIT_VEHICLE                          = 68741,
    SPELL_ROUND_UP_HORSE                        = 68903,
    SPELL_ROPE_IN_HORSE                         = 68908,
    SPELL_MOUNTAIN_HORSE_CREDIT                 = 68917,
    SPELL_ROPE_CHANNEL                          = 68940,
    SPELL_CATACLYSM_1                           = 68953,
    SPELL_FORCECAST_CATACLYSM_I                 = 69027,
    SPELL_BARREL_KEG                            = 69094,
    SPELL_IN_STOCKS                             = 69169,
    SPELL_FORCECAST_SUMMON_SWIFT_MOUNTAIN_HORSE = 69256,
    SPELL_FORCECAST_GILNEAS_TELESCOPE           = 69258,
    SPELL_STEALTH_70456                         = 70456,
    SPELL_FREEZING_TRAP_EFFECT                  = 70794,
    SPELL_AIMED_SHOOT                           = 70796,
    SPELL_RIDE_VEHICLE_72764                    = 72764,
    SPELL_SUMMON_CARRIAGE                       = 72767,
    SPELL_THROW_BOULDER                         = 72768,
    SPELL_LAST_STAND_COMPLETE                   = 72799,
    SPELL_CATACLYSM_3                           = 80133,
    SPELL_CATACLYSM_2                           = 80134,
    SPELL_UPDATE_BIND_TO_GREYMANE_MANOR         = 82892,
    SPELL_GENERIC_QUEST_INVISIBLE_DETECTION_10  = 84481,
    SPELL_UPDATE_ZONE_AURAS                     = 89180,
    SPELL_FADE_BACK                             = 94053,
    SPELL_RIDE_VEHICLE                          = 94654,
    SPELL_FORCECAST_UPDATE_ZONE_AURAS           = 94828,
    SPELL_LAUNCH4                               = 96185,
    SPELL_KNEEL                                 = 68442,

    SPELL_PHASE_QUEST_ZONE_SPECIFIC_06 = 68481, // 181
    SPELL_PHASE_QUEST_ZONE_SPECIFIC_07 = 68482, // 182
    SPELL_PHASE_QUEST_ZONE_SPECIFIC_08 = 68483, // 183
    SPELL_PHASE_QUEST_ZONE_SPECIFIC_09 = 69077, // 184
    SPELL_PHASE_QUEST_ZONE_SPECIFIC_10 = 69078, // 185
    SPELL_PHASE_QUEST_ZONE_SPECIFIC_11 = 69484, // 186
    SPELL_PHASE_QUEST_ZONE_SPECIFIC_12 = 69485, // 187
    SPELL_PHASE_QUEST_ZONE_SPECIFIC_19 = 74096, // 194

};

// start in duskhaven at phase 169, neutral vision..
// 36331
class npc_krennan_aranas_36331 : public CreatureScript
{
public:
    npc_krennan_aranas_36331() : CreatureScript("npc_krennan_aranas_36331") { }

    struct npc_krennan_aranas_36331AI : public ScriptedAI
    {
        npc_krennan_aranas_36331AI(Creature* creature) : ScriptedAI(creature) { Initialize(); }

        EventMap    _events;
        bool        _videoStarted;
        ObjectGuid  _playerGUID;
        ObjectGuid  _kingGUID;
        ObjectGuid  _godfreyGUID;

        void Initialize()
        {
            _videoStarted = false;
            _playerGUID = ObjectGuid::Empty;
        }

        void Reset() override
        {
            _kingGUID = ObjectGuid::Empty;
            _godfreyGUID = ObjectGuid::Empty;
            _events.RescheduleEvent(EVENT_CHECK_ARRIVEL_PLAYER, 1000);
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_CHECK_ARRIVEL_PLAYER:
                {
                    CheckVideoMember();
                    if (!_videoStarted)
                        if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        {
                            // when a gm teleport near this posion, we teleport him into stock and start video..
                            if (!player->HasAura(SPELL_CURSE_OF_THE_WORGEN))
                            {
                                float x1 = abs(player->GetPosition().GetPositionX() - -1818.4f);
                                float y1 = abs(player->GetPosition().GetPositionY() - 2294.25f);
                                if ((x1 > 2.f && x1 < 10.f) || (y1 > 2.f && y1 < 10.f))
                                    player->CastSpell(player, SPELL_CURSE_OF_THE_WORGEN, true);
                                player->CastSpell(player, SPELL_IN_STOCKS, true);
                                player->CastSpell(player, SPELL_SELF_ROOT, true);
                                player->SetFlag(UNIT_FIELD_FLAGS_2, UNIT_FLAG2_DISABLE_TURN);
                            }
                            _videoStarted = true;
                            _events.ScheduleEvent(EVENT_TALK_PART_00, 4000);
                            return;
                        }

                    _videoStarted = false;
                    _playerGUID = ObjectGuid::Empty;
                    _events.ScheduleEvent(EVENT_CHECK_ARRIVEL_PLAYER, 1000);
                    break;
                }
                case EVENT_TALK_PART_00:
                {
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        Talk(0, player);
                    if (Creature* king = ObjectAccessor::GetCreature(*me, _kingGUID))
                        king->RemoveFlag(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_QUESTGIVER);

                    _events.ScheduleEvent(EVENT_TALK_PART_01, 14000);
                    break;
                }
                case EVENT_TALK_PART_01:
                {
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        if (Creature* lord = ObjectAccessor::GetCreature(*me, _godfreyGUID))
                        {
                            player->CastSpell(player, SPELL_CATACLYSM_1);
                            lord->AI()->Talk(0, player);
                        }
                    _events.ScheduleEvent(EVENT_TALK_PART_02, 8000);
                    break;
                }
                case EVENT_TALK_PART_02:
                {
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        if (Creature* king = ObjectAccessor::GetCreature(*me, _kingGUID))
                        {
                            player->CastSpell(player, SPELL_CATACLYSM_2);
                            king->AI()->Talk(0, player);
                        }
                    _events.ScheduleEvent(EVENT_TALK_PART_03, 9000);
                    break;
                }
                case EVENT_TALK_PART_03:
                {
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        if (Creature* king = ObjectAccessor::GetCreature(*me, _kingGUID))
                        {
                            player->CastSpell(player, SPELL_CATACLYSM_3);
                            king->AI()->Talk(1, player);
                        }
                    _events.ScheduleEvent(EVENT_TALK_PART_04, 8000);
                    break;
                }
                case EVENT_TALK_PART_04:
                {
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                    {
                        //player->CastSpell(player, SPELL_LAST_STAND_COMPLETE_2);
                    }
                    if (Creature* king = ObjectAccessor::GetCreature(*me, _kingGUID))
                        king->SetFlag(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_QUESTGIVER);

                    _events.ScheduleEvent(EVENT_COOLDOWN_00, 30000);
                    break;
                }
                case EVENT_COOLDOWN_00:
                {
                    _videoStarted = false;
                    _playerGUID = ObjectGuid::Empty;
                    _events.ScheduleEvent(EVENT_CHECK_ARRIVEL_PLAYER, 1000);
                    break;
                }
                }
            }
        }

        void CheckVideoMember()
        {
            if (!_kingGUID)
                if (Creature* king = me->FindNearestCreature(NPC_KING_GENN_GREYMANE_36332, 25.0f))
                    _kingGUID = king->GetGUID();
            if (!_godfreyGUID)
                if (Creature* lord = me->FindNearestCreature(NPC_LORD_GODFREY_36330, 25.0f))
                    _godfreyGUID = lord->GetGUID();

            if (_videoStarted)
                return;

            uint32 zoneId = 0;
            uint32 areaId = 0;
            if (_playerGUID.IsEmpty())
                if (Player* player = me->FindNearestPlayer(10.0f, true))
                    if (player->GetQuestStatus(QUEST_LAST_CHANCE_AT_HUMANITY) == QUEST_STATUS_NONE)
                    {
                        player->GetZoneAndAreaId(zoneId, areaId);
                        if (areaId == 4786)
                        {
                            _playerGUID = player->GetGUID();
                            return;
                        }

                    }

            _playerGUID = ObjectGuid::Empty;
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_krennan_aranas_36331AI(creature);
    }
};

// start phase 181
// 36332
class npc_king_genn_greymane_36332 : public CreatureScript
{
public:
    npc_king_genn_greymane_36332() : CreatureScript("npc_king_genn_greymane_36332") { }

    bool OnQuestReward(Player* player, Creature* creature, const Quest *_Quest, uint32) override
    {
        if (_Quest->GetQuestId() == QUEST_LAST_CHANCE_AT_HUMANITY)
        {
            player->CastSpell(player, SPELL_PHASE_QUEST_ZONE_SPECIFIC_06, true);
            player->SetUInt32Value(UNIT_FIELD_FLAGS_2, 2048);
            player->RemoveAura(42716);
            player->RemoveAura(50220);
            player->RemoveAura(58284);
            player->RemoveAura(68630);
            return true;
        }
        return false;
    }
};

// player
// 196394
class go_mandragore_196394 : public GameObjectScript
{
public:
    go_mandragore_196394() : GameObjectScript("go_mandragore_196394") {}

    bool OnQuestReward(Player* player, GameObject* /*gameObject*/, Quest const* quest, uint32 /*opt*/) override
    {
        if (quest->GetQuestId() == 14320)
        {
            player->SendCinematicStart(168);
            player->PlayDirectSound(23676);
            return true;
        }

        return false;
    }
};

// Phase 8192/182
// 69094
class spell_cast_keg_69094 : public SpellScriptLoader
{
public:
    spell_cast_keg_69094() : SpellScriptLoader("spell_cast_keg_69094") { }

    class spell_cast_keg_69094_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_cast_keg_69094_SpellScript);

        void CheckTarget(WorldObject*& target)
        {
            if (target->GetEntry() != NPC_HORRID_ABOMINATION)
                target = target->FindNearestCreature(NPC_HORRID_ABOMINATION, 25.0f);
        }

        void Register() override
        {
            OnObjectTargetSelect += SpellObjectTargetSelectFn(spell_cast_keg_69094_SpellScript::CheckTarget, EFFECT_0, TARGET_UNIT_TARGET_ANY);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_cast_keg_69094_SpellScript();
    }
};

// 36283 quest 14382 Two by Sea (enter the ship)
class npc_forsaken_catapult_36283 : public CreatureScript
{
public:
    npc_forsaken_catapult_36283() : CreatureScript("npc_forsaken_catapult_36283") { }

    struct npc_forsaken_catapult_36283AI : public ScriptedAI
    {
        npc_forsaken_catapult_36283AI(Creature* creature) : ScriptedAI(creature) { }

        EventMap    _events;
        ObjectGuid  _playerGUID; // guid only set if mounted
        ObjectGuid  _forsakenGUID; // guid only set if mounted

        void Reset() override
        {
            me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_PC | UNIT_FLAG_IMMUNE_TO_NPC | UNIT_FLAG_NOT_SELECTABLE);
            me->SetReactState(REACT_PASSIVE);
            me->setFaction(1735);
        }

        void PassengerBoarded(Unit* passenger, int8 seatId, bool apply) override
        {
            if (apply)
            {
                if (Player* player = passenger->ToPlayer())
                {
                    _playerGUID = player->GetGUID();
                    if (seatId == 1)
                        _events.ScheduleEvent(EVENT_PLAYER_LAUNCH, 2000);
                }
                else if (Creature* npc = passenger->ToCreature())
                {
                    _forsakenGUID = npc->GetGUID();
                    npc->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_PC | UNIT_FLAG_IMMUNE_TO_NPC | UNIT_FLAG_NOT_SELECTABLE);
                    _events.ScheduleEvent(EVENT_CAST_BOULDER, urand(100, 5000));
                    _events.ScheduleEvent(EVENT_CHECK_PLAYER, 1000);
                    me->setFaction(1735);
                    me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
                }
            }
            else
            {
                if (passenger->ToPlayer())
                {
                    if (seatId == 0)
                        _playerGUID = ObjectGuid::Empty;
                }
                else if (Creature* npc = passenger->ToCreature())
                {
                    _forsakenGUID = ObjectGuid::Empty;
                    npc->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_PC | UNIT_FLAG_IMMUNE_TO_NPC | UNIT_FLAG_NOT_SELECTABLE);
                    _events.CancelEvent(EVENT_CAST_BOULDER);
                    _events.CancelEvent(EVENT_CHECK_PLAYER);
                    _events.ScheduleEvent(EVENT_MASTER_RESET, 180000);
                    me->setFaction(35);
                    me->RemoveAllAuras();
                    me->HandleEmoteCommand(EMOTE_ONESHOT_NONE);
                    me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_NOT_SELECTABLE);
                }
            }
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_CHECK_PLAYER:
                {
                    if (Creature* target = ObjectAccessor::GetCreature(*me, _forsakenGUID))
                        if (me->SelectNearestPlayer(7.0f))
                        {
                            target->ExitVehicle();
                            break;
                        }

                    _events.ScheduleEvent(EVENT_CHECK_PLAYER, 1000);
                    break;
                }
                case EVENT_CAST_BOULDER:
                {
                    me->CastSpell(me, SPELL_FIERY_BOULDER, true);
                    _events.ScheduleEvent(EVENT_CAST_BOULDER, urand(8000, 15000));
                    break;
                }
                case EVENT_MASTER_RESET:
                {
                    if (!_forsakenGUID.IsEmpty() || !_playerGUID.IsEmpty())
                        _events.ScheduleEvent(EVENT_MASTER_RESET, 180000);
                    else
                    {
                        if (TempSummon* npc = me->SummonCreature(NPC_FORSAKEN_MACHINIST, me->GetPosition()))
                            npc->EnterVehicle(me, 0);

                        Reset();
                    }
                    break;
                }
                case EVENT_PLAYER_LAUNCH:
                {
                    if (ObjectAccessor::GetPlayer(*me, _playerGUID))
                    {
                        me->CastSpell(me, 96185, true); // trigger spell 66251 (Aura Id 144 (SPELL_AURA_SAFE_FALL)
                    }

                    _events.ScheduleEvent(EVENT_PLAYER_LANDING, 5000);
                    break;
                }
                case EVENT_PLAYER_LANDING:
                {

                    _events.RescheduleEvent(EVENT_MASTER_RESET, 10000);
                    _playerGUID = ObjectGuid::Empty;

                    break;
                }
                }
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_forsaken_catapult_36283AI(creature);
    }
};

// 68659 trigger 96114
class spell_launch_68659 : public SpellScriptLoader
{
public:
    spell_launch_68659() : SpellScriptLoader("spell_launch_68659") { }

    class spell_launch_68659_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_launch_68659_SpellScript);

        void CheckTargets(std::list<WorldObject*>& targets)
        {
            targets.remove_if([](WorldObject* object)
            {
                return object->GetEntry() == NPC_GENERIC_TRIGGER_LAB_MP;
            });

            if (targets.size() > 0)
                if (WorldObject* wo = targets.front())
                    if (Creature* target = wo->ToCreature())
                    {
                        Position t = target->GetPosition();
                        if (Unit* unit = GetCaster())
                            if (Vehicle* car = unit->GetVehicleKit())
                                if (Unit* pas = car->GetPassenger(0))
                                    if (Player* player = pas->ToPlayer())
                                    {
                                        player->NearTeleportTo(t.GetPositionX(), t.GetPositionY(), t.GetPositionZ(), player->GetOrientation());
                                        return;
                                    }
                    }

            targets.clear();
            this->FinishCast(SPELL_CAST_OK);
        }

        void Register() override
        {
            OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_launch_68659_SpellScript::CheckTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENTRY);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_launch_68659_SpellScript();
    }
};

// 96185 trigger 66251
class spell_launch_96185 : public SpellScriptLoader
{
public:
    spell_launch_96185() : SpellScriptLoader("spell_launch_96185") { }

    class spell_launch_96185_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_launch_96185_SpellScript);

        ObjectGuid _playerGUID;
        Position pos = Position();

        void CheckTarget(WorldObject*& target)
        {
            if (Vehicle* cat = GetCaster()->GetVehicleKit())
                if (Unit* pas = cat->GetPassenger(1))
                    if (Player* player = pas->ToPlayer())
                        target = player;
        }

        void Register() override
        {
            OnObjectTargetSelect += SpellObjectTargetSelectFn(spell_launch_96185_SpellScript::CheckTarget, EFFECT_0, TARGET_UNIT_CASTER);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_launch_96185_SpellScript();
    }
};

// 68591
class spell_fire_boulder_68591 : public SpellScriptLoader
{
public:
    spell_fire_boulder_68591() : SpellScriptLoader("spell_fire_boulder_68591") { }

    class IsNotEntryButPlayer
    {
    public:
        explicit IsNotEntryButPlayer(Unit const* caster, uint32 entry) : _caster(caster), _entry(entry) { }

        bool operator()(WorldObject* obj) const
        {
            if (_caster->GetExactDist2d(obj) < 5.0f || _caster->GetExactDist2d(obj) > 80.0f)
                return true;
            if (Player* player = obj->ToPlayer())
                return player->IsMounted();
            else if (Creature* target = obj->ToCreature())
                return target->GetEntry() != _entry;

            return true;
        }

    private:
        const Unit* _caster;
        uint32 _entry;
    };

    class spell_fire_boulder_68591_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_fire_boulder_68591_SpellScript);

        void CheckTargets(std::list<WorldObject*>& targets)
        {
            targets.remove_if(IsNotEntryButPlayer(GetCaster(), NPC_GENERIC_TRIGGER_LAB_AOI));
            if (targets.size() == 0)
                this->FinishCast(SPELL_CAST_OK);
            else
            {
                WorldObject* target = Trinity::Containers::SelectRandomContainerElement(targets);
                if (!target)
                    this->FinishCast(SPELL_CAST_OK);
                else
                {
                    std::list<WorldObject*>::iterator itr = targets.begin();
                    GetCaster()->SetFacingToObject(*itr);
                }
            }
        }

        void Register() override
        {
            OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_fire_boulder_68591_SpellScript::CheckTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ENTRY);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_fire_boulder_68591_SpellScript();
    }
};

// 36409
class npc_mastiff_36409 : public CreatureScript
{
public:
    npc_mastiff_36409() : CreatureScript("npc_mastiff_36409") { }

    enum eNPC
    {
        EVENT_SEND_MORE_MASTIFF = 901
    };

    struct npc_mastiff_36409AI : public ScriptedAI
    {
        npc_mastiff_36409AI(Creature* creature) : ScriptedAI(creature) { Initialize(); }

        EventMap _events;
        ObjectGuid   _thyalaGUID;
        ObjectGuid   _player_GUID;
        uint32   _mastiff_counter;
        uint32     _mastiff_state;

        void Initialize()
        {
            _events.Reset();
            _mastiff_state = 0;
        }

        void Reset() override
        {
            _thyalaGUID = ObjectGuid::Empty;
            _player_GUID = ObjectGuid::Empty;
            _mastiff_counter = 0;
            me->GetMotionMaster()->Clear(true);
            me->SetSpeedRate(MOVE_WALK, 8.0f);
            me->SetSpeedRate(MOVE_RUN, 8.0f);
            me->SetWalk(false);
            _events.ScheduleEvent(EVENT_CHECK_ATTACK, 500);
        }

        void IsSummonedBy(Unit* summoner) override
        {
            if (Player* player = summoner->ToPlayer())
            {
                _player_GUID = player->GetGUID();
                if (Pet* pet = player->GetPet())
                    player->RemovePet(pet, PET_SAVE_CURRENT_STATE, true);
                me->GetMotionMaster()->MoveFollow(player, 1.2f, RAND(1.75f, 4.71f));
                me->setActive(true);
            }
        }

        void DamageTaken(Unit* attacker, uint32& damage) override
        {
            damage = 0;
        }

        void JustSummoned(Creature* summon) override
        {
            _mastiff_counter += 1;
            summon->GetAI()->SetGUID(_player_GUID, PLAYER_GUID);
            summon->GetAI()->SetGUID(_thyalaGUID, NPC_DARK_RANGER_THYALA);
        }

        void SummonedCreatureDies(Creature* summon, Unit* killer) override
        {
            _mastiff_counter -= 1;
        }

        void SummonedCreatureDespawn(Creature* summon) override
        {
            _mastiff_counter -= 1;
        }

        void EnterEvadeMode(EvadeReason why) override { }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_CHECK_ATTACK:
                {
                    if (_thyalaGUID.IsEmpty())
                        if (Creature* thyala = me->FindNearestCreature(NPC_DARK_RANGER_THYALA, 100.0f))
                            _thyalaGUID = thyala->GetGUID();

                    if (Creature* thyala = ObjectAccessor::GetCreature(*me, _thyalaGUID))
                    {
                        if (!thyala->IsAlive() || !thyala->IsInWorld())
                        {
                            if (Player* player = ObjectAccessor::GetPlayer(*me, _player_GUID))
                                player->KilledMonsterCredit(NPC_DARK_RANGER_THYALA);

                            me->DespawnOrUnsummon(1000);
                        }

                        float dist = me->GetDistance2d(thyala);

                        switch (_mastiff_state)
                        {
                        case 0: // we are outside range.. and comes near
                        {
                            if (dist < 100.0f)
                            {
                                _mastiff_state = 1;
                                me->SetReactState(REACT_PASSIVE);
                            }
                            break;
                        }
                        case 1: // more near
                        {
                            if (dist < 50.0f)
                            {
                                _mastiff_state = 2;
                                me->SetReactState(REACT_PASSIVE);
                                me->GetMotionMaster()->MoveChase(thyala, 19.0f, 0.0f);
                            }
                            break;
                        }
                        case 2: // now we run to our command place
                        {
                            if (dist < 20.0f)
                            {
                                _mastiff_state = 3;
                                me->SetReactState(REACT_PASSIVE);
                                me->GetMotionMaster()->MoveIdle();
                            }
                            break;
                        }
                        case 3: // and call all other attack mastiff
                        {
                            if (_mastiff_counter < 50)
                            {
                                std::list<Creature*>trigger = me->FindNearestCreatures(NPC_TRIGGER, 100.0f);
                                for (std::list<Creature*>::const_iterator itr = trigger.begin(); itr != trigger.end(); ++itr)
                                    me->SummonCreature(NPC_MASTIFF, (*itr)->GetNearPosition(5.0f, frand(0.0f, 6.28f)), TEMPSUMMON_TIMED_DESPAWN, 60000);
                            }
                            break;
                        }
                        }
                        _events.ScheduleEvent(EVENT_CHECK_ATTACK, 1000);
                        break;
                    }
                }
                }
            }

            if (!UpdateVictim())
                return;
            else
                DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_mastiff_36409AI(creature);
    }
};

// 36405
class npc_mastiff_36405 : public CreatureScript
{
public:
    npc_mastiff_36405() : CreatureScript("npc_mastiff_36405") { }

    struct npc_mastiff_36405AI : public ScriptedAI
    {
        npc_mastiff_36405AI(Creature* creature) : ScriptedAI(creature) { Initialize(); }

        EventMap _events;
        ObjectGuid   _thyalaGUID;
        ObjectGuid   _player_GUID;

        void Initialize()
        {
            _events.Reset();
        }

        void Reset() override
        {
            me->SetWalk(false);
            me->SetSpeedRate(MOVE_WALK, 8.0f);
            me->SetSpeedRate(MOVE_RUN, 8.0f);
            me->SetReactState(REACT_AGGRESSIVE);
            _thyalaGUID = ObjectGuid::Empty;
            _player_GUID = ObjectGuid::Empty;
            _events.RescheduleEvent(EVENT_CHECK_ATTACK, 1000);
        }

        void SetGUID(ObjectGuid guid, int32 id) override
        {
            switch (id)
            {
            case PLAYER_GUID:
            {
                _player_GUID = guid;
                break;
            }
            case NPC_DARK_RANGER_THYALA:
            {
                _thyalaGUID = guid;
                break;
            }
            }
        }

        void EnterEvadeMode(EvadeReason why) override
        {
            StartAttackThyala();
            _events.RescheduleEvent(EVENT_CHECK_ATTACK, 1000);
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_CHECK_ATTACK:
                {
                    StartAttackThyala();
                    _events.ScheduleEvent(EVENT_CHECK_ATTACK, 1000);
                    break;
                }
                }
            }

            if (!UpdateVictim())
                return;
            else
                DoMeleeAttackIfReady();
        }

        void StartAttackThyala()
        {
            Creature* thyala = ObjectAccessor::GetCreature(*me, _thyalaGUID);
            if (!thyala)
                return;

            if (!thyala->IsAlive() || !thyala->IsInWorld())
            {
                me->DespawnOrUnsummon(1);
                return;
            }

            if (me->IsInCombat())
                if (Unit* npc = me->GetVictim())
                    if (npc->GetEntry() == NPC_DARK_RANGER_THYALA)
                        return;

            me->SetReactState(REACT_AGGRESSIVE);
            me->GetMotionMaster()->MoveChase(thyala, 3.0f, frand(0.0f, 6.28f));
            me->Attack(thyala, true);
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_mastiff_36405AI(creature);
    }
};

// 36458
class npc_gramdma_wahl_36458 : public CreatureScript
{
public:
    npc_gramdma_wahl_36458() : CreatureScript("npc_gramdma_wahl_36458") { }

    struct npc_gramdma_wahl_36458AI : public ScriptedAI
    {
        npc_gramdma_wahl_36458AI(Creature* creature) : ScriptedAI(creature) { }

        EventMap     _events;
        ObjectGuid   _playerGUID;

        void Reset() override
        {
            _playerGUID = ObjectGuid::Empty;
        }

        void IsSummonedBy(Unit* summoner) override
        {
            if (Creature* felix = summoner->ToCreature())
                if (felix->GetEntry() == NPC_CAT_FELIX)
                    _events.RescheduleEvent(EVENT_START_ANIM, 10);
        }

        void MovementInform(uint32 type, uint32 id) override
        {
            if (type == POINT_MOTION_TYPE)
            {
                if (id == 1044)
                    _events.ScheduleEvent(EVENT_START_ANIM_02, 10);
                else if (id == 1045)
                    _events.ScheduleEvent(EVENT_START_ANIM_04, 10);
            }
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_START_ANIM:
                {
                    me->GetMotionMaster()->MovePoint(1044, -2102.53f, 2335.98f, 7.529f, false);
                    break;
                }
                case EVENT_START_ANIM_02:
                    Talk(0);
                    _events.ScheduleEvent(EVENT_START_ANIM_03, 5000);
                    break;
                case EVENT_START_ANIM_03:
                    me->GetMotionMaster()->MovePoint(1045, -2103.53f, 2357.98f, 6.4f, false);
                    break;
                case EVENT_START_ANIM_04:
                    me->DespawnOrUnsummon();
                    break;
                }
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_gramdma_wahl_36458AI(creature);
    }
};



// Phase 183 (16384)
// 36743
class npc_king_genn_greymane_36743 : public CreatureScript
{
public:
    npc_king_genn_greymane_36743() : CreatureScript("npc_king_genn_greymane_36743") { }

    bool OnQuestReward(Player* player, Creature* creature, Quest const* quest, uint32 /*opt*/) override
    {
        switch (quest->GetQuestId())
        {
            case QUEST_THE_KINGS_OBSERVATORY:
            {
                creature->AI()->SetGUID(player->GetGUID(), PLAYER_GUID);
                creature->AI()->DoAction(EVENT_CHANGE_PHASE);
                break;
            }
            case QUEST_ALAS_GILNEAS:
            {
                player->RemoveAura(SPELL_PHASE_QUEST_ZONE_SPECIFIC_08);
                player->AddAura(SPELL_PHASE_QUEST_ZONE_SPECIFIC_11, player);
                creature->AI()->SetGUID(player->GetGUID(), PLAYER_GUID);
                creature->AI()->DoAction(EVENT_START_VIDEO);
                break;
            }
        }
        return false;
    }

    struct npc_king_genn_greymane_36743AI : public ScriptedAI
    {
        npc_king_genn_greymane_36743AI(Creature* creature) : ScriptedAI(creature) { }

        EventMap    _events;
        ObjectGuid  _playerGUID;

        void Reset() override
        {
            _playerGUID = ObjectGuid::Empty;
        }

        void SetGUID(ObjectGuid guid, int32 id) override
        {
            switch (id)
            {
                case PLAYER_GUID:
                {
                    _playerGUID = guid;
                    break;
                }
            }
        }

        void DoAction(int32 param) override
        {
            switch (param)
            {
                case EVENT_CHANGE_PHASE:
                {
                    _events.RescheduleEvent(EVENT_CHANGE_PHASE, 4000);
                    break;
                }
                case EVENT_START_VIDEO:
                {
                    _events.RescheduleEvent(EVENT_START_VIDEO, 1000);
                    break;
                }
            }
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_START_VIDEO:
                    {
                        if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        {
                            player->SendCinematicStart(167);
                            player->PlayDirectSound(23539);
                        }
                        break;
                    }
                    case 1:
                    {
                        if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        {
                            player->RemoveAura(SPELL_PHASE_QUEST_ZONE_SPECIFIC_08);
                            player->AddAura(SPELL_PHASE_QUEST_ZONE_SPECIFIC_11, player);
                        }
                        break;
                    }
                }
            }

            if (!UpdateVictim())
                return;
            else
                DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_king_genn_greymane_36743AI(creature);
    }
};

// after cataclysm earthquake : phase 186 (131072), WorldMap 679 and TerrainSwap 656
// the ride has additional phase 194

/* NOTE: 1.) the carriage is sampled by 2 vehicles, harness and carriage. the harness take the waypoints_data and the carriage take the passengers
   2.) the core is allways trying to mount us on the clicked vehicle. The spell 72767 give us the mount spell 72764, but the core is NOT able to point
   the 72764 spell to the new spawned carriage 43337 and mount us there.. 3.) the mount is not take notice from that we use the SpellEffect 28 to spawn new vehicles.
   4.) so we have to prevent mounting player from 44928 and mount him to the seat 0 of the new spawned 43337 and then starting waypoints on the harness 43336 */

// 44928 
class npc_stagecoach_carriage_44928 : public CreatureScript
{
public:
    npc_stagecoach_carriage_44928() : CreatureScript("npc_stagecoach_carriage_44928") { }

    struct npc_stagecoach_carriage_44928AI : public VehicleAI
    {
        npc_stagecoach_carriage_44928AI(Creature* creature) : VehicleAI(creature) { }

        void PassengerBoarded(Unit* passenger, int8 seatId, bool apply) override
        {
            if (apply)
            {
                if (Player* player = passenger->ToPlayer())
                    player->ExitVehicle();
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_stagecoach_carriage_44928AI(creature);
    }
};

// 43336
class npc_harness_43336 : public CreatureScript
{
public:
    npc_harness_43336() : CreatureScript("npc_harness_43336") { }

    struct npc_harness_43336AI : public VehicleAI
    {
        npc_harness_43336AI(Creature* creature) : VehicleAI(creature) { }

        EventMap _events;
        ObjectGuid   _playerGUID;
        ObjectGuid   _carriageGUID;
        ObjectGuid   _gateGUID;

        void Reset() override
        {
            _events.Reset();
            _playerGUID = ObjectGuid::Empty;
            _carriageGUID = ObjectGuid::Empty;
            _gateGUID = ObjectGuid::Empty;
            me->setActive(true);
        }

        void IsSummonedBy(Unit* summoner) override
        {
            if (Player* player = summoner->ToPlayer())
            {
                _playerGUID = player->GetGUID();
            }
        }

        void JustSummoned(Creature* summon) override
        {
            switch (summon->GetEntry())
            {
            case NPC_CARRIAGE_43337:
                _carriageGUID = summon->GetGUID();
                break;
            }
        }

        void MovementInform(uint32 type, uint32 id) override
        {
            if (type == WAYPOINT_MOTION_TYPE)
                switch (id)
                {
                case 5: // open gate 1
                    if (GameObject* go = me->FindNearestGameObject(196864, 25.0f))
                    {
                        go->UseDoorOrButton(3000);
                        _gateGUID = go->GetGUID();
                    }
                    break;
                case 7:
                    if (GameObject* go = ObjectAccessor::GetGameObject(*me, _gateGUID))
                        go->ResetDoorOrButton();
                    break;
                case 21: // open kings gate
                    if (GameObject* go = me->FindNearestGameObject(196412, 75.0f))
                        go->Use(me);
                    break;
                case 26: // add phase 194
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                    {
                        player->AddAura(SPELL_PHASE_QUEST_ZONE_SPECIFIC_19); // so we see the welcome orc's
                        player->RemoveAura(SPELL_GENERIC_QUEST_INVISIBLE_DETECTION_10); // this is only for endfight near orc's
                    }
                    break;
                case 37: // attack from orc...
                {
                    break;
                }
                case 42: // player exit vehicle
                {
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        player->ExitVehicle();
                    break;
                }
                case 54: // derspawn
                {
                    me->DespawnOrUnsummon(1000);
                    if (Creature* car = ObjectAccessor::GetCreature(*me, _carriageGUID))
                        car->DespawnOrUnsummon(1000);
                    break;
                }
                }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_harness_43336AI(creature);
    }
};

// 43337
class npc_stagecoach_carriage_43337 : public CreatureScript
{
public:
    npc_stagecoach_carriage_43337() : CreatureScript("npc_stagecoach_carriage_43337") { }

    struct npc_stagecoach_carriage_43337AI : public VehicleAI
    {
        npc_stagecoach_carriage_43337AI(Creature* creature) : VehicleAI(creature) { }

        EventMap     _events;
        uint32       _count;
        Position     _pos;
        ObjectGuid   _playerGUID;
        ObjectGuid   _harnessGUID;

        void Reset() override
        {
            _count = 0;
            _playerGUID = ObjectGuid::Empty;
            _harnessGUID = ObjectGuid::Empty;
            me->setActive(true);
        }

        void IsSummonedBy(Unit* summoner) override
        {
            if (Unit* unit = summoner->GetCharmerOrOwner())
                if (Player* player = unit->ToPlayer())
                {
                    _playerGUID = player->GetGUID();
                    _harnessGUID = summoner->GetGUID();
                }
        }

        void PassengerBoarded(Unit* passenger, int8 seatId, bool apply) override
        {
            if (apply)
            {
                if (Player* player = passenger->ToPlayer())
                    _events.ScheduleEvent(EVENT_START_WAYPOINTS_01, 10);
                else if (Creature* npc = passenger->ToCreature())
                {
                    _count += 1;
                    if (_count == 6)
                    {
                        if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                            if (Vehicle* v1 = me->GetVehicleKit())
                                v1->AddPassenger(player, 0);
                    }
                }
            }
            else
            {
                if (Creature* npc = passenger->ToCreature())
                    _count -= 1;
                else if (Player* player = passenger->ToPlayer()) // let player jump on the left carriage site 
                {
                     _pos = me->GetNearPosition(10.0f, 4.57f);
                    _events.ScheduleEvent(EVENT_SET_ORIENTATION, 1000);
                }
            }
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_START_WAYPOINTS_01:
                {
                    if (Creature* npc = ObjectAccessor::GetCreature(*me, _harnessGUID))
                        npc->GetMotionMaster()->MovePath(4333601, false);

                    break;
                }
                case EVENT_SET_ORIENTATION:
                {
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        if (Creature* prince = me->FindNearestCreature(37065, 25.0f))
                        {

                            player->NearTeleportTo(_pos.m_positionX, _pos.m_positionY, _pos.m_positionZ, _pos.GetOrientation());
                            player->SetFacingToObject(prince);
                        }

                    break;
                }
                }
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_stagecoach_carriage_43337AI(creature);
    }
};

// 38762
class npc_ogre_ambusher_38762 : public CreatureScript
{
public:
    npc_ogre_ambusher_38762() : CreatureScript("npc_ogre_ambusher_38762") { }

    struct npc_ogre_ambusher_38762AI : public ScriptedAI
    {
        npc_ogre_ambusher_38762AI(Creature* creature) : ScriptedAI(creature) { }

        EventMap _events;
        bool _cast_cooldown;

        void Reset() override
        {
            _cast_cooldown = false;
            me->SetReactState(REACT_PASSIVE);
        }

        void MoveInLineOfSight(Unit* who) override
        {
            if (!_cast_cooldown)
                if (who->ToPlayer() || who->getFaction() == 2203 || who->getFaction() == 2163)
                    if (me->GetDistance(who) < 60.0f)
                    {
                        me->SetFacingToObject(who);
                        if (urand(0, 100) < 30)
                        {
                            me->CastSpell(who, SPELL_THROW_BOULDER, true);
                            _cast_cooldown = true;
                            _events.ScheduleEvent(EVENT_COOLDOWN_00, urand(2500, 3500));
                        }
                    }
        }

        void AttackStart(Unit* who) override
        {
            DoStartNoMovement(who);
        }

        void EnterCombat(Unit* /*victim*/) override { }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_COOLDOWN_00:
                    {
                        _cast_cooldown = false;
                        break;
                    }
                }
            }

            if (!UpdateVictim())
                return;
            else
                DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_ogre_ambusher_38762AI(creature);
    }
};

// 6687
class at_the_blackwald_6687 : public AreaTriggerScript
{
public:
    at_the_blackwald_6687() : AreaTriggerScript("at_the_blackwald_6687") { }

    bool OnTrigger(Player* player, const AreaTriggerEntry* /*at*/, bool /*enter*/) override
    {
        if (player->GetQuestStatus(QUEST_LOSING_YOUR_TAIL) == QUEST_STATUS_INCOMPLETE)
            if (!player->FindNearestCreature(NPC_DARK_SCOUT, 50.0f))
                player->CastSpell(player, SPELL_FREEZING_TRAP_EFFECT, true);
        return false;
    }
};

// 37953
class npc_dark_scout_37953 : public CreatureScript
{
public:
    npc_dark_scout_37953() : CreatureScript("npc_dark_scout_37953") { }

    enum eNpc
    {
        TALK_EASY = 0,
        TALK_DO = 2,
        TALK_HOW = 1,
    };

    struct npc_dark_scout_37953AI : public ScriptedAI
    {
        npc_dark_scout_37953AI(Creature* creature) : ScriptedAI(creature) { }

        EventMap    _events;
        ObjectGuid  _playerGUID;

        void Reset() override
        {
            _events.Reset();
            _playerGUID = ObjectGuid::Empty;
        }

        void IsSummonedBy(Unit* summoner) override
        {
            if (Player* player = summoner->ToPlayer())
            {
                _playerGUID = player->GetGUID();
                me->GetMotionMaster()->Clear();
                me->SetFacingToObject(player);
                me->AI()->Talk(TALK_EASY);
                _events.ScheduleEvent(EVENT_TALK_START, 5000);
                _events.ScheduleEvent(EVENT_CHECK_PLAYER, 1000);
            }
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_CHECK_PLAYER:
                    {
                        if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                            if (player->GetQuestStatus(QUEST_LOSING_YOUR_TAIL) == QUEST_STATUS_INCOMPLETE)
                                if (player->GetDistance2d(me) < 30.0f)
                                {
                                    if (!me->IsInCombat())
                                        _events.ScheduleEvent(EVENT_MELEE_ATTACK, 500);
                                    _events.ScheduleEvent(EVENT_CHECK_PLAYER, 1000);
                                    break;
                                }
                        me->DespawnOrUnsummon(100);
                        break;
                    }
                    case EVENT_TALK_START:
                    {
                        if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                            Talk(TALK_DO, player);
                        _events.ScheduleEvent(EVENT_SHOOT, 7500);
                        _events.ScheduleEvent(EVENT_CHECK_AURA, 250);
                        break;
                    }
                    case EVENT_SHOOT:
                    {
                        if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        {
                            _events.CancelEvent(EVENT_CHECK_AURA);
                            me->CastSpell(player, SPELL_AIMED_SHOOT);
                            me->GetMotionMaster()->Clear();
                            me->SetWalk(true);
                            me->SetSpeedRate(MOVE_WALK, 1.0f);
                            me->GetMotionMaster()->MoveChase(player);
                            me->Attack(player, true);
                        }
                        break;
                    }
                    case EVENT_CHECK_AURA:
                    {
                        if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                            if (player->HasAura(SPELL_FREEZING_TRAP_EFFECT))
                            {
                                _events.ScheduleEvent(EVENT_CHECK_AURA, 250);
                                break;
                            }
                        _events.CancelEvent(EVENT_SHOOT);
                        Talk(TALK_HOW);
                        _events.ScheduleEvent(EVENT_MELEE_ATTACK, 1500);
                        break;
                    }
                    case EVENT_MELEE_ATTACK:
                    {
                        if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                            if (!player->IsInCombat())
                                if (player->GetDistance2d(me) < 30.0f)
                                {
                                    me->GetMotionMaster()->Clear();
                                    me->SetWalk(true);
                                    me->SetSpeedRate(MOVE_WALK, 1.0f);
                                    me->GetMotionMaster()->MoveChase(player);
                                    me->Attack(player, true);
                                }
                        _events.ScheduleEvent(EVENT_MELEE_ATTACK, 1500);
                        break;
                    }
                }
            }

            if (!UpdateVictim())
                return;
            else
                DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_dark_scout_37953AI(creature);
    }
};

// quest_24616
class npc_trigger_quest_24616 : public CreatureScript
{
public:
    npc_trigger_quest_24616() : CreatureScript("npc_trigger_quest_24616") { }

    struct npc_trigger_quest_24616AI : public ScriptedAI
    {
        npc_trigger_quest_24616AI(Creature* creature) : ScriptedAI(creature) { }

        void Reset() override
        {
            mui_timerAllowSummon = urand(3000, 5000);
            allowSummon = false;
            playerGUID = ObjectGuid::Empty;
        }

        void MoveInLineOfSight(Unit* who) override
        {
            if (who->GetTypeId() != TYPEID_PLAYER)
                return;
            if (who->ToPlayer()->GetQuestStatus(24616) != QUEST_STATUS_INCOMPLETE || me->FindNearestCreature(37953, 100, false) != NULL)
                return;
            if (me->IsWithinDistInMap(who, 20.0f))
            {
                allowSummon = true;
                playerGUID = who->GetGUID();
            }
        }

        void UpdateAI(uint32 diff) override
        {
            if (!allowSummon)
                return;
            if (mui_timerAllowSummon <= diff)
            {
                if (Player *player = ObjectAccessor::GetPlayer(*me, playerGUID))
                    if (me->FindNearestCreature(37953, 100) == NULL)
                        me->CastSpell(player, 70794, true);
                allowSummon = false;
                playerGUID = ObjectGuid::Empty;
                mui_timerAllowSummon = urand(3000, 5000);
            }
            else mui_timerAllowSummon -= diff;
        }

    private:
        uint32 mui_timerAllowSummon;
        bool allowSummon;
        ObjectGuid playerGUID;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_trigger_quest_24616AI(creature);
    }
};

// 49944
class item_belysras_talisman_49944 : public ItemScript
{
public:
    item_belysras_talisman_49944() : ItemScript("item_belysras_talisman_49944") { }

    bool OnUse(Player* player, Item* /*item*/, SpellCastTargets const& /*targets*/, ObjectGuid /*castId*/) override
    {
        if (player->GetQuestStatus(QUEST_LOSING_YOUR_TAIL) == QUEST_STATUS_INCOMPLETE)
            if (player->HasAura(SPELL_FREEZING_TRAP_EFFECT))
                player->RemoveAura(SPELL_FREEZING_TRAP_EFFECT);

        return false;
    }
};

// 37694
class npc_enslaved_villager_37694 : public CreatureScript
{
public:
    npc_enslaved_villager_37694() : CreatureScript("npc_enslaved_villager_37694") { }

    struct npc_enslaved_villager_37694AI : public ScriptedAI
    {
        npc_enslaved_villager_37694AI(Creature* creature) : ScriptedAI(creature) { }

        EventMap    _events;
        ObjectGuid  _playerGUID;
        ObjectGuid  _ballGUID;

        void Reset() override
        {
            _playerGUID = ObjectGuid::Empty;
            _ballGUID = ObjectGuid::Empty;
        }

        void SetGUID(ObjectGuid guid, int32 id) override
        {
            switch (id)
            {
                case PLAYER_GUID:
                    _playerGUID = guid;
                    break;
                case GO_BALL_AND_CHAIN:
                    _ballGUID = guid;
                    break;
            }
        }

        void DoAction(int32 param) override
        {
            switch (param)
            {
                case EVENT_START_ANIM:
                {
                    if (GameObject* ball = ObjectAccessor::GetGameObject(*me, _ballGUID))
                        ball->DestroyForNearbyPlayers();

                    _events.ScheduleEvent(EVENT_START_ANIM, 1000);
                    break;
                }
            }
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_START_ANIM:
                    {
                        me->HandleEmoteCommand(EMOTE_STATE_NONE);
                        if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        {
                            me->SetFacingToObject(player);
                            Talk(0, player);
                        }

                        _events.ScheduleEvent(EVENT_TALK_PART_00, 3000);
                        break;
                    }
                    case EVENT_TALK_PART_00:
                    {
                        if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                            me->GetMotionMaster()->MoveFleeing(player, 7000);

                        _events.ScheduleEvent(EVENT_TALK_PART_01, 6000);
                        break;
                    }
                    case EVENT_TALK_PART_01:
                        me->DespawnOrUnsummon(10);
                        break;
                }
            }

            if (!UpdateVictim())
                return;
            else
                DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_enslaved_villager_37694AI(creature);
    }
};

// 201775
class go_ball_and_chain_201775 : public GameObjectScript
{
public:
    go_ball_and_chain_201775() : GameObjectScript("go_ball_and_chain_201775") {}

    void OnLootStateChanged(GameObject* go, uint32 state, Unit* unit) override
    {
        if (state == 2 && unit)
            if (Player* player = unit->ToPlayer())
                if (player->GetQuestStatus(QUEST_LIBERATION_DAY) == QUEST_STATUS_INCOMPLETE || player->GetQuestStatus(QUEST_LIBERATION_DAY) == QUEST_STATUS_COMPLETE)
                    if (Creature* villager = go->FindNearestCreature(NPC_ENSLAVED_VILLAGER, 5.0f))
                    {
                        villager->AI()->SetGUID(go->GetGUID(), go->GetEntry());
                        villager->AI()->SetGUID(player->GetGUID(), PLAYER_GUID);
                        villager->AI()->DoAction(EVENT_START_ANIM);
                    }
    }
};

// from here phase 262144 is active.. battle for gilneas in zone_gilneas_city3

void AddSC_zone_gilneas_duskhaven()
{
    new npc_krennan_aranas_36331();
    new npc_king_genn_greymane_36332();
    new go_mandragore_196394();
    new spell_cast_keg_69094();
    new npc_forsaken_catapult_36283();
    new spell_launch_68659();
    new spell_launch_96185();
    new spell_fire_boulder_68591();
    new npc_mastiff_36409();
    new npc_mastiff_36405();
    new npc_gramdma_wahl_36458();
    new npc_stagecoach_carriage_44928();
    new npc_harness_43336();
    new npc_stagecoach_carriage_43337();

    new npc_king_genn_greymane_36743();
    new npc_ogre_ambusher_38762();
    new at_the_blackwald_6687();
    new npc_dark_scout_37953();
    new npc_trigger_quest_24616();
    new item_belysras_talisman_49944();
    new npc_enslaved_villager_37694();
    new go_ball_and_chain_201775();
};
