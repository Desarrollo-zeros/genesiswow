/*
 * Copyright (C) 2019 PPA-Core.
 * Copyright (C) 2011-2016 ArkCORE <http://www.arkania.net/>
 * Copyright (C) 2008-2014 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2006-2009 ScriptDev2 <https://scriptdev2.svn.sourceforge.net/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "zone_gilneas.h"
#include "Cell.h"
#include "CellImpl.h"
#include "CombatAI.h"
#include "Creature.h"
#include "CreatureAI.h"
#include "CreatureAIImpl.h"
#include "GameObjectAI.h"
#include "GameObject.h"
#include "GridNotifiers.h"
#include "MotionMaster.h"
#include "MoveSplineInit.h"
#include "MoveSpline.h"
#include "ObjectAccessor.h"
#include "PassiveAI.h"
#include "Pet.h"
#include "PhasingHandler.h"
#include "Player.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "ScriptedEscortAI.h"
#include "ScriptedFollowerAI.h"
#include "SpellScript.h"
#include "Unit.h"
#include "Vehicle.h"



enum eZoneGilneas
{
    GO_MERCHANT_DOOR                             = 195327,

    NPC_PANICKED_CITIZEN                         = 34851,
    NPC_PRINCE_LIAM                              = 34913,
    NPC_WORGEN_ALPHA_C2                          = 35167,
    NPC_WORGEN_ALPHA_C1                          = 35170,
    NPC_WORGEN_RUNT_C1                           = 35188,
    NPC_BLOODFANG_STALKER                        = 35229,
    NPC_DARIUS_CROWLEY                           = 35230,
    NPC_CROWLEYS_HORSE                           = 35231,
    NPC_JOSIAH_AVERY_35370                       = 35370,
    NPC_WORGEN_RUNT_C2                           = 35456,
    NPC_BLOODFANG_LURKER                         = 35463,
    NPC_BLOODFANG_RIPPER_35505                   = 35505,
    NPC_PRINCE_GREYMANE                          = 35551,
    NPC_LORD_DARIUS_CROWLEY                      = 35552,
    NPC_BLOODFANG_STALKER_CREDIT                 = 35582,
    NPC_TOBIAS_MISTMANTLE                        = 35618,
    NPC_FRENZIED_STALKER                         = 35627,
    NPC_GILNEAN_MASTIFF                          = 35631,
    NPC_KRENNAN_ARANAS_TREE                      = 35753,
    NPC_GILNEAS_EVACUATION_FACING_MARKER_35830   = 35830,
    NPC_GILNEAS_EVACUATION_FACING_MARKER_35010   = 35010,
    NPC_FRIGHTENED_CITIZEN_WORGEN                = 35836,
    NPC_GRAYMANE_HORSE_35905                     = 35905,
    NPC_KRENNAN_ARANAS                           = 35907,
    NPC_KING_GREYMANE                            = 35911,
    NPC_COMMANDEERED_CANNON                      = 35914,
    NPC_NORTHGATE_REBEL_1                        = 36057,
    NPC_PANICKED_CITIZEN_GATE                    = 44086,
    NPC_HEALING_CREDIT                           = 47091,
    NPC_JOSAIH_AVERY_TRIGGER                     = 50415,
    NPC_LORNA_CROWLEY_35378                      = 35378,
    NPC_FRIGHTENED_CITIZEN_34981                 = 34981,
    NPC_RAMPAGING_WORGEN_35660                   = 35660,

    QUEST_LOCKDOWN                               = 14078,
    QUEST_EVACUATE_THE_MERCHANT_SQUARE           = 14098,

    QUEST_THE_REBEL_LORDS_ARSENAL                = 14159,
    QUEST_FROM_THE_SHADOWS                       = 14204,
    QUEST_SACRIFICES                             = 14212,
    QUEST_LAST_STAND                             = 14222,
    QUEST_FLASH_HEAL                             = 14279,
    QUEST_A_REJUVENATING_TOUCH                   = 14283,
    QUEST_SAVE_KRENNAN_ARANAS                    = 14293,

    SPELL_RENEW                                  = 139,
    SPELL_SHADOWSTALKER_STEALTH                  = 5916,
    SPELL_SHOOT                                  = 6660,
    SPELL_ENRAGE_8599                            = 8599,
    SPELL_ENRAGE_56646                           = 56646,
    SPELL_BY_THE_SKIN                            = 66914,
    SPELL_THROW_TORCH                            = 67063,
    SPELL_GET_SHOT                               = 67349,
    SPELL_SUMMON_JOSIAH_AVERY                    = 67350,
    SPELL_FORCE_CAST_SUMMON_JOSIAH               = 67352,
    SPELL_ATTACK_LURKER                          = 67805,
    SPELL_SUMMON_GILNEAN_MASTIFF                 = 67807,
    SPELL_FORCECAST_MASTIFF                      = 68234,
    SPELL_RESCUE_KRENNAN                         = 68219,
    SPELL_FORCECAST_SUMMON_GRAYMANE_HORSE        = 68232,
    SPELL_CANNON_FIRE                            = 68235,
    SPELL_CURSE_OF_THE_WORGEN                    = 68630,
    SPELL_TWO_FORMS                              = 68996,
    SPELL_IN_STOCKS                              = 69196,
    SPELL_COSMETIC_COMBAT_ATTACK                 = 69873,
    SPELL_TRANSFORMING                           = 72794,
    SPELL_WORGEN_BITE                            = 72870,
    SPELL_INFECTED_BITE                          = 72872,
    SPELL_FROSTBOLT_VISUAL_ONLY                  = 74277,
    SPELL_HIDEOUS_BITE_WOUND                     = 76642,
    SPELL_RIDE_BUNNY_SEAT2                       = 84275,
    SPELL_GILNEAS_CANNON_CAMERA                  = 93555,
    SPELL_FADE_OF_BLACK                          = 94053,
    SPELL_ALTERED_FORM                           = 94293,
    SPELL_ALTERED_FORM2                          = 97709,
    SPELL_FORCE_WORGEN_ALTERED_FORM              = 98274,
    SPELL_SUMMON_CROWLEY_HORSE                   = 67001,
    SPELL_KNOCK_ON_DOOR_66639                    = 66639,
    SPELL_KNOCKING_67869                         = 67869,

    ZONE_DUSKHAVEN                               = 4786,

    SPELL_GENERIC_QUEST_INVISIBILITY_DETECTION_1 = 49416,
    SPELL_PHASE_QUEST_ZONE_SPECIFIC_01           = 59073,
    SPELL_PHASE_QUEST_ZONE_SPECIFIC_02           = 59074,

    WAYPOINT_KING_HORSE_PART1                    = 3590501,
    WAYPOINT_KING_HORSE_PART2                    = 3590502,
    WAYPOINT_KING_HORSE_PART3                    = 3590503,

    EVENT_ANIM_1 = 1000,
    EVENT_ANIM_2,
    EVENT_ANIM_3,
    EVENT_ANIM_4,
    EVENT_ANIM_5,
    EVENT_CHECK_ORIENTATION,
    EVENT_CHECK_QUEST_REWARDED,
    EVENT_COOLDOWN_DOOR,
    EVENT_ENRAGE_COOLDOWN,
    EVENT_FOLLOW_CITIZEN1,
    EVENT_HORSE_DISMOUNT,
    EVENT_HORSE_UNSUMMON,
    EVENT_KING_HORSE_END,
    EVENT_KING_HORSE_JUMP,
    EVENT_KING_HORSE_START2,
    EVENT_MOVE_TO_DOOR,
    EVENT_MOVE_TO_PLAYER,
    EVENT_MOVE_TO_MARKER,
    EVENT_MOVE_TO_END,
    EVENT_OPEN_DOOR,
    EVENT_SAY_JOSIAH_AVERY,
    EVENT_TALK_00,
    EVENT_TELEPORT,
    EVENT_TELEPORT_NEAR,
    EVENT_WAIT_SAVE_ARANAS,

    ACTION_START_ANIM_MERCANT,
    ACTION_START_ANIM_CITIZEN,
    ACTION_START_ANIM,
    ACTION_TELEPORT,
    ACTION_TELEPORT_NEAR,

    MOVE_TO_START_POSITION,
    MOVE_TO_DOOR,
    MOVE_TO_PLAYER,
    MOVE_TO_MARKER,
    MOVE_TO_END,

};

//Cuervos Gilneas 50260
class npc_gilnean_crow : public CreatureScript
{
public:
    npc_gilnean_crow() : CreatureScript("npc_gilnean_crow") { }
     struct Coord
    {
        float x;
        float y;
        float z;
    };
     struct CrowFlyPosition
    {
        Coord FirstCoord;
        Coord SecondCoord;
    };
     struct npc_gilnean_crow50260AI : public ScriptedAI
    {
        npc_gilnean_crow50260AI(Creature* creature) : ScriptedAI(creature) { }
         uint8 pointId;
        bool flying = false;
         const CrowFlyPosition CrowFlyPos[12] =
        {
            {{-1407.20f, 1441.68f, 39.6586f}, {-1407.20f, 1441.68f, 67.7066f}},
            {{-1619.61f, 1310.61f, 27.7544f}, {-1619.61f, 1310.61f, 54.9702f}},
            {{-1799.56f, 1552.02f, 34.9408f}, {-1799.56f, 1552.02f, 38.4683f}},
            {{-1837.99f, 2289.09f, 50.2894f}, {-1837.99f, 2289.09f, 52.4776f}},
            {{-1970.88f, 2326.11f, 36.5107f}, {-1970.88f, 2326.11f, 38.8598f}},
            {{-1918.45f, 2406.86f, 37.4498f}, {-1918.45f, 2406.86f, 39.2891f}},
            {{-1845.37f, 2322.57f, 47.8401f}, {-1845.37f, 2322.57f, 50.0315f}},
            {{-1844.33f, 2492.53f, 6.67603f}, {-1844.33f, 2492.53f, 9.67311f}},
            {{-2035.53f, 2289.68f, 28.7353f}, {-2035.53f, 2289.68f, 32.0705f}},
            {{-2164.88f, 2222.65f, 27.4170f}, {-2164.88f, 2222.65f, 29.1592f}},
            {{-1781.95f, 2382.97f, 51.9086f}, {-1781.95f, 2382.97f, 55.8622f}},
            {{-1654.78f, 2503.14f, 109.893f}, {-1654.78f, 2503.14f, 115.819f}},
        };
         void InitializeAI()
        {
            FindFlyId();
        }
         void FindFlyId()
        {
            float dist = std::numeric_limits<float>::max();
            int i = 0;
             for (int j = 0; j < 12; ++j)
            {
                float _dist = me->GetDistance2d(CrowFlyPos[j].FirstCoord.x, CrowFlyPos[j].FirstCoord.y);
                 if (dist > _dist)
                {
                    dist = _dist;
                    i = j;
                }
            }
             pointId = i;
        }
         void FlyAway()
        {
            flying = true;
             Movement::MoveSplineInit init(me);
             G3D::Vector3 vertice0(
                CrowFlyPos[pointId].FirstCoord.x + irand(-4, 4),
                CrowFlyPos[pointId].FirstCoord.y + irand(-4, 4),
                CrowFlyPos[pointId].FirstCoord.z + irand(-4, 4));
            init.Path().push_back(vertice0);
             G3D::Vector3 vertice1(
                CrowFlyPos[pointId].SecondCoord.x + irand(-4, 4),
                CrowFlyPos[pointId].SecondCoord.y + irand(-4, 4),
                CrowFlyPos[pointId].SecondCoord.z + irand(-4, 4));
            init.Path().push_back(vertice1);
             init.SetFly();
            init.SetSmooth();
            init.SetVelocity(7.5f);
            init.SetUncompressed();
            init.Launch();
            init.DisableTransportPathTransformations();
            me->ForcedDespawn(10 * IN_MILLISECONDS);
        }
         void JustRespawned()
        {
            flying = false;
        }
         void MoveInLineOfSight(Unit* who)
        {
            if (!flying && who->GetTypeId() == TYPEID_PLAYER && me->IsWithinDistInMap(who, 15.0f))
                FlyAway();
        }
    };
     CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_gilnean_crow50260AI(creature);
    }
 };
 
// 34864
class npc_gilneas_city_guard_gate_34864 : public CreatureScript
{
public:
    npc_gilneas_city_guard_gate_34864() : CreatureScript("npc_gilneas_city_guard_gate_34864") { }

    enum eNpc
    {
        EVENT_START_TALK_WITH_CITIZEN = 101,
        EVENT_TALK_WITH_CITIZEN_1,
        EVENT_TALK_WITH_CITIZEN_2,
        EVENT_TALK_WITH_CITIZEN_3,
    };

    struct npc_gilneas_city_guard_gate_34864AI : public ScriptedAI
    {
        npc_gilneas_city_guard_gate_34864AI(Creature* creature) : ScriptedAI(creature) { }

        EventMap _events;
        uint8    _say;
        uint8    _emote;
        ObjectGuid     m_citicenGUID;

        void Reset() override
        {
            if (me->GetDistance2d(-1430.47f, 1345.55f) < 10.0f)
                _events.ScheduleEvent(EVENT_START_TALK_WITH_CITIZEN, urand(10000, 30000));
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_START_TALK_WITH_CITIZEN:
                    {
                        m_citicenGUID = GetRandomCitizen();
                        _emote = RAND(EMOTE_STATE_COWER, EMOTE_STATE_TALK, EMOTE_ONESHOT_CRY, EMOTE_STATE_SPELL_PRECAST, EMOTE_STATE_EXCLAIM);
                        _say = urand(0, 2);
                        if (Creature* npc = ObjectAccessor::GetCreature(*me, m_citicenGUID))
                            npc->HandleEmoteCommand(_emote);
                        _events.ScheduleEvent(EVENT_TALK_WITH_CITIZEN_1, urand(1200, 2000));
                        break;
                    }
                    case EVENT_TALK_WITH_CITIZEN_1:
                    {
                        if (Creature* npc = ObjectAccessor::GetCreature(*me, m_citicenGUID))
                            npc->AI()->Talk(_say);
                        _events.ScheduleEvent(EVENT_TALK_WITH_CITIZEN_2, 5000);
                        break;
                    }
                    case EVENT_TALK_WITH_CITIZEN_2:
                    {
                        Talk(_say);
                        _events.ScheduleEvent(EVENT_TALK_WITH_CITIZEN_3, 5000);
                        break;
                    }
                    case EVENT_TALK_WITH_CITIZEN_3:
                    {
                        if (Creature* npc = ObjectAccessor::GetCreature(*me, m_citicenGUID))
                            npc->HandleEmoteCommand(EMOTE_STATE_NONE);
                        _events.ScheduleEvent(EVENT_START_TALK_WITH_CITIZEN, urand(5000, 30000));
                        break;
                    }
                }
            }

            if (!UpdateVictim())
                return;

            DoMeleeAttackIfReady();
        }

        void FillCitizenList()
        {
            listOfCitizenGUID.clear();
            std::list<Creature*> listOfCitizen;
            me->GetCreatureListWithEntryInGrid(listOfCitizen, NPC_PANICKED_CITIZEN_GATE, 35.0f);
            for (std::list<Creature*>::iterator itr = listOfCitizen.begin(); itr != listOfCitizen.end(); ++itr)
                listOfCitizenGUID.push_back((*itr)->GetGUID());
        }

        ObjectGuid GetRandomCitizen()
        {
            if (listOfCitizenGUID.empty())
                FillCitizenList();
            uint8 rol = urand(0, listOfCitizenGUID.size() - 1);
            std::list<ObjectGuid>::iterator itr = listOfCitizenGUID.begin();
            std::advance(itr, rol);
            return (*itr);
        }

    private:
        std::list<ObjectGuid> listOfCitizenGUID;
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_gilneas_city_guard_gate_34864AI(creature);
    }
};

// 195327 Merchant Square Door(for Quest 14098)
class go_merchant_square_door_195327 : public GameObjectScript
{
public:
    go_merchant_square_door_195327() : GameObjectScript("go_merchant_square_door_195327") { }

    struct go_merchant_square_door_AI : public GameObjectAI
    {
        go_merchant_square_door_AI(GameObject* go) : GameObjectAI(go) { }

        EventMap   _events;
        uint8      _spawnTyp; // 0=unknown, 1=single, 2=both
        ObjectGuid _playerGUID;

        void Reset() override
        {
            _spawnTyp = 0;
            _playerGUID = ObjectGuid::Empty;
        }

        bool GossipHello(Player* player, bool isUse) override
        {
            // the gossip hello is called from different parts.. so we must prepare for calling twice
            if (go->GetGoState() != GOState::GO_STATE_READY /* door is open */ || player->GetGUID() != _playerGUID)
                _playerGUID = ObjectGuid::Empty;

            if (!_playerGUID)
                if (go->GetGoState() == GOState::GO_STATE_READY) /* door is closed */
                    if (player->GetQuestStatus(QUEST_EVACUATE_THE_MERCHANT_SQUARE) == QUEST_STATUS_INCOMPLETE)
                    {
                        // Spell 66639, SpellEffect ForceCast_160, is not working completely,
                        // so we emulate the triggered spell 67869
                        _playerGUID = player->GetGUID();
                        // SPELL_EFFECT_DUMMY: ID - 68087 Just Citizen
                        _spawnTyp = urand(1, 2);
                        if (_spawnTyp == 1)
                            SummonOnlyCitizen(player);
                        else
                            // SPELL_EFFECT_SCRIPT_EFFECT: ID - 68070 Summon Citizen and Worgen
                            SummonCitizenAndWorgen(player);

                        // SPELL_EFFECT_OPEN_LOCK: ID - 6477 Opening
                        _events.RescheduleEvent(EVENT_OPEN_DOOR, 1000);
                        _events.RescheduleEvent(EVENT_COOLDOWN_DOOR, 30000);
                    }

            return false;
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_COOLDOWN_DOOR:
                        _playerGUID = ObjectGuid::Empty;
                        break;
                    case EVENT_OPEN_DOOR:
                        if (Player* player = ObjectAccessor::GetPlayer(*go, _playerGUID))
                        {
                            player->CastSpell(go, 6477, true);
                            player->KilledMonsterCredit(NPC_GILNEAS_EVACUATION_FACING_MARKER_35830);
                        }
                        break;
                }
            }
        }

        void SummonOnlyCitizen(Player* player)
        {
            float x, y;
            go->GetNearPoint2D(x, y, 1.0f, go->GetOrientation() + M_PI);
            Position posC = Position(x, y, go->GetPositionZ());
            if (Creature* citizen = player->SummonCreature(NPC_FRIGHTENED_CITIZEN_34981, posC, TEMPSUMMON_TIMED_DESPAWN, 30000))
            {
                citizen->GetAI()->SetGUID(go->GetGUID(), go->GetEntry());
                citizen->GetAI()->SetGUID(player->GetGUID(), PLAYER_GUID);
                citizen->GetAI()->DoAction(ACTION_START_ANIM_CITIZEN);
            }
        }

        void SummonCitizenAndWorgen(Player* player)
        {
            float x1, y1;
            go->GetNearPoint2D(x1, y1, 1.0f, go->GetOrientation() + M_PI);
            Position posC = Position(x1, y1, go->GetPositionZ());
            float x2, y2;
            go->GetNearPoint2D(x2, y2, 3.0f, go->GetOrientation() + M_PI * 0.75f);
            Position posW = Position(x2, y2, go->GetPositionZ());
            if (Creature* citizen = player->SummonCreature(NPC_FRIGHTENED_CITIZEN_WORGEN, posC, TEMPSUMMON_TIMED_DESPAWN, 30000))
                if (Creature* worgen = player->SummonCreature(NPC_RAMPAGING_WORGEN_35660, posW, TEMPSUMMON_TIMED_DESPAWN, 30000))
                {
                    citizen->GetAI()->SetGUID(go->GetGUID(), go->GetEntry());
                    citizen->GetAI()->SetGUID(player->GetGUID(), PLAYER_GUID);
                    citizen->GetAI()->SetGUID(worgen->GetGUID(), worgen->GetEntry());
                    citizen->GetAI()->DoAction(ACTION_START_ANIM_CITIZEN);
                    worgen->GetAI()->SetGUID(go->GetGUID(), go->GetEntry());
                    worgen->GetAI()->SetGUID(player->GetGUID(), PLAYER_GUID);
                    worgen->GetAI()->SetGUID(citizen->GetGUID(), citizen->GetEntry());
                    worgen->GetAI()->DoAction(ACTION_START_ANIM_MERCANT);
                }
        }
    };

    GameObjectAI* GetAI(GameObject* go) const override
    {
        return new go_merchant_square_door_AI(go);
    }
};

// 35660 
class npc_rampaging_worgen_35660 : public CreatureScript
{
public:
    npc_rampaging_worgen_35660() : CreatureScript("npc_rampaging_worgen_35660") { }

    struct npc_rampaging_worgen_35660AI : public ScriptedAI
    {
        npc_rampaging_worgen_35660AI(Creature* creature) : ScriptedAI(creature) { }

        EventMap _events;
        bool _enrage;
        ObjectGuid _citizenGUID;
        ObjectGuid _doorGUID;

        void Reset() override
        {
            _enrage = false;
            _doorGUID = ObjectGuid::Empty;
            _citizenGUID = ObjectGuid::Empty;
            me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_NPC || UNIT_FLAG_IMMUNE_TO_PC);
        }

        void MovementInform(uint32 type, uint32 id) override
        {
            if (type == POINT_MOTION_TYPE)
                switch (id)
                {
                case MOVE_TO_DOOR:
                {
                    _events.ScheduleEvent(EVENT_FOLLOW_CITIZEN1, 1000);
                    break;
                }
                }
        }

        void DamageTaken(Unit* who, uint32 &damage) override
        {
            if (!_enrage && me->GetHealthPct() < 90.0f)
            {
                me->CastSpell(me, SPELL_ENRAGE_56646);
                _enrage = true;
                _events.ScheduleEvent(EVENT_ENRAGE_COOLDOWN, urand(121000, 150000));
            }
        }

        void SetGUID(ObjectGuid guid, int32 id = 0) override
        {
            switch (id)
            {
            case NPC_FRIGHTENED_CITIZEN_WORGEN:
            {
                _citizenGUID = guid;
                break;
            }
            case GO_MERCHANT_DOOR:
            {
                _doorGUID = guid;
                break;
            }
            }
        }

        void DoAction(int32 param) override
        {
            switch (param)
            {
            case ACTION_START_ANIM_MERCANT:
            {
                _events.ScheduleEvent(EVENT_MOVE_TO_DOOR, 3000);
                break;
            }
            }
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_ENRAGE_COOLDOWN:
                {
                    _enrage = false;
                    break;
                }
                case EVENT_MOVE_TO_DOOR:
                {
                    if (GameObject* go = ObjectAccessor::GetGameObject(*me, _doorGUID))
                        me->GetMotionMaster()->MovePoint(MOVE_TO_DOOR, go->GetPosition());
                    break;
                }
                case EVENT_FOLLOW_CITIZEN1:
                {
                    if (Player* player = me->SelectNearestPlayer(10.0f))
                        me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_NPC);
                    else if (Creature* citizen = ObjectAccessor::GetCreature(*me, _citizenGUID))
                        me->GetMotionMaster()->MoveFollow(citizen, 1.0f, 0.0f);

                    _events.ScheduleEvent(100, 5000);
                    break;
                }
                case 100:
                {
                    me->DespawnOrUnsummon();
                    break;
                }
                }
            }

            if (!UpdateVictim())
                return;
            else
                DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_rampaging_worgen_35660AI(creature);
    }
};

// 34981 // citizen alone
class npc_frightened_citizen_34981 : public CreatureScript
{
public:
    npc_frightened_citizen_34981() : CreatureScript("npc_frightened_citizen_34981") {}

    struct npc_frightened_citizen_34981AI : public ScriptedAI
    {
        npc_frightened_citizen_34981AI(Creature* creature) : ScriptedAI(creature) {}

        EventMap _events;
        ObjectGuid _playerGUID;
        ObjectGuid _doorGUID;

        void Reset() override
        {
            _playerGUID = ObjectGuid::Empty;
            _doorGUID = ObjectGuid::Empty;
            me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_NPC);
        }

        void MovementInform(uint32 type, uint32 point) override
        {
            if (type == POINT_MOTION_TYPE)
                switch (point)
                {
                case MOVE_TO_DOOR:
                {
                    Talk(1);
                    _events.ScheduleEvent(EVENT_MOVE_TO_MARKER, 4000);
                    break;
                }
                case MOVE_TO_MARKER:
                {
                    _events.ScheduleEvent(EVENT_MOVE_TO_END, 1);
                    break;
                }
                case MOVE_TO_END:
                {
                    me->DespawnOrUnsummon();
                    break;
                }
                }
        }

        void SetGUID(ObjectGuid guid, int32 id = 0) override
        {
            switch (id)
            {
            case PLAYER_GUID:
            {
                _playerGUID = guid;
                break;
            }
            case GO_MERCHANT_DOOR:
            {
                _doorGUID = guid;
                break;
            }
            }
        }

        void DoAction(int32 param) override
        {
            switch (param)
            {
            case ACTION_START_ANIM_CITIZEN:
            {
                me->HandleEmoteCommand(0);
                _events.ScheduleEvent(EVENT_MOVE_TO_DOOR, 2000);
                break;
            }
            }
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_MOVE_TO_DOOR:
                {
                    if (GameObject* go = ObjectAccessor::GetGameObject(*me, _doorGUID))
                        me->GetMotionMaster()->MovePoint(MOVE_TO_DOOR, go->GetPosition());
                    break;
                }
                case EVENT_MOVE_TO_MARKER:
                {
                    if (Creature* marker = me->FindNearestCreature(NPC_GILNEAS_EVACUATION_FACING_MARKER_35830, 100.0f))
                        me->GetMotionMaster()->MovePoint(MOVE_TO_MARKER, marker->GetPosition());
                    break;
                }
                case EVENT_MOVE_TO_END:
                {
                    if (Creature* marker = me->FindNearestCreature(NPC_GILNEAS_EVACUATION_FACING_MARKER_35010, 100.0f))
                        me->GetMotionMaster()->MovePoint(MOVE_TO_END, marker->GetPosition());
                    break;
                }
                }
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_frightened_citizen_34981AI(creature);
    }
};

// 35836 // citizen with worgen
class npc_frightened_citizen_35836 : public CreatureScript
{
public:
    npc_frightened_citizen_35836() : CreatureScript("npc_frightened_citizen_35836") {}

    struct npc_frightened_citizen_35836AI : public ScriptedAI
    {
        npc_frightened_citizen_35836AI(Creature* creature) : ScriptedAI(creature) {}

        EventMap _events;
        ObjectGuid _playerGUID;
        ObjectGuid _doorGUID;

        void Reset() override
        {
            _playerGUID = ObjectGuid::Empty;
            _doorGUID = ObjectGuid::Empty;
            me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_NPC);
        }

        void MovementInform(uint32 type, uint32 point) override
        {
            if (type == POINT_MOTION_TYPE)
                switch (point)
                {
                case MOVE_TO_DOOR:
                {
                    Talk(0);
                    _events.ScheduleEvent(EVENT_MOVE_TO_MARKER, 1000);
                    break;
                }
                case MOVE_TO_MARKER:
                {
                    _events.ScheduleEvent(EVENT_MOVE_TO_END, 1);
                    break;
                }
                case MOVE_TO_END:
                {
                    me->DespawnOrUnsummon();
                    break;
                }
                }
        }

        void SetGUID(ObjectGuid guid, int32 id = 0) override
        {
            switch (id)
            {
            case PLAYER_GUID:
            {
                _playerGUID = guid;
                break;
            }
            case GO_MERCHANT_DOOR:
            {
                _doorGUID = guid;
                break;
            }
            }
        }

        void DoAction(int32 param) override
        {
            switch (param)
            {
            case ACTION_START_ANIM_CITIZEN:
            {
                me->HandleEmoteCommand(0);
                _events.ScheduleEvent(EVENT_MOVE_TO_DOOR, 1000);
                break;
            }
            }
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_MOVE_TO_DOOR:
                {
                    if (GameObject* go = ObjectAccessor::GetGameObject(*me, _doorGUID))
                        me->GetMotionMaster()->MovePoint(MOVE_TO_DOOR, go->GetPosition());
                    break;
                }
                case EVENT_MOVE_TO_MARKER:
                {
                    if (Creature* marker = me->FindNearestCreature(NPC_GILNEAS_EVACUATION_FACING_MARKER_35830, 100.0f))
                        me->GetMotionMaster()->MovePoint(MOVE_TO_MARKER, marker->GetPosition());
                    break;
                }
                case EVENT_MOVE_TO_END:
                {
                    if (Creature* marker = me->FindNearestCreature(NPC_GILNEAS_EVACUATION_FACING_MARKER_35010, 100.0f))
                        me->GetMotionMaster()->MovePoint(MOVE_TO_END, marker->GetPosition());
                    break;
                }
                }
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_frightened_citizen_35836AI(creature);
    }
};

/* 35077 - QUEST: 14154 - By The Skin of His Teeth - START */
class npc_lord_darius_crowley_35077 : public CreatureScript
{
public:
    npc_lord_darius_crowley_35077() : CreatureScript("npc_lord_darius_crowley_35077") { }

    enum eNpc
    {
        ACTION_START_EVENT = 101,
    };

    bool OnQuestAccept(Player* player, Creature* creature, Quest const* quest) override
    {
        if (quest->GetQuestId() == 14154)
            if (CAST_AI(npc_lord_darius_crowley_35077AI, creature->AI())->_playerGUID.IsEmpty())
            {
                creature->AI()->SetGUID(player->GetGUID());
                creature->AI()->DoAction(ACTION_START_EVENT);
                creature->CastSpell(player, SPELL_BY_THE_SKIN, true);
            }
        return true;
    }

    struct npc_lord_darius_crowley_35077AI : public ScriptedAI
    {
        npc_lord_darius_crowley_35077AI(Creature* creature) : ScriptedAI(creature), _summons(me) { Init(); }

        enum eQ14154
        {
            Event120Secounds = 1,
            EventCheckPlayerIsAlive,
            EventSummonNextWave,
            EventHelpPlayer,
        };

        ObjectGuid _playerGUID;
        EventMap _events;
        SummonList _summons;
        uint32 _phase;

        void Init()
        {
            _events.Reset();
            _playerGUID = ObjectGuid::Empty;
            _summons.DespawnAll();
        }

        void JustSummoned(Creature* summoned) override
        {
            _summons.Summon(summoned);
        }

        void SummonedCreatureDies(Creature* summon, Unit* /*killer*/) override
        {
            _summons.Despawn(summon);
        }

        void DoAction(int32 /*action*/) override
        {
            _phase = 1;
            _events.ScheduleEvent(EventCheckPlayerIsAlive, 1000);
            _events.ScheduleEvent(EventSummonNextWave, 1000);
            _events.ScheduleEvent(Event120Secounds, 120000);
            _events.ScheduleEvent(EventHelpPlayer, 250);
        }

        void SetGUID(ObjectGuid guid, int32 /*type = 0*/) override
        {
            _playerGUID = guid;
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case Event120Secounds:
                        Init();
                        break;
                    case EventCheckPlayerIsAlive: // check every sec player is alive
                        if (!_playerGUID.IsEmpty() && _phase)
                            if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                                if (!player->IsInWorld() || !player->IsAlive())
                                    Init();

                        _events.ScheduleEvent(EventCheckPlayerIsAlive, 1000);
                        break;
                    case EventSummonNextWave:
                    {
                        for (int i = 0; i < 4; i++)
                        {
                            uint32 w1 = RAND(NPC_WORGEN_RUNT_C1, NPC_WORGEN_RUNT_C2, NPC_WORGEN_ALPHA_C1, NPC_WORGEN_ALPHA_C2);
                            uint32 w2 = RAND(NPC_WORGEN_RUNT_C1, NPC_WORGEN_RUNT_C2, NPC_WORGEN_ALPHA_C1, NPC_WORGEN_ALPHA_C2);
                            Creature* creature1 = me->SummonCreature(w1, -1610.39f, 1507.16f, 74.99f, 3.94f, TEMPSUMMON_TIMED_DESPAWN, 120000);
                            _summons.Summon(creature1);
                            creature1->AI()->SetGUID(_playerGUID);
                            creature1->AI()->DoAction(1);
                            Creature* creature2 = me->SummonCreature(w2, -1718.01f, 1516.81f, 55.40f, 4.6f, TEMPSUMMON_TIMED_DESPAWN, 120000);
                            _summons.Summon(creature2);
                            creature2->AI()->SetGUID(_playerGUID);
                            creature2->AI()->DoAction(2);
                        }

                        _events.ScheduleEvent(EventSummonNextWave, 30000); // every 30 secounds one wave
                        break;
                    }
                    case EventHelpPlayer:
                    {
                        if (!me->IsInCombat())
                        {
                            Creature* creature = nullptr;
                            creature = me->FindNearestCreature(NPC_WORGEN_RUNT_C1, 5.0f);
                            if (!creature)
                                creature = me->FindNearestCreature(NPC_WORGEN_RUNT_C2, 5.0f);
                            if (!creature)
                                creature = me->FindNearestCreature(NPC_WORGEN_ALPHA_C1, 5.0f);
                            if (!creature)
                                creature = me->FindNearestCreature(NPC_WORGEN_ALPHA_C2, 5.0f);
                            if (creature)
                            {
                                me->Attack(creature, true);
                                // creature->Attack(me, true);
                            }
                        }

                        _events.ScheduleEvent(EventHelpPlayer, 250);
                        break;
                    }
                }
            }

            if (!UpdateVictim())
                return;

            DoMeleeAttackIfReady();
        }

    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_lord_darius_crowley_35077AI(creature);
    }
};

// 35124
class npc_tobias_mistmantle_35124 : public CreatureScript
{
public:
    npc_tobias_mistmantle_35124() : CreatureScript("npc_tobias_mistmantle_35124") { }

    struct npc_tobias_mistmantle_35124AI : public ScriptedAI
    {
        npc_tobias_mistmantle_35124AI(Creature* creature) : ScriptedAI(creature) { }

        void UpdateAI(uint32 /*diff*/) override
        {
            if (!UpdateVictim())
                return;

            DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_tobias_mistmantle_35124AI(creature);
    }
};

// 35188
class npc_worgen_runt_35188 : public CreatureScript
{
public:
    npc_worgen_runt_35188() : CreatureScript("npc_worgen_runt_35188") {}

    struct npc_worgen_runt_35188AI : public ScriptedAI
    {
        npc_worgen_runt_35188AI(Creature* creature) : ScriptedAI(creature) { Init(); }

        ObjectGuid _playerGUID;
        EventMap _events;
        uint32 _phase;
        Position jump;
        Position JumpW1[3];
        Position LandingW1[3];
        Position LandingW2[4];
        float    _running;
        uint32   _hitCounter;

        void Init()
        {
            _phase = 0;
            _running = frand(4.2f, 5.6f);
            _hitCounter = 0;
            JumpW1[0] = Position(-1643.91f, 1482.96f, 63.22f, 3.95f);
            JumpW1[1] = Position(-1638.83f, 1478.07f, 65.36f, 3.84f);
            JumpW1[2] = Position(-1631.49f, 1475.02f, 65.64f, 3.84f);

            LandingW1[0] = Position(-1677.39f, 1455.52f, 52.29f, 4.06f);
            LandingW1[1] = Position(-1671.89f, 1449.27f, 52.29f, 3.84f);
            LandingW1[2] = Position(-1664.82f, 1443.62f, 52.29f, 3.84f);

            LandingW2[0] = Position(-1704.93f, 1469.07f, 52.29f, 5.34f);
            LandingW2[1] = Position(-1698.90f, 1472.92f, 52.29f, 5.41f);
            LandingW2[2] = Position(-1703.37f, 1470.66f, 52.29f, 5.49f);
            LandingW2[3] = Position(-1700.37f, 1473.32f, 52.29f, 5.41f);
        }

        void DoAction(int32 action) override
        {
            if (!_playerGUID.IsEmpty())
            {
                _events.ScheduleEvent(1, 500);
                _phase = action;
                me->SetSpeedRate(MOVE_RUN, _running);
                me->SetSpeedRate(MOVE_WALK, _running);
                me->SetWalk(false);
            }
        }

        void SetGUID(ObjectGuid guid, int32 /*type*/) override
        {
            _playerGUID = guid;
        }

        void MovementInform(uint32 type, uint32 pointId) override
        {
            if (type == POINT_MOTION_TYPE || type == EFFECT_MOTION_TYPE)
                _phase = pointId;
        }

        void DamageTaken(Unit* attacker, uint32& damage) override
        {
            if (attacker->GetEntry() == 35077 || attacker->GetEntry() == 35124)
            {
                _hitCounter += urand(1, 3);
                if (_hitCounter > 8)
                    attacker->Kill(me);
            }
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            uint32 eventId = _events.ExecuteEvent();
            switch (eventId)
            {
                case 1:
                {
                    _events.ScheduleEvent(1, 500);
                    DoWalk();
                    break;
                }
            }

            if (!UpdateVictim())
                return;

            DoMeleeAttackIfReady();
        }

        void DoWalk()
        {
            switch (_phase)
            {
                case 1:
                {
                    _phase = 3;
                    uint8 rol = urand(0, 2);
                    jump = JumpW1[rol];
                    me->GetMotionMaster()->MovePoint(11 + rol, jump);
                    break;
                }
                case 2:
                    _phase = 3;
                    jump = Position(-1717.73f, 1486.27f, 57.23f, 5.45f);
                    me->GetMotionMaster()->MovePoint(21, jump);
                    break;
                case 11:
                    _phase = 4;
                    me->GetMotionMaster()->MoveJump(LandingW1[0], frand(20.0f, 25.0f), frand(15.0f, 20.0f), 25);
                    break;
                case 12:
                    _phase = 4;
                    me->GetMotionMaster()->MoveJump(LandingW1[1], frand(20.0f, 25.0f), frand(15.0f, 20.0f), 25);
                    break;
                case 13:
                    _phase = 4;
                    me->GetMotionMaster()->MoveJump(LandingW1[2], frand(20.0f, 25.0f), frand(15.0f, 20.0f), 25);
                    break;
                case 21:
                {
                    _phase = 5;
                    uint8 rol = urand(0, 3);
                    jump = LandingW2[rol];
                    me->GetMotionMaster()->MoveJump(jump, frand(20.0f, 25.0f), frand(15.0f, 20.0f), 25);
                    break;
                }
                case 25:
                    _phase = 6;
                    if (!_playerGUID.IsEmpty())
                        if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                            if (player->IsInWorld() || player->IsAlive())
                            {
                                Position pos = player->GetNearPosition(frand(2.0f, 4.0f), frand(3.14f, 6.28f));
                                me->GetMotionMaster()->MovePoint(26, pos);
                            }
                    break;
                case 26:
                    _phase = 7;
                    me->SetHomePosition(me->GetPosition());
                    break;
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_worgen_runt_35188AI(creature);
    }
};

// 35456
class npc_worgen_runt_35456 : public CreatureScript
{
public:
    npc_worgen_runt_35456() : CreatureScript("npc_worgen_runt_35456") { }

    struct npc_worgen_runt_35456AI : public ScriptedAI
    {
        npc_worgen_runt_35456AI(Creature* creature) : ScriptedAI(creature) { Init(); }

        ObjectGuid _playerGUID;
        EventMap _events;
        uint32 _phase;
        Position jump;
        Position JumpW1[3];
        Position LandingW1[3];
        Position LandingW2[4];
        float    _running;
        uint32   _hitCounter;

        void Init()
        {
            _phase = 0;
            _running = frand(4.2f, 5.6f);
            _hitCounter = 0;
            JumpW1[0] = Position(-1643.91f, 1482.96f, 63.22f, 3.95f);
            JumpW1[1] = Position(-1638.83f, 1478.07f, 65.36f, 3.84f);
            JumpW1[2] = Position(-1631.49f, 1475.02f, 65.64f, 3.84f);

            LandingW1[0] = Position(-1677.39f, 1455.52f, 52.29f, 4.06f);
            LandingW1[1] = Position(-1671.89f, 1449.27f, 52.29f, 3.84f);
            LandingW1[2] = Position(-1664.82f, 1443.62f, 52.29f, 3.84f);

            LandingW2[0] = Position(-1704.93f, 1469.07f, 52.29f, 5.34f);
            LandingW2[1] = Position(-1698.90f, 1472.92f, 52.29f, 5.41f);
            LandingW2[2] = Position(-1703.37f, 1470.66f, 52.29f, 5.49f);
            LandingW2[3] = Position(-1700.37f, 1473.32f, 52.29f, 5.41f);
        }

        void DoAction(int32 action) override
        {
            if (!_playerGUID.IsEmpty())
            {
                _events.ScheduleEvent(1, 500);
                _phase = action;
                me->SetSpeedRate(MOVE_RUN, _running);
                me->SetSpeedRate(MOVE_WALK, _running);
                me->SetWalk(false);
            }
        }

        void SetGUID(ObjectGuid guid, int32 /*type*/) override
        {
            _playerGUID = guid;
        }

        void MovementInform(uint32 type, uint32 pointId) override
        {
            if (type == POINT_MOTION_TYPE || type == EFFECT_MOTION_TYPE)
                _phase = pointId;
        }

        void DamageTaken(Unit* attacker, uint32& damage) override
        {
            if (attacker->GetEntry() == 35077 || attacker->GetEntry() == 35124)
            {
                _hitCounter += urand(1, 3);
                if (_hitCounter > 8)
                    attacker->Kill(me);
            }
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            uint32 eventId = _events.ExecuteEvent();
            switch (eventId)
            {
            case 1:
            {
                _events.ScheduleEvent(1, 500);
                DoWalk();
                break;
            }
            }

            if (!UpdateVictim())
                return;

            DoMeleeAttackIfReady();
        }

        void DoWalk()
        {
            switch (_phase)
            {
                case 1:
                {
                    _phase = 3;
                    uint8 rol = urand(0, 2);
                    jump = JumpW1[rol];
                    me->GetMotionMaster()->MovePoint(11 + rol, jump);
                    break;
                }
                case 2:
                    _phase = 3;
                    jump = Position(-1717.73f, 1486.27f, 57.23f, 5.45f);
                    me->GetMotionMaster()->MovePoint(21, jump);
                    break;
                case 11:
                    _phase = 4;
                    me->GetMotionMaster()->MoveJump(LandingW1[0], frand(20.0f, 25.0f), frand(15.0f, 20.0f), 25);
                    break;
                case 12:
                    _phase = 4;
                    me->GetMotionMaster()->MoveJump(LandingW1[1], frand(20.0f, 25.0f), frand(15.0f, 20.0f), 25);
                    break;
                case 13:
                    _phase = 4;
                    me->GetMotionMaster()->MoveJump(LandingW1[2], frand(20.0f, 25.0f), frand(15.0f, 20.0f), 25);
                    break;
                case 21:
                {
                    _phase = 5;
                    uint8 rol = urand(0, 3);
                    jump = LandingW2[rol];
                    me->GetMotionMaster()->MoveJump(jump, frand(20.0f, 25.0f), frand(15.0f, 20.0f), 25);
                    break;
                }
                case 25:
                    _phase = 6;
                    if (!_playerGUID.IsEmpty())
                        if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                            if (player->IsInWorld() || player->IsAlive())
                            {
                                Position pos = player->GetNearPosition(frand(2.0f, 4.0f), frand(3.14f, 6.28f));
                                me->GetMotionMaster()->MovePoint(26, pos);
                            }
                    break;
                case 26:
                    _phase = 7;
                    me->SetHomePosition(me->GetPosition());
                    break;
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_worgen_runt_35456AI(creature);
    }
};

// 35170
class npc_worgen_alpha_35170 : public CreatureScript
{
public:
    npc_worgen_alpha_35170() : CreatureScript("npc_worgen_alpha_35170") {}

    struct npc_worgen_alpha_35170AI : public ScriptedAI
    {
        npc_worgen_alpha_35170AI(Creature* creature) : ScriptedAI(creature) { Init(); }

        ObjectGuid _playerGUID;
        EventMap _events;
        uint32 _phase;
        Position jump;
        Position JumpW1[3];
        Position LandingW1[3];
        Position LandingW2[4];
        float    _running;
        uint32   _hitCounter;

        void Init()
        {
            _phase = 0;
            _running = frand(4.2f, 5.6f);
            _hitCounter = 0;
            JumpW1[0] = Position(-1643.91f, 1482.96f, 63.22f, 3.95f);
            JumpW1[1] = Position(-1638.83f, 1478.07f, 65.36f, 3.84f);
            JumpW1[2] = Position(-1631.49f, 1475.02f, 65.64f, 3.84f);

            LandingW1[0] = Position(-1677.39f, 1455.52f, 52.29f, 4.06f);
            LandingW1[1] = Position(-1671.89f, 1449.27f, 52.29f, 3.84f);
            LandingW1[2] = Position(-1664.82f, 1443.62f, 52.29f, 3.84f);

            LandingW2[0] = Position(-1704.93f, 1469.07f, 52.29f, 5.34f);
            LandingW2[1] = Position(-1698.90f, 1472.92f, 52.29f, 5.41f);
            LandingW2[2] = Position(-1703.37f, 1470.66f, 52.29f, 5.49f);
            LandingW2[3] = Position(-1700.37f, 1473.32f, 52.29f, 5.41f);
        }

        void DoAction(int32 action) override
        {
            if (!_playerGUID.IsEmpty())
            {
                _events.ScheduleEvent(1, 500);
                _phase = action;
                me->SetSpeedRate(MOVE_RUN, _running);
                me->SetSpeedRate(MOVE_WALK, _running);
                me->SetWalk(false);
            }
        }

        void SetGUID(ObjectGuid guid, int32 /*type*/) override
        {
            _playerGUID = guid;
        }

        void MovementInform(uint32 type, uint32 pointId) override
        {
            if (type == POINT_MOTION_TYPE || type == EFFECT_MOTION_TYPE)
                _phase = pointId;
        }

        void DamageTaken(Unit* attacker, uint32& damage) override
        {
            if (attacker->GetEntry() == 35077 || attacker->GetEntry() == 35124)
            {
                _hitCounter += urand(1, 3);
                if (_hitCounter > 8)
                    attacker->Kill(me);
            }
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            uint32 eventId = _events.ExecuteEvent();
            switch (eventId)
            {
                case 1:
                {
                    _events.ScheduleEvent(1, 500);
                    DoWalk();
                    break;
                }
            }

            if (!UpdateVictim())
                return;

            DoMeleeAttackIfReady();
        }

        void DoWalk()
        {
            switch (_phase)
            {
                case 1:
                {
                    _phase = 3;
                    uint8 rol = urand(0, 2);
                    jump = JumpW1[rol];
                    me->GetMotionMaster()->MovePoint(11 + rol, jump);
                    break;
                }
                case 2:
                    _phase = 3;
                    jump = Position(-1717.73f, 1486.27f, 57.23f, 5.45f);
                    me->GetMotionMaster()->MovePoint(21, jump);
                    break;
                case 11:
                    _phase = 4;
                    me->GetMotionMaster()->MoveJump(LandingW1[0], frand(20.0f, 25.0f), frand(15.0f, 20.0f), 25);
                    break;
                case 12:
                    _phase = 4;
                    me->GetMotionMaster()->MoveJump(LandingW1[1], frand(20.0f, 25.0f), frand(15.0f, 20.0f), 25);
                    break;
                case 13:
                    _phase = 4;
                    me->GetMotionMaster()->MoveJump(LandingW1[2], frand(20.0f, 25.0f), frand(15.0f, 20.0f), 25);
                    break;
                case 21:
                {
                    _phase = 5;
                    uint8 rol = urand(0, 3);
                    jump = LandingW2[rol];
                    me->GetMotionMaster()->MoveJump(jump, frand(20.0f, 25.0f), frand(15.0f, 20.0f), 25);
                    break;
                }
                case 25:
                    _phase = 6;
                    if (!_playerGUID.IsEmpty())
                        if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                            if (player->IsInWorld() || player->IsAlive())
                            {
                                Position pos = player->GetNearPosition(frand(2.0f, 4.0f), frand(3.14f, 6.28f));
                                me->GetMotionMaster()->MovePoint(26, pos);
                            }
                    break;
                case 26:
                    _phase = 7;
                    me->SetHomePosition(me->GetPosition());
                    break;
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_worgen_alpha_35170AI(creature);
    }
};

// 35167
class npc_worgen_alpha_35167 : public CreatureScript
{
public:
    npc_worgen_alpha_35167() : CreatureScript("npc_worgen_alpha_35167") {}

    struct npc_worgen_alpha_35167AI : public ScriptedAI
    {
        npc_worgen_alpha_35167AI(Creature* creature) : ScriptedAI(creature) { Init(); }

        ObjectGuid _playerGUID;
        EventMap _events;
        uint32 _phase;
        Position jump;
        Position JumpW1[3];
        Position LandingW1[3];
        Position LandingW2[4];
        float    _running;
        uint32   _hitCounter;

        void Init()
        {
            _phase = 0;
            _running = frand(4.2f, 5.6f);
            _hitCounter = 0;
            JumpW1[0] = Position(-1643.91f, 1482.96f, 63.22f, 3.95f);
            JumpW1[1] = Position(-1638.83f, 1478.07f, 65.36f, 3.84f);
            JumpW1[2] = Position(-1631.49f, 1475.02f, 65.64f, 3.84f);

            LandingW1[0] = Position(-1677.39f, 1455.52f, 52.29f, 4.06f);
            LandingW1[1] = Position(-1671.89f, 1449.27f, 52.29f, 3.84f);
            LandingW1[2] = Position(-1664.82f, 1443.62f, 52.29f, 3.84f);

            LandingW2[0] = Position(-1704.93f, 1469.07f, 52.29f, 5.34f);
            LandingW2[1] = Position(-1698.90f, 1472.92f, 52.29f, 5.41f);
            LandingW2[2] = Position(-1703.37f, 1470.66f, 52.29f, 5.49f);
            LandingW2[3] = Position(-1700.37f, 1473.32f, 52.29f, 5.41f);
        }

        void DoAction(int32 action) override
        {
            if (!_playerGUID.IsEmpty())
            {
                _events.ScheduleEvent(1, 500);
                _phase = action;
                me->SetSpeedRate(MOVE_RUN, _running);
                me->SetSpeedRate(MOVE_WALK, _running);
                me->SetWalk(true);
            }
        }

        void SetGUID(ObjectGuid guid, int32 /*type = 0*/) override
        {
            _playerGUID = guid;
        }

        void MovementInform(uint32 type, uint32 pointId) override
        {
            if (type == POINT_MOTION_TYPE || type == EFFECT_MOTION_TYPE)
                _phase = pointId;
        }

        void DamageTaken(Unit* attacker, uint32& damage) override
        {
            if (attacker->GetEntry() == 35077 || attacker->GetEntry() == 35124)
            {
                _hitCounter += urand(1, 3);
                if (_hitCounter > 8)
                    attacker->Kill(me);
            }
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            uint32 eventId = _events.ExecuteEvent();
            switch (eventId)
            {
            case 1:
            {
                _events.ScheduleEvent(1, 500);
                DoWalk();
                break;
            }
            }

            if (!UpdateVictim())
                return;

            DoMeleeAttackIfReady();
        }

        void DoWalk()
        {
            switch (_phase)
            {
                case 1:
                {
                    _phase = 3;
                    uint8 rol = urand(0, 2);
                    jump = JumpW1[rol];
                    me->GetMotionMaster()->MovePoint(11 + rol, jump);
                    break;
                }
                case 2:
                    _phase = 3;
                    jump = Position(-1717.73f, 1486.27f, 57.23f, 5.45f);
                    me->GetMotionMaster()->MovePoint(21, jump);
                    break;
                case 11:
                    _phase = 4;
                    me->GetMotionMaster()->MoveJump(LandingW1[0], frand(20.0f, 25.0f), frand(15.0f, 20.0f), 25);
                    break;
                case 12:
                    _phase = 4;
                    me->GetMotionMaster()->MoveJump(LandingW1[1], frand(20.0f, 25.0f), frand(15.0f, 20.0f), 25);
                    break;
                case 13:
                    _phase = 4;
                    me->GetMotionMaster()->MoveJump(LandingW1[2], frand(20.0f, 25.0f), frand(15.0f, 20.0f), 25);
                    break;
                case 21:
                {
                    _phase = 5;
                    uint8 rol = urand(0, 3);
                    jump = LandingW2[rol];
                    me->GetMotionMaster()->MoveJump(jump, frand(20.0f, 25.0f), frand(15.0f, 20.0f), 25);
                    break;
                }
                case 25:
                    _phase = 6;
                    if (!_playerGUID.IsEmpty())
                        if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                            if (player->IsInWorld() || player->IsAlive())
                            {
                                Position pos = player->GetNearPosition(frand(2.0f, 4.0f), frand(3.14f, 6.28f));
                                me->GetMotionMaster()->MovePoint(26, pos);
                            }
                    break;
                case 26:
                    _phase = 7;
                    me->SetHomePosition(me->GetPosition());
                    break;
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_worgen_alpha_35167AI(creature);
    }
};

// Part of 35369 
class JosiahAveryDeleteOutDoors
{
public:
    JosiahAveryDeleteOutDoors() { }

    bool operator()(Player* player)
    {
        if (player->GetMap()->IsOutdoors(player))
            return true;
        if (player->GetQuestStatus(QUEST_THE_REBEL_LORDS_ARSENAL) != QUEST_STATUS_COMPLETE)
            return true;

        return false;
    }
};

// 35369
class npc_josiah_avery_35369 : public CreatureScript
{
public:
    npc_josiah_avery_35369() : CreatureScript("npc_josiah_avery_35369") {}

    bool OnQuestReward(Player* player, Creature* creature, Quest const* quest, uint32 /*opt*/) override
    {
        if (quest->ID == QUEST_THE_REBEL_LORDS_ARSENAL)
        {
            if (Pet* pet = player->GetPet())
                player->RemovePet(pet, PET_SAVE_CURRENT_STATE, true);
            creature->AddAura(SPELL_WORGEN_BITE, player); // add phase 171 to..
            creature->AI()->SetGUID(player->GetGUID(), PLAYER_GUID);
            creature->AI()->DoAction(EVENT_CHECK_ORIENTATION);
            return true;
        }
        return false;
    }

    struct npc_josiah_avery_35369AI : public ScriptedAI
    {
        npc_josiah_avery_35369AI(Creature* creature) : ScriptedAI(creature) {}

        EventMap   _events;
        uint32     _currentSayCounter; // Current Say
        ObjectGuid _playerGUID;

        void Reset() override
        {
            _playerGUID = ObjectGuid::Empty;
            _events.RescheduleEvent(EVENT_SAY_JOSIAH_AVERY, 20000);
            _events.RescheduleEvent(EVENT_CHECK_ORIENTATION, 5000);
            _currentSayCounter = 0;
        }

        void SetGUID(ObjectGuid guid, int32 type) override
        {
            switch (type)
            {
                case PLAYER_GUID:
                    _playerGUID = guid;
                    break;
            }
        }

        void DoAction(int32 param) override
        {
            switch (param)
            {
            case EVENT_CHECK_ORIENTATION:
            {
                _events.RescheduleEvent(EVENT_CHECK_ORIENTATION, 5000);
                break;
            }
            }
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                    case EVENT_SAY_JOSIAH_AVERY:
                    {
                        _currentSayCounter += 1;
                        if (_currentSayCounter > 5)
                            _currentSayCounter = 1;

                        SharedVisionList pList = GetListOfPlayersNearAndIndoorsAndWithQuest();
                        TalkToGroup(pList, _currentSayCounter);

                        _events.ScheduleEvent(EVENT_SAY_JOSIAH_AVERY, 20000);
                        break;
                    }
                    case EVENT_CHECK_ORIENTATION:
                    {
                        _playerGUID = ObjectGuid::Empty;
                        me->SetOrientation(3.857f);
                        break;
                    }
                }
            }
        }

        std::list<Player*> GetListOfPlayersNearAndIndoorsAndWithQuest()
        {
            SharedVisionList pList = me->FindNearestPlayers(50.0f);
            pList.remove_if(JosiahAveryDeleteOutDoors()); 
            return pList;
        }

        void TalkToGroup(SharedVisionList pList, uint8 say)
        {
            if (pList.empty())
                return;

            for (SharedVisionList::const_iterator itr = pList.begin(); itr != pList.end(); itr++)
                Talk(say, (*itr));
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_josiah_avery_35369AI(creature);
    }
};

// 35370
class npc_bad_josiah_avery_35370 : public CreatureScript
{
public:
    npc_bad_josiah_avery_35370() : CreatureScript("npc_bad_josiah_avery_35370") {}

    struct npc_bad_josiah_avery_35370AI : public ScriptedAI
    {
        npc_bad_josiah_avery_35370AI(Creature* creature) : ScriptedAI(creature) {}

        void Reset() override
        {
            PhasingHandler::RemovePhase(me, 170);
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_bad_josiah_avery_35370AI(creature);
    }
};

// 50415
class npc_josiah_avery_trigger_50415 : public CreatureScript
{
public:
    npc_josiah_avery_trigger_50415() : CreatureScript("npc_josiah_avery_trigger_50415") {}

    struct npc_josiah_avery_trigger_50415AI : public ScriptedAI
    {
        npc_josiah_avery_trigger_50415AI(Creature* creature) : ScriptedAI(creature) {}

        EventMap   _events;
        ObjectGuid _playerGUID;
        ObjectGuid _badAveryGUID;
        ObjectGuid _lornaGUID;

        void Reset() override
        {
            _playerGUID = ObjectGuid::Empty;
            _badAveryGUID = ObjectGuid::Empty;
            _lornaGUID = ObjectGuid::Empty;
            me->SetDisplayId(11686); // not visible 11686
            PhasingHandler::RemovePhase(me, 170);
            _events.RescheduleEvent(EVENT_START_ANIM, 1000);
        }

        void IsSummonedBy(Unit* summoner) override
        {
            if (Player* player = summoner->ToPlayer())
                _playerGUID = player->GetGUID();

            _events.RescheduleEvent(EVENT_START_ANIM, 25);
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_START_ANIM:
                {
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        Talk(1, player); // Tell Player they have been bitten
                    _events.ScheduleEvent(EVENT_ANIM_1, 200);
                    break;
                }
                case EVENT_ANIM_1:
                {
                    FindLornaAndAvery();
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        if (Creature* badAvery = ObjectAccessor::GetCreature(*me, _badAveryGUID))
                        {
                            badAvery->SetFacingToObject(player); // Face Player
                            badAvery->CastSpell(player, SPELL_COSMETIC_COMBAT_ATTACK, true); // Do Cosmetic Attack
                            player->GetMotionMaster()->MoveKnockTo(-1791.94f, 1427.29f, 12.4584f, 22.0f, 8.0f, 1005);
                            badAvery->getThreatManager().resetAllAggro();
                        }
                    _events.ScheduleEvent(EVENT_ANIM_2, 1200);
                    break;
                }
                case EVENT_ANIM_2:
                {
                    if (Creature* badAvery = ObjectAccessor::GetCreature(*me, _badAveryGUID))
                        badAvery->GetMotionMaster()->MoveJump(-1794.89f, 1427.86f, 12.4584f, 0.0f, 18.0f, 7.0f);
                    _events.ScheduleEvent(EVENT_ANIM_3, 600);
                    break;
                }
                case EVENT_ANIM_3:
                {
                    if (Creature* badAvery = ObjectAccessor::GetCreature(*me, _badAveryGUID))
                        if (Creature* lorna = ObjectAccessor::GetCreature(*me, _lornaGUID))
                        {
                            DoStartNoMovement(lorna);
                            lorna->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_NPC);
                            lorna->CastSpell(badAvery, SPELL_SHOOT, true);
                        }

                    _events.ScheduleEvent(EVENT_ANIM_4, 200);
                    break;
                }
                case EVENT_ANIM_4:
                {
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        if (Creature* badAvery = ObjectAccessor::GetCreature(*me, _badAveryGUID))
                        {
                            badAvery->CastSpell(badAvery, SPELL_GET_SHOT, true);
                            badAvery->setDeathState(JUST_DIED);
                            badAvery->DespawnOrUnsummon(5000);
                            me->DespawnOrUnsummon(1000);
                        }
                    break;
                }
                }
            }
        }

        void FindLornaAndAvery()
        {
            if (_lornaGUID.IsEmpty())
                if (Creature* lorna = me->FindNearestCreature(NPC_LORNA_CROWLEY_35378, 60.0f, true))
                    _lornaGUID = lorna->GetGUID();

            if (_badAveryGUID.IsEmpty())
                if (Creature* badAvery = me->FindNearestCreature(NPC_JOSIAH_AVERY_35370, 60.0f, true))
                    _badAveryGUID = badAvery->GetGUID();
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_josiah_avery_trigger_50415AI(creature);
    }
};

// 35378
class npc_lorna_crowley_35378 : public CreatureScript
{
public:
    npc_lorna_crowley_35378() : CreatureScript("npc_lorna_crowley_35378") {}

    bool OnQuestAccept(Player* player, Creature* creature, Quest const* quest) override
    {
        if (quest->GetQuestId() == QUEST_FROM_THE_SHADOWS)
        {
            Pet* pet = player->GetPet();
            if (pet)
                player->RemovePet(pet, PET_SAVE_CURRENT_STATE, true);
            creature->CastSpell(player, SPELL_FORCECAST_MASTIFF, true);
            creature->AI()->Talk(0);
            pet = player->GetPet();
//            if (!pet)
  //              player->CastSpell(player, SPELL_SUMMON_GILNEAN_MASTIFF, true);
        }
        return true;
    }
};

// 35631
class npc_gilnean_mastiff_35631 : public CreatureScript
{
public:
    npc_gilnean_mastiff_35631() : CreatureScript("npc_gilnean_mastiff_35631") { }

    struct npc_gilnean_mastiff_35631AI : public ScriptedAI
    {
        npc_gilnean_mastiff_35631AI(Creature* creature) : ScriptedAI(creature) { }

        EventMap _events;
        ObjectGuid _playerGUID;
        ObjectGuid _lurkerGUID;

        void Reset() override
        {
            if (CharmInfo* info = me->GetCharmInfo())
            {
                info->InitEmptyActionBar(false);
                info->SetActionBar(0, SPELL_ATTACK_LURKER, ACT_PASSIVE);
                me->SetReactState(REACT_DEFENSIVE);
                info->SetIsFollowing(true);
            }
            
            _events.Reset();
            _events.ScheduleEvent(EVENT_CHECK_QUEST_REWARDED, 10000);
        }

        void IsSummonedBy(Unit* summoner) override
        {
            if (Player* player = summoner->ToPlayer())
                _playerGUID = player->GetGUID();
        }

        void SpellHitTarget(Unit* mastiff, SpellInfo const* cSpell) override
        {
            printf("Mastiff: SpellHitTarget %u \n", cSpell->Id);
        }

        void JustDied(Unit* /*killer*/) override // Otherwise, player is stuck with pet corpse they cannot remove from world
        {
            me->DespawnOrUnsummon(1);
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_CHECK_QUEST_REWARDED:
                {
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        if (player->GetQuestStatus(QUEST_FROM_THE_SHADOWS) != QUEST_STATUS_INCOMPLETE)
                            me->DespawnOrUnsummon(1);

                    _events.ScheduleEvent(EVENT_CHECK_QUEST_REWARDED, 10000);
                    break;
                }
                }
            }

            if (!UpdateVictim())
                return;
            else
                DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_gilnean_mastiff_35631AI(creature);
    }
};

// king 35550 do control of sparring attack

// 35905
class npc_king_greymanes_horse_35905 : public CreatureScript
{
public:
    npc_king_greymanes_horse_35905() : CreatureScript("npc_king_greymanes_horse_35905") { }

    struct npc_king_greymanes_horse_35905AI : public VehicleAI
    {
        npc_king_greymanes_horse_35905AI(Creature* creature) : VehicleAI(creature) { }

        EventMap     _events;
        uint32       _wayPoint;
        ObjectGuid   _playerGUID;
        ObjectGuid   _aranasGUID;

        void Reset() override
        {
            _wayPoint = 0;
            _playerGUID = ObjectGuid::Empty;
            _aranasGUID = ObjectGuid::Empty;
            me->SetSpeedRate(UnitMoveType::MOVE_WALK, 12.0f);
            me->SetOrientation(4.902f);
        }

        void PassengerBoarded(Unit* passenger, int8 seatId, bool apply) override
        {
            if (apply)
            {
                if (Player* player = passenger->ToPlayer())
                {
                    _playerGUID = player->GetGUID();
                    _wayPoint = WAYPOINT_KING_HORSE_PART1;
                    me->GetMotionMaster()->MovePath(WAYPOINT_KING_HORSE_PART1, false);
                }
                else if (Creature* npc = passenger->ToCreature())
                {
                    if (npc->GetEntry() == NPC_KRENNAN_ARANAS)
                    {
                        _aranasGUID = npc->GetGUID();
                        _wayPoint = WAYPOINT_KING_HORSE_PART3;
                        me->GetMotionMaster()->MovePath(WAYPOINT_KING_HORSE_PART3, false);
                    }
                }
            }
            else 
            {
                // horse and aranas are despawned from core..
                _events.ScheduleEvent(EVENT_HORSE_DISMOUNT, 10);
            }
        }

        void MovementInform(uint32 type, uint32 id) override
        {
            switch (type)
            {
            case EFFECT_MOTION_TYPE:
                switch (id)
                {
                case 1100: // jump from stairway down to aranas
                    _events.ScheduleEvent(EVENT_KING_HORSE_START2, 10);
                    break;
                }
                break;
            case WAYPOINT_MOTION_TYPE:
                switch (_wayPoint)
                {
                case WAYPOINT_KING_HORSE_PART1:
                    if (id == 8)
                        _events.ScheduleEvent(EVENT_KING_HORSE_JUMP, 10);
                    break;
                case WAYPOINT_KING_HORSE_PART2:
                    if (id == 1)
                        _events.ScheduleEvent(EVENT_WAIT_SAVE_ARANAS, 10);
                    break;
                case WAYPOINT_KING_HORSE_PART3:
                    if (id == 13)
                        _events.ScheduleEvent(EVENT_KING_HORSE_END, 10);
                    break;
                }
                break;
            }
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_KING_HORSE_JUMP:
                {
                    me->GetMotionMaster()->MoveJump(-1687.103f, 1344.571f, 15.134f, 6.247f, 30.0f, 15.0f, 1100);
                    break;
                }
                case EVENT_KING_HORSE_START2:
                {
                    _wayPoint = WAYPOINT_KING_HORSE_PART2;
                    me->GetMotionMaster()->MovePath(WAYPOINT_KING_HORSE_PART2, false);
                    break;
                }
                case EVENT_WAIT_SAVE_ARANAS:
                {
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        Talk(0, player); // Big Message: rescue aranas
                    _wayPoint = 0;
                    break;
                }
                case EVENT_KING_HORSE_END:
                    if (Vehicle* vehicle = me->GetVehicleKit())
                        vehicle->RemoveAllPassengers();

                    break;
                case EVENT_HORSE_DISMOUNT:
                    if (Creature* aranas = ObjectAccessor::GetCreature(*me, _aranasGUID))
                        aranas->GetAI()->DoAction(ACTION_TELEPORT_NEAR);

                    break;
                }
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_king_greymanes_horse_35905AI(creature);
    }
};

// 35907
class npc_krennan_aranas_35907 : public CreatureScript
{
public:
    npc_krennan_aranas_35907() : CreatureScript("npc_krennan_aranas_35907") { }

    struct npc_krennan_aranas_35907AI : public ScriptedAI
    {
        npc_krennan_aranas_35907AI(Creature* creature) : ScriptedAI(creature) { }

        EventMap     _events;
        ObjectGuid   _playerGUID;
        ObjectGuid   _horseGUID;

        void Reset() override
        {
            _playerGUID = ObjectGuid::Empty;
            _horseGUID = ObjectGuid::Empty;
        }

        void AttackStart(Unit* /*who*/) override {}
        void EnterCombat(Unit* /*who*/) override {}
        void EnterEvadeMode(EvadeReason /*reason*/) override {}
             
        void IsSummonedBy(Unit* summoner) override
        {
            if (Player* player = summoner->ToPlayer())
                if (Vehicle* vHorse = player->GetVehicle())
                    if (Unit* horse = vHorse->GetBase())
                    {
                        _playerGUID = player->GetGUID();
                        _horseGUID = horse->GetGUID();
                        DoCast(horse, SPELL_RIDE_BUNNY_SEAT2, true);
                    }
        }

        void DoAction(int32 param) override
        {
            switch (param)
            {
            case ACTION_TELEPORT_NEAR: 
                _events.ScheduleEvent(EVENT_TELEPORT_NEAR, 10);
                break;
            }
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_TELEPORT_NEAR:
                {
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                    {
                        Position t1 = me->GetNearPosition(3.0f, 1.57f);
                        Position t2 = me->GetNearPosition(1.0f, 4.71f);
                        player->NearTeleportTo(t1.GetPositionX(), t1.GetPositionY(), t1.GetPositionZ(), player->GetOrientation());
                        me->NearTeleportTo(t2.GetPositionX(), t2.GetPositionY(), t2.GetPositionZ(), player->GetOrientation());

                        _events.ScheduleEvent(EVENT_TALK_00, 1000);
                    }
                    break;
                }
                case EVENT_TALK_00:
                {
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        Talk(0, player); // aranas say thanks

                    _events.ScheduleEvent(EVENT_HORSE_UNSUMMON, 8000);
                    break;
                }
                case EVENT_HORSE_UNSUMMON:
                {
                    me->DespawnOrUnsummon(10);
                    break;
                }
                }
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_krennan_aranas_35907AI(creature);
    }
};

// 67063
class spell_throw_torch_67063 : public SpellScriptLoader
{
public:
    spell_throw_torch_67063() : SpellScriptLoader("spell_throw_torch_67063") { }

    class FriendlyCheck
    {
    public:
        FriendlyCheck(Unit* caster) : _caster(caster) { }

        bool operator()(WorldObject* object) const
        {
            if (Unit* unit = object->ToUnit())
                return !unit->IsHostileTo(_caster);
            return true;
        }

    private:
        Unit* _caster;
    };

    class IsNotStalker
    {
    public:
        IsNotStalker() {}

        bool operator()(WorldObject* object) const
        {
            if (Unit* unit = object->ToUnit())
                return unit->GetEntry() != NPC_BLOODFANG_STALKER;
            return true;
        }
    };

    class spell_throw_torch_67063_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_throw_torch_67063_SpellScript);

        void FilterTargets(std::list<WorldObject*>& targets)
        {
            targets.remove_if(IsNotStalker()); // (FriendlyCheck(GetCaster()));
            uint8 rol = urand(3, 8);
            while (targets.size() > rol)
                targets.erase(targets.begin());
        }

        void Register() override
        {
            OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_throw_torch_67063_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENTRY);
            OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_throw_torch_67063_SpellScript::FilterTargets, EFFECT_1, TARGET_UNIT_DEST_AREA_ENTRY);
            OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_throw_torch_67063_SpellScript::FilterTargets, EFFECT_2, TARGET_UNIT_DEST_AREA_ENTRY);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_throw_torch_67063_SpellScript();
    }
};


/* Phase 1024 - START */

// 35566
class npc_lord_darius_crowley_35566 : public CreatureScript
{
public:
    npc_lord_darius_crowley_35566() : CreatureScript("npc_lord_darius_crowley_35566") {}

    // ID - 72799 Last Stand Complete (bind 4786) 72788 95758 95759 72792 68992 68975 68978 68976

    bool OnQuestAccept(Player* player, Creature* /*creature*/, Quest const* quest) override
    {
        if (quest->GetQuestId() == QUEST_LAST_STAND)
        {
            if (player->HasAura(SPELL_INFECTED_BITE))
                player->RemoveAura(SPELL_INFECTED_BITE);
            player->AddAura(SPELL_HIDEOUS_BITE_WOUND, player);
            return true;
        }
        return false;
    }

    bool OnQuestReward(Player* player, Creature* creature, const Quest *_Quest, uint32 /*opt*/) override
    {
        if (_Quest->GetQuestId() == QUEST_LAST_STAND)
        {
            player->RemoveAura(SPELL_HIDEOUS_BITE_WOUND);
            player->CastSpell(player, SPELL_ALTERED_FORM, true);
            player->CastSpell(player, SPELL_FORCE_WORGEN_ALTERED_FORM, true);
            player->CastSpell(player, SPELL_ALTERED_FORM2, true);
            creature->AI()->SetGUID(player->GetGUID(), PLAYER_GUID);
            creature->AI()->DoAction(EVENT_START_MOVIE);
            return true;
        }
        return false;
    }

    struct npc_lord_darius_crowley_35566AI : public ScriptedAI
    {
        npc_lord_darius_crowley_35566AI(Creature* creature) : ScriptedAI(creature) {  }

        EventMap _events;
        ObjectGuid   _playerGUID;

        void Reset() override
        {
            _playerGUID = ObjectGuid::Empty;
        }

        void SetGUID(ObjectGuid guid, int32 id) override
        {
            switch (id)
            {
                case PLAYER_GUID:
                {
                    _playerGUID = guid;
                    break;
                }
            }
        }

        void DoAction(int32 param) override
        {
            switch (param)
            {
                case EVENT_START_MOVIE:
                {
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        player->CastSpell(player, SPELL_CURSE_OF_THE_WORGEN, true);
                    _events.RescheduleEvent(EVENT_START_MOVIE, 2000);
                    break;
                }
            }
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {

                case EVENT_START_MOVIE:
                {
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                    {
                        player->CastSpell(player, 72799, true);
                    }
                    break;
                }
                }
            }

            if (!UpdateVictim())
                return;
            else
                DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_lord_darius_crowley_35566AI(creature);
    }
};

// next part in script zone_duskhaven

void AddSC_zone_gilneas_city1()
{
    new npc_gilneas_city_guard_gate_34864();
    new go_merchant_square_door_195327();
    new npc_rampaging_worgen_35660();
    new npc_frightened_citizen_34981();
    new npc_frightened_citizen_35836();
    new npc_tobias_mistmantle_35124();
    new npc_lord_darius_crowley_35077();
    new npc_worgen_runt_35188();
    new npc_worgen_alpha_35170();
    new npc_worgen_runt_35456();
    new npc_worgen_alpha_35167();
    new npc_josiah_avery_35369();
    new npc_bad_josiah_avery_35370();
    new npc_josiah_avery_trigger_50415();
    new npc_lorna_crowley_35378();
    new npc_gilnean_mastiff_35631();
    new npc_king_greymanes_horse_35905();
    new npc_krennan_aranas_35907();

    new spell_throw_torch_67063();
    new npc_lord_darius_crowley_35566();
    new npc_gilnean_crow();
};

