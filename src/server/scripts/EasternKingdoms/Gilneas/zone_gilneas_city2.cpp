/*
 * Copyright (C) 2019 PPA-Core.
 * Copyright (C) 2008-2014 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2011-2016 ArkCORE <http://www.arkania.net/>
 * Copyright (C) 2005-2009 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "CreatureTextMgr.h"
#include "CreatureGroups.h"
#include "GameObject.h"
#include "GameObjectAI.h"
#include "MoveSplineInit.h"
#include "MotionMaster.h"
#include "ObjectAccessor.h"
#include "PassiveAI.h"
#include "PhasingHandler.h"
#include "Player.h"
#include "ScriptMgr.h"
#include "ScriptedGossip.h"
#include "ScriptedCreature.h"
#include "ScriptedEscortAI.h"
#include "ScriptedFollowerAI.h"
#include "SpellAuraEffects.h"
#include "SpellMgr.h"
#include "SpellScript.h"
#include "Spell.h"
#include "SpellInfo.h"
#include "Unit.h"
#include "TemporarySummon.h"
#include "Vehicle.h"
#include "WorldSession.h"
#include "zone_gilneas.h"

// 72050
class spell_fiery_boulder_72050 : public SpellScriptLoader
{
public:
    spell_fiery_boulder_72050() : SpellScriptLoader("spell_fiery_boulder_72050") { }

    class IsFriendly
    {
    public:
        explicit IsFriendly(Unit const* caster) : _caster(caster) { }

        bool operator()(WorldObject* obj) const
        {
            if (Unit* unit = obj->ToUnit())
                return _caster->IsFriendlyTo(unit);

            return false;
        }

    private:
        Unit const* _caster;
    };

    class spell_fiery_boulder_72050_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_fiery_boulder_72050_SpellScript);

        void FilterTargets(std::list<WorldObject*>& unitList)
        {
            unitList.remove_if(IsFriendly(GetCaster()));
        }

        void Register() override
        {
            OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_fiery_boulder_72050_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENEMY);
            OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_fiery_boulder_72050_SpellScript::FilterTargets, EFFECT_1, TARGET_UNIT_DEST_AREA_ENEMY);

        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_fiery_boulder_72050_SpellScript();
    }
};

// 72051
class spell_fiery_boulder_72051 : public SpellScriptLoader
{
public:
    spell_fiery_boulder_72051() : SpellScriptLoader("spell_fiery_boulder_72051") { }

    class spell_fiery_boulder_72051_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_fiery_boulder_72051_SpellScript);

        void SetDest(SpellDestination& dest)
        {
            if (WorldLocation* loc = const_cast<WorldLocation*>(GetExplTargetDest()))
                dest._position = loc->GetWorldLocation();
        }

        void Register() override
        {
            OnDestinationTargetSelect += SpellDestinationTargetSelectFn(spell_fiery_boulder_72051_SpellScript::SetDest, EFFECT_0, TARGET_DEST_TRAJ);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_fiery_boulder_72051_SpellScript();
    }
};

// 71388
class spell_rapier_of_the_gilnean_patriots_71388 : public SpellScriptLoader
{
public:
    spell_rapier_of_the_gilnean_patriots_71388() : SpellScriptLoader("spell_rapier_of_the_gilnean_patriots_71388") { }

    class IsHostile
    {
    public:
        explicit IsHostile(Unit const* caster) : _caster(caster) { }

        bool operator()(WorldObject* obj) const
        {
            if (Unit* unit = obj->ToUnit())
                return _caster->IsHostileTo(unit);

            return false;
        }

    private:
        Unit const* _caster;
    };

    class spell_rapier_of_the_gilnean_patriots_71388_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_rapier_of_the_gilnean_patriots_71388_SpellScript);

        void FilterTargets(std::list<WorldObject*>& unitList)
        {
            unitList.remove_if(IsHostile(GetCaster()));
        }

        void Register() override
        {
            OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_rapier_of_the_gilnean_patriots_71388_SpellScript::FilterTargets, EFFECT_0, TARGET_UNIT_DEST_AREA_ENTRY);
            OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_rapier_of_the_gilnean_patriots_71388_SpellScript::FilterTargets, EFFECT_1, TARGET_UNIT_DEST_AREA_ENTRY);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_rapier_of_the_gilnean_patriots_71388_SpellScript();
    }
};

// 72116
class spell_shoot_liam_72116 : public SpellScriptLoader
{
public:
    spell_shoot_liam_72116() : SpellScriptLoader("spell_shoot_liam_72116") { }

    class spell_shoot_liam_72116_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_shoot_liam_72116_SpellScript);

        void CheckTarget(WorldObject*& target)
        {
            if (target)
                if (target->GetEntry() != 38474)
                    target = target->FindNearestCreature(38474, 25.0f);
        }

        void Register() override
        {
            OnObjectTargetSelect += SpellObjectTargetSelectFn(spell_shoot_liam_72116_SpellScript::CheckTarget, EFFECT_0, TARGET_UNIT_NEARBY_ENTRY);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_shoot_liam_72116_SpellScript();
    }
};

void AddSC_zone_gilneas_city2()
{
    new spell_rapier_of_the_gilnean_patriots_71388();
    new spell_fiery_boulder_72050();
    new spell_fiery_boulder_72051();
    new spell_shoot_liam_72116();
}

