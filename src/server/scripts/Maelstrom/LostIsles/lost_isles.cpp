/*
 * Copyright (C) 2019 PPA-Core.
 * Copyright (C) 2011 - 2012 ArkCORE <http://www.arkania.net/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#include "CellImpl.h"
#include "Creature.h"
#include "GameObject.h"
#include "GameObjectAI.h"
#include "GridNotifiers.h"
#include "GridNotifiersImpl.h"
#include "Item.h"
#include "PhasingHandler.h"
#include "Player.h"
#include "ScriptMgr.h"
#include "ScriptedEscortAI.h"
#include "ScriptedGossip.h"
#include "SpellAuraEffects.h"
#include "SpellAuras.h"
#include "SpellMgr.h"
#include "SpellScript.h"
#include "Vehicle.h"
#include "WorldSession.h"

enum DontGoIntoTheLight
{
    NPC_GIZMO                           = 36600,
    SPELL_DEAD_STILL                    = 69010,

    SPELL_DGITL_QUEST_COMPLETE          = 69013,
    SPELL_DGITL_SUMMON_DOC_ZAPNOZZLE    = 69018,
    SPELL_DGITL_JUMP_CABLES             = 69022,
    SPELL_DGITL_DESPAWN_DOC_ZAPNOZZLE   = 69043,
};

// 69010
class spell_lost_isles_near_death : public SpellScript
{
    PrepareSpellScript(spell_lost_isles_near_death);

    void HandleHit(SpellEffIndex /*effIndex*/)
    {
        GetCaster()->CastSpell(nullptr, SPELL_DGITL_SUMMON_DOC_ZAPNOZZLE, true);
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_lost_isles_near_death::HandleHit, EFFECT_0, SPELL_EFFECT_APPLY_AURA);
    }
};

enum GoblinEscapePod
{
    SPELL_SUMMON_LIVE_GOBLIN_SURVIVOR   = 66137,
    SPELL_TRADE_PRINCE_CONTROLLER_AURA  = 67433,
    SPELL_SUMMON_TRADE_PRINCE           = 67845,

    NCP_TRADE_PRINCE_GALLYWIX           = 35649
};

// 195188
class gob_goblin_escape_pod : public GameObjectAI
{
public:
    gob_goblin_escape_pod(GameObject* go) : GameObjectAI(go) { }

    void OnStateChanged(uint32 state, Unit* unit) override
    {
        if (unit && state == GO_ACTIVATED)
        {
            if (Player* player = unit->ToPlayer())
            {
                if (!player->HasAura(SPELL_TRADE_PRINCE_CONTROLLER_AURA))
                {
                    player->CastSpell(nullptr, SPELL_SUMMON_TRADE_PRINCE, true);
                    player->CastSpell(nullptr, SPELL_TRADE_PRINCE_CONTROLLER_AURA, true);
                }
                else
                {
                    player->CastSpell(nullptr, SPELL_SUMMON_LIVE_GOBLIN_SURVIVOR, true);
                }

                player->KilledMonsterCredit(34748);
                go->DestroyForPlayer(player);
            }
        }
    }
};

// 34748
// 35649
struct npc_goblin_espace_pod : public ScriptedAI
{
    npc_goblin_espace_pod(Creature* creature) : ScriptedAI(creature) {}

    Position const beachPosition = { 617.317383f, 3142.259277f, -0.114589f, 2.176555f };

    void Reset() override
    {
        me->SetFlying(true);
        me->SetSwim(true);

        me->GetScheduler().Schedule(1s, [this](TaskContext /*context*/)
        {
            Player* nearestPlayer = me->SelectNearestPlayer(20.f);

            if (me->GetEntry() == NCP_TRADE_PRINCE_GALLYWIX)
                Talk(0, nearestPlayer);
            else
                Talk(urand(0, 3), nearestPlayer);

        }).Schedule(2s, [this](TaskContext /*context*/)
        {
            me->GetMotionMaster()->MovePoint(1, beachPosition);
            me->DespawnOrUnsummon(3000);
        });
    }
};

enum LostIslesWeed
{
    QUEST_WEED_WHAKER       = 14236,
    SPELL_WEED_WHACKER_AURA = 68212
};

class spell_weed_whacker : public SpellScript
{
    PrepareSpellScript(spell_weed_whacker);

    void HandleDummy(SpellEffIndex /*effIndex*/)
    {
        if (GetCastItem())
        {
            if (Unit* caster = GetCastItem()->GetOwner())
            {
                if (Player* player = caster->ToPlayer())
                {
                    if (player->HasAura(SPELL_WEED_WHACKER_AURA))
                        player->RemoveAura(SPELL_WEED_WHACKER_AURA);
                    else if (player->GetQuestStatus(QUEST_WEED_WHAKER) == QUEST_STATUS_INCOMPLETE)
                        player->CastSpell(player, SPELL_WEED_WHACKER_AURA, true);
                }
            }
        }
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_weed_whacker::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
    }
};

enum Infrared
{
    NPC_ORC_SCOUT = 36100,
};

// 68338
class aura_infrared_orc_scout : public AuraScript
{
    PrepareAuraScript(aura_infrared_orc_scout);

    void OnRemove(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
    {
        if (Creature* orcScout = GetTarget()->GetSummonedCreatureByEntry(NPC_ORC_SCOUT))
            orcScout->DespawnOrUnsummon();
    }

    void Register() override
    {
        AfterEffectRemove += AuraEffectRemoveFn(aura_infrared_orc_scout::OnRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
    }
};

enum PreciousCargo
{
    QUEST_PRECIOUS_CARO                 = 14242,
    QUEST_MEET_ME_UP_TOP                = 14326,

    SPELL_PRECIOUS_CARGO_QUEST_ACCEPT   = 68386,
    SPELL_PRECIOUS_CARGO_KILL_CREDIT    = 69081,

    NPC_GYROCOPTER                      = 36143,

    GOB_ARCANE_CAGE                     = 195704,
};

// 36127
struct npc_gyrocopterequest_giver : public ScriptedAI
{
    npc_gyrocopterequest_giver(Creature* creature) : ScriptedAI(creature) {}

    void sQuestAccept(Player* player, const Quest* quest) override
    {
        if (quest->GetQuestId() == QUEST_PRECIOUS_CARO)
        {
            player->CastSpell(player, SPELL_PRECIOUS_CARGO_QUEST_ACCEPT, true);

            if (Creature* gyrocopter = player->GetSummonedCreatureByEntry(NPC_GYROCOPTER))
                CAST_AI(npc_escortAI, gyrocopter->AI())->Start(false, true, player->GetGUID(), quest);
        }
    }
};

// 36143
struct npc_precious_cargo_gyrocopter : public npc_escortAI
{
    npc_precious_cargo_gyrocopter(Creature* creature) : npc_escortAI(creature) {}

    void AttackStart(Unit* /*who*/) override {}
    void EnterCombat(Unit* /*who*/) override {}
    void EnterEvadeMode(EvadeReason /*why*/) override {}
    void OnCharmed(bool /*apply*/) override {}

    void Reset() override
    {
        me->SetCanFly(true);
        me->SetSpeedRate(MOVE_FLIGHT, 21.0f);
    }

    void WaypointReached(uint32 pointId) override
    {
        if (pointId == 2)
        {
            if (Player* player = GetPlayerForEscort())
                me->CastSpell(player, SPELL_PRECIOUS_CARGO_KILL_CREDIT, true);

            me->DespawnOrUnsummon();
        }
    }
};

// 36145
struct npc_lost_isles_thrall_prisonner : public ScriptedAI
{
    npc_lost_isles_thrall_prisonner(Creature* creature) : ScriptedAI(creature) {}

    void MoveInLineOfSight(Unit* who) override
    {
        if (Player* player = who->ToPlayer())
            if (player->GetQuestStatus(QUEST_PRECIOUS_CARO) == QUEST_STATUS_INCOMPLETE)
                if (player->GetDistance(me) < 5.f)
                    player->KilledMonsterCredit(me->GetEntry());
    }

    void sQuestAccept(Player* player, const Quest* /*quest*/) override
    {
        Talk(0, player);
    }
};

enum UpUpAndAway
{
    QUEST_UP_UP_AND_AWAY            = 14244,
    
    SPELL_ROCKET_BLAST              = 66110,
    SPELL_SUMMON_CHARACTER_ROCKET   = 68806,
    SPELL_UUAA_KILL_CREDIT          = 68813,
    SPELL_SUMMON_GALLYWIX           = 68815,
    SPELL_SUMMON_GALLYWIX_ROCKET    = 68819,

    NPC_GALLYWIX_ROCKET             = 36514
};

// 71091
class spell_tiab_effect2 : public SpellScript
{
    PrepareSpellScript(spell_tiab_effect2);

    void HandleDummy(SpellEffIndex effIndex)
    {
        PreventHitDefaultEffect(effIndex);

        if (Unit* unit = GetHitUnit())
            if (Player* casterPlayer = GetCaster()->ToPlayer())
                casterPlayer->KilledMonsterCredit(38024);
    }

    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_tiab_effect2::HandleDummy, EFFECT_0, SPELL_EFFECT_FORCE_CAST);
    }
};

class npc_poule : public CreatureScript
{
public:
    npc_poule() : CreatureScript("npc_poule") { }

    struct npc_pouleAI : public ScriptedAI
    {
        npc_pouleAI(Creature* creature) : ScriptedAI(creature) {}

        void Reset() override
        {
            me->GetMotionMaster()->MoveRandom(10.0f);
        }

        void DoAction(const int32 /*param*/) override
        {
            me->CastSpell(me, 67919, true);
        }

        void SpellHit(Unit* /*caster*/, const SpellInfo* spell) override
        {
            if (spell->Id == 67917)
            {
                if (me->IsNonMeleeSpellCast(true))
                    me->InterruptNonMeleeSpells(true);
                if (Creature *t = me->SummonCreature(me->GetEntry(), me->GetPositionX(), me->GetPositionY(),  me->GetPositionZ(),
                                                     me->GetOrientation(), TEMPSUMMON_MANUAL_DESPAWN))
                {
                    t->AI()->DoAction(0);
                    t->DespawnOrUnsummon(3000);
                }
                me->DespawnOrUnsummon();
            }
        }

        void UpdateAI(uint32 /*diff*/) override
        {
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_pouleAI (creature);
    }
};

#define GO_PIEGE 201972

class spell_egg_gob : public SpellScriptLoader
{
public:
    spell_egg_gob() : SpellScriptLoader("spell_egg_gob") { }

    class spell_egg_gobSpellScript : public SpellScript
    {
        PrepareSpellScript(spell_egg_gobSpellScript);

        bool Validate(SpellInfo const* /*spellEntry*/) override
        {
            return true;
        }


        bool Load() override
        {
            if (Unit* caster = GetCastItem()->GetOwner())
            {
                if (GameObject* go = caster->FindNearestGameObject(GO_PIEGE, 5))
                {
                    if (Creature *c = go->FindNearestCreature(38187, 10))
                        c->AI()->DoAction(1);
                }
            }
            return true;
        }


        void HandleActivateGameobject(SpellEffIndex /*effIndex*/)
        {

        }


        void Register() override
        {
            OnEffectHit += SpellEffectFn(spell_egg_gobSpellScript::HandleActivateGameobject,EFFECT_0,SPELL_EFFECT_ACTIVATE_OBJECT);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_egg_gobSpellScript();
    }
};


class SparkSearcher
{
public:
    SparkSearcher(Creature const* source, float range) : _source(source), _range(range) {}

    bool operator()(Unit* unit)
    {
        if (!unit->IsAlive())
            return false;

        switch (unit->GetEntry())
        {
            case 38318:
                break;
            default:
                return false;
        }

        if (!unit->IsWithinDist(_source, _range, false))
            return false;

        return true;
    }

private:
    Creature const* _source;
    float _range;
};



#define SPELL_CRACK_INVOK 72058

// (Chef des nagas)
#define NAGA_SAY_A "WHO OSE?"
#define NAGA_SAY_B "Small goblins? I remember the creation of your race."
#define NAGA_SAY_C "Your threats do not impress me, nor the naga. Get ready to disappear from this reality."
#define NAGA_SAY_D  "Now, young $ N, you will die!"
//                    (Crack)
//-Quand on rend la quete d'avant
#define QUEST_RENDER_CRACK "I want to let these little demons, $ N. Nagas will never attack until we have grandchildren."

//-Quand on commence la quete
#define QUEST_RESET_CRACK "You are $ gpret, ready to force their leader to get $ gmy friend: my friend; ?"

//Quand on se rend syur place
#define CRACK_PROVOC "Come on, Chief naga, come out of your hiding and surrendered for $ N and Bilgewater Cartel!"
#define CRACK_EVADE "Hula treasure, it smells bad. I am size!"

#define NPC_CRACK 39198

class YoungNagaSearcher
{
public:
    YoungNagaSearcher(Creature const* source, float range) : _source(source), _range(range) {}

    bool operator()(Creature* creature)
    {
        if (!creature->IsAlive())
            return false;

        switch (creature->GetEntry())
        {
            case 44580:
                break;
            case 44579:
                break;
            case 44578:
                break;
            case 38412:
                break;
            default:
                return false;
        }

        if (!creature->IsWithinDist(_source, _range, false))
            return false;

        return true;
    }

private:
    Creature const* _source;
    float _range;
};


class spell_boot_gob : public SpellScriptLoader
{
public:
    spell_boot_gob() : SpellScriptLoader("spell_boot_gob") { }


    class spell_boot_gobSpellScript : public SpellScript
    {
        PrepareSpellScript(spell_boot_gobSpellScript);

        bool Validate(SpellInfo const* /*spellEntry*/) override
        {
            st = false;
            return true;
        }


        bool Load() override
        {
            return true;
        }

        void HandleOnHit()
        {
            if (Unit* caster = GetCastItem()->GetOwner())
                if (caster->GetTypeId() == TYPEID_PLAYER)
                    caster->ToPlayer()->GetMotionMaster()->MoveJump(1480.31f, 1269.97f, 110.0f, 50.0f, 50.0f, 300.0f);
        }

    private :
        bool st;

        void Register() override
        {
            OnHit += SpellHitFn(spell_boot_gobSpellScript::HandleOnHit);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_boot_gobSpellScript();
    }
};



class npc_tremblement_volcano : public CreatureScript
{
public:
    npc_tremblement_volcano() : CreatureScript("npc_tremblement_volcano") { }

    struct npc_tremblement_volcanoAI : public ScriptedAI
    {
        npc_tremblement_volcanoAI(Creature* creature) : ScriptedAI(creature) {}

        void AttackStart(Unit* /*who*/) override {}
        void EnterCombat(Unit* /*who*/) override {}
        void EnterEvadeMode(EvadeReason /*why*/) override {}

        void Reset() override
        {
            mui_soufle = 2000;
        }

        void JustDied(Unit* /*killer*/) override
        {
        }

        void UpdateAI(uint32 diff) override
        {
            if (mui_soufle <= diff)
            {
                me->CastSpell(me, 69235, true);
                mui_soufle = 3000;
            }
            else
                mui_soufle -= diff;
        }

    private :
        uint32 mui_soufle;

    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_tremblement_volcanoAI (creature);
    }
};

class npc_meteor2_gob : public CreatureScript
{
public:
    npc_meteor2_gob() : CreatureScript("npc_meteor2_gob") { }

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_meteor2_gobAI(creature);
    }

    struct npc_meteor2_gobAI : public ScriptedAI
    {
        npc_meteor2_gobAI(Creature* creature) : ScriptedAI(creature) {}

        void Reset() override
        {
            _a = urand(15000, 20200);
            _b = 600000;
            _c = 600000;
        }

        void JustReachedHome() override
        {

        }

        void UpdateAI(uint32 diff) override
        {
            if (_a <= diff)
            {
                me->CastSpell(me, 93668, true);
                _a = urand(15000, 20200);
                _b = 800;
            }
            else _a -= diff;
            if (_b <= diff)
            {
                me->CastSpell(me, 87701, true);
                _b = 600000;
                _c = 500;
            }
            else _b -= diff;
            if (_c <= diff)
            {
                me->CastSpell(me, 69235, true);
                _c = 600000;
            }
            else _c -= diff;
        }

    private :
        uint32 _a, _b, _c;
    };
};

class npc_explosion_volcano : public CreatureScript
{
public:
    npc_explosion_volcano() : CreatureScript("npc_explosion_volcano") { }

    struct npc_explosion_volcanoAI : public ScriptedAI
    {
        npc_explosion_volcanoAI(Creature* creature) : ScriptedAI(creature) {}

        void AttackStart(Unit* /*who*/) override {}
        void EnterCombat(Unit* /*who*/) override {}
        void EnterEvadeMode(EvadeReason /*why*/) override {}

        void Reset() override
        {
            mui_soufle = urand(1100, 2000);
        }

        void JustDied(Unit* /*killer*/) override
        {
        }

        void UpdateAI(uint32 diff) override
        {
            if (mui_soufle <= diff)
            {
                me->CastSpell(me, 73193, true);
                mui_soufle = urand(4000, 5200);
            }
            else
                mui_soufle -= diff;
        }

    private :
        uint32 mui_soufle;

    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_explosion_volcanoAI (creature);
    }
};





class BootSearcher
{
public:
    bool operator()(WorldObject* object)
    {
        if (!object)
            return true;
        Unit* unit = object->ToUnit();
        if (!unit || !unit->IsAlive() || unit->GetTypeId() == TYPEID_PLAYER)
            return true;

        if (unit->ToCreature())
        {
            switch (unit->ToCreature()->GetEntry())
            {
                case 38753:
                    return false;
                default:
                    break;
            }
        }
        return true;
    }
};

void AddSC_lost_isle()
{
    RegisterSpellScript(spell_lost_isles_near_death);
    RegisterGameObjectAI(gob_goblin_escape_pod);
    RegisterCreatureAI(npc_goblin_espace_pod);
    RegisterSpellScript(spell_weed_whacker);
    RegisterAuraScript(aura_infrared_orc_scout);
    RegisterCreatureAI(npc_gyrocopterequest_giver);
    RegisterCreatureAI(npc_precious_cargo_gyrocopter);
    RegisterCreatureAI(npc_lost_isles_thrall_prisonner);
    RegisterSpellScript(spell_tiab_effect2);
    new npc_poule();
    new spell_boot_gob();
    new npc_meteor2_gob();
    new npc_tremblement_volcano();
    new npc_explosion_volcano();
}
