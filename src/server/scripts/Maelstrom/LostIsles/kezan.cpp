/*
 * Copyright (C) 2019 PPA-Core.
 * Copyright (C) 2010 - 2012 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2011 - 2012 ArkCORE <http://www.arkania.net/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptMgr.h"
#include "ScriptedEscortAI.h"
#include "SpellMgr.h"
#include "PhasingHandler.h"
#include "Player.h"
#include "Item.h"
#include "GameObject.h"
#include "Creature.h"
#include "Vehicle.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "SpellScript.h"
#include "SpellInfo.h"
#include "WorldSession.h"

enum eKezan
{
    NPC_KEZAN_PARTYGOER_KILL_CREDIT = 35186,
    NPC_SZABO_35128 = 35128,
    NPC_MISSA_SPEKKIES_35130 = 35130,
    NPC_GAPPY_SILVERTOOTH_35126 = 35126,
    NPC_BILGEWATER_BUCCANEER_37213 = 37213,

    GO_KAJAMITE_DEPOSIT_202593 = 202593,
    GO_KAJAMITE_CHUNK_195492 = 195492,

    QUEST_THE_NEW_YOU_14110 = 14110,
    QUEST_GOOD_HELP_IS_HARD_TO_FIND = 14069,
    QUEST_LIFE_OF_THE_PARTY_14153 = 14153,
    QUEST_LIFE_OF_THE_PARTY_14113 = 14113,
    QUEST_NECESSARY_ROUGHNESS_24502 = 24502,
    QUEST_FOURTH_AND_GOAL_28414 = 28414,

    SOUND_RADIO = 23406,

    SPELL_RAGE = 45111,
    SPELL_SLEEP = 62248,
    SPELL_LIGHTNING_VISUAL = 45870,
    SPELL_OUTFIT = 66781,
    SPELL_COOL_SHADE = 66782,
    SPELL_SHINY_BLING = 66780,
    SPELL_DRINK = 66909,
    SPELL_WATER = 66910,
    SPELL_DANCE = 66911,
    SPELL_FIRE = 66912,
    SPELL_FOOD = 66913,

    AAAA = 1000,
    ACCEPT_EVENT,
    ACTION_A,
    ACTION_B,
    ACTION_C,
    ACTION_D,
    ACTION_E,
    ACTION_START,
    ACTION_FAIL,
    ACTION_SUCCESS,

};

const Position SharkPos[8] =
{
    { -8278.0f, 1485.0f, 43.14f, 6.28f},
    { -8285.0f, 1485.0f, 43.9f, 6.28f},
    { -8285.61f, 1489.94f, 43.9f, 6.28f},
    { -8285.76f, 1480.03f, 43.9f, 6.28f},
    { -8290.68f, 1475.38f, 43.95f, 6.28f},
    { -8291.06f, 1480.00f, 43.98f, 6.28f},
    { -8290.98f, 1487.85f ,43.99f, 6.28f},
    { -8290.90f, 1492.98f, 43.9f, 6.28f},
};

// 35120
class npc_fbok_bank_teller_35120 : public CreatureScript
{
public:
    npc_fbok_bank_teller_35120() : CreatureScript("npc_fbok_bank_teller_35120") { }

    bool OnGossipHello(Player* player, Creature* creature) override
    {
        if (player->GetQuestStatus(QUEST_THE_NEW_YOU_14110) == QUEST_STATUS_INCOMPLETE)
        {
            if (creature->GetEntry() == NPC_SZABO_35128)
                player->CastSpell(player, SPELL_OUTFIT, true);
            if (creature->GetEntry() == NPC_MISSA_SPEKKIES_35130)
                player->CastSpell(player, SPELL_COOL_SHADE, true);
            if (creature->GetEntry() == NPC_GAPPY_SILVERTOOTH_35126)
                player->CastSpell(player, SPELL_SHINY_BLING, true);
            return true;
        }
        return false;
    }

    struct npc_fbok_bank_teller_35120AI : public ScriptedAI
    {
        npc_fbok_bank_teller_35120AI(Creature* creature) : ScriptedAI(creature) {}
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_fbok_bank_teller_35120AI(creature);
    }
};


// 195525
class gob_bank_gobelin : public GameObjectScript
{
public:
    gob_bank_gobelin() : GameObjectScript("gob_bank_gobelin") { }

    bool OnGossipHello(Player* player, GameObject* go) override
    {
        if (player->GetQuestStatus(14122) == QUEST_STATUS_INCOMPLETE)
        {
            if (Creature *t = player->SummonCreature(35486, player->GetPositionX(), player->GetPositionY(), player->GetPositionZ(), player->GetOrientation(), TEMPSUMMON_CORPSE_TIMED_DESPAWN, 300 * IN_MILLISECONDS))
                PhasingHandler::AddPhase(t, 180);
            if (Creature *t = player->SummonCreature(850000, go->GetPositionX(), go->GetPositionY(),  go->GetPositionZ(),  player->GetOrientation(), TEMPSUMMON_CORPSE_TIMED_DESPAWN, 300*IN_MILLISECONDS))
                PhasingHandler::AddPhase(t, 180);
            return true;
        }
        return true;
    }

    bool OnGossipSelect(Player* player, GameObject* /*go*/, uint32 /*sender*/, uint32 /*action*/) override
    {
        player->PlayerTalkClass->ClearMenus();
        CloseGossipMenuFor(player);
        return true;
    }
};

// 67682
class spell_kabummbomb : public SpellScriptLoader
{
public:
    spell_kabummbomb() : SpellScriptLoader("spell_kabummbomb") { }

    class spell_kabummbombSpellScript : public SpellScript
    {
        PrepareSpellScript(spell_kabummbombSpellScript);

        bool Validate(SpellInfo const* /*spellEntry*/) override
        {
            return true;
        }

        bool Load() override
        {
            return true;
        }

        void HandleActivateGameobject(SpellEffIndex /*effIndex*/)
        {
            if (Unit* caster = GetCastItem()->GetOwner())
            {
                if (caster->ToPlayer()->GetQuestStatus(14124) != QUEST_STATUS_INCOMPLETE)
                    return;
                if (GameObject* Deposit = caster->FindNearestGameObject(GO_KAJAMITE_DEPOSIT_202593, 20))
                {
                    caster->SummonGameObject(GO_KAJAMITE_CHUNK_195492, Deposit->GetPositionX()-1, Deposit->GetPositionY(), Deposit->GetPositionZ()+1, 0, QuaternionData(), 0);
                    caster->SummonGameObject(GO_KAJAMITE_CHUNK_195492, Deposit->GetPositionX()-1, Deposit->GetPositionY()-1, Deposit->GetPositionZ()+1, 0, QuaternionData(), 0);
                    caster->SummonGameObject(GO_KAJAMITE_CHUNK_195492, Deposit->GetPositionX()-1, Deposit->GetPositionY()+1, Deposit->GetPositionZ()+1, 0, QuaternionData(), 0);
                    Deposit->Delete();
                }
            }
        }

        void Register() override
        {
            OnEffectHit += SpellEffectFn(spell_kabummbombSpellScript::HandleActivateGameobject,EFFECT_0,SPELL_EFFECT_ACTIVATE_OBJECT);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_kabummbombSpellScript();
    }
};

// 35075
class npc_kezan_citizen_35075 : public CreatureScript
{
public:
    npc_kezan_citizen_35075() : CreatureScript("npc_kezan_citizen_35075") { }

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_kezan_citizen_35075AI(creature);
    }

    struct npc_kezan_citizen_35075AI : public ScriptedAI
    {
        npc_kezan_citizen_35075AI(Creature* creature) : ScriptedAI(creature) {}


        void Reset() override
        {
            _s = urand(3000, 7000);
            _t = 2000;
            move = false;
        }

        void MovementInform(uint32 /*type*/, uint32 /*id*/) override
        {
        }

        void JustReachedHome() override
        {
        }

        void MoveInLineOfSight(Unit* who) override
        {
            if (who->ToPlayer() || move)
                return ;
            if (!me->IsWithinDistInMap(who, 2.0f))
                return ;
            if (who->GetEntry() == 34840)
            {
                Talk(0);
                int x, y;
                x = 0;
                y = 0;
                switch (urand(0, 1))
                {
                case 0:
                    x = - irand(5, 10);
                    break;
                case 1:
                    x = irand(5, 10);
                    break;
                }
                switch (irand(0, 1))
                {
                case 0:
                    y = - irand(5, 10);
                    break;
                case 1:
                    y = irand(5, 10);
                    break;
                }
                _t = 6000;
                if (me->GetEntry() == 35234)
                    if (Unit *pl = who->GetVehicleKit()->GetPassenger(0))
                        if (pl->ToPlayer())
                        {
                            if (pl->ToPlayer()->GetQuestStatus(14121) == QUEST_STATUS_INCOMPLETE)
                                pl->ToPlayer()->AddItem(47530, 1);
                            _t = 2000;
                        }
                me->GetMotionMaster()->MoveJump(me->GetPositionX() + x, me->GetPositionY() + y, me->GetPositionZ() + 1, 0.0f, 10.0f, 10.0f);
                move = true;
            }
        }

        void UpdateAI(uint32 diff) override
        {
            if (_t <= diff)
            {
                if (move)
                {
                    me->GetMotionMaster()->MoveTargetedHome();
                    move = false;
                    if (me->GetEntry() == 35234)
                    {
                        me->Kill(me);
                        return ;
                    }
                }
                _t = 40000000;
            }
            else
                _t -= diff;
            if (_s <= diff)
            {
                _s = urand(5000, 11350);
                if (!UpdateVictim() || me->GetEntry() != 35234)
                    return;
                DoCastVictim(6257);
            }
            else
                _s -= diff;
        }

    private :
        uint32 _t, _s;
        bool move;
    };
};

// 207355
class gob_canon_gobelin : public GameObjectScript
{
public:
    gob_canon_gobelin() : GameObjectScript("gob_canon_gobelin") { }

    bool OnGossipHello(Player* player, GameObject* /*go*/) override
    {
        player->GetMotionMaster()->MoveJump( -7851.79f, 1838.72f, 8.0f, 0.0f, 20.0f, 20.0f);
        return true;
    }

};

// 352220
class npc_prince_marchand_gallywix_352220: public CreatureScript
{
public:
    npc_prince_marchand_gallywix_352220() : CreatureScript("npc_prince_marchand_gallywix_352220") {}

    bool OnQuestAccept(Player* player, Creature* /*creature*/, const Quest *_Quest) override
    {
        if (_Quest->GetQuestId() == 14120)
        {
            player->RemoveAurasDueToSpell(59074);
            player->SetAuraStack(67789, player, 1);
        }
        return true;
    }
};

// 2000001 
class npc_sumkaja_gob : public CreatureScript
{
public:
    npc_sumkaja_gob() : CreatureScript("npc_sumkaja_gob") { }

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_sumkaja_gobAI(creature);
    }

    struct npc_sumkaja_gobAI : public ScriptedAI
    {
        npc_sumkaja_gobAI(Creature* creature) : ScriptedAI(creature) {}

        void Reset() override
        {
            _t = 1000;
        }

        void JustReachedHome() override { }

        void UpdateAI(uint32 diff) override
        {
            if (_t <= diff)
            {
                if (me->FindNearestGameObject(GO_KAJAMITE_DEPOSIT_202593, 5) == NULL)
                    me->SummonGameObject(GO_KAJAMITE_DEPOSIT_202593, me->GetPositionX(), me->GetPositionY(), me->GetPositionZ(), 0, QuaternionData(), 120000);
                _t = 60000;
            }
            else _t -= diff;
        }

    private :
        uint32 _t;
    };
};

// 7000000
class npc_meteor_gob : public CreatureScript
{
public:
    npc_meteor_gob() : CreatureScript("npc_meteor_gob") { }

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_meteor_gobAI(creature);
    }

    struct npc_meteor_gobAI : public ScriptedAI
    {
        npc_meteor_gobAI(Creature* creature) : ScriptedAI(creature) {}


        void Reset() override
        {
            _a = urand(66000, 200200);
            _b = 600000;
            _c = 600000;
        }

        void JustReachedHome() override { }

        void UpdateAI(uint32 diff) override
        {
            if (_a <= diff)
            {
                me->CastSpell(me, 93668, true);
                _a = urand(66000, 200200);
                _b = 800;
            }
            else _a -= diff;
            if (_b <= diff)
            {
                me->CastSpell(me, 87701, true);
                _b = 600000;
                _c = 500;
            }
            else _b -= diff;
            if (_c <= diff)
            {
                me->CastSpell(me, 69235, true);
                _c = 600000;
            }
            else _c -= diff;
        }

    private :
        uint32 _a, _b, _c;
    };
};


// 8500000, 850000
class npc_gls_gob : public CreatureScript
{
public:
    npc_gls_gob() : CreatureScript("npc_gls_gob") { }

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_gls_gobAI(creature);
    }

    struct npc_gls_gobAI : public ScriptedAI
    {
        npc_gls_gobAI(Creature* creature) : ScriptedAI(creature) {}

        void Reset() override
        {
            _start_event = true;
            _diag = 0;
            mui_diag = 5000;
            _count = 0;
            _will_aura = ACTION_START;
            _scriptetre = true;
            if (me->ToTempSummon())
            {
                if (Unit *p = me->ToTempSummon()->GetSummoner())
                {
                    if (p->GetTypeId() != TYPEID_PLAYER)
                        _scriptetre = false;
                }
                else
                    _scriptetre = false;
            }
            else
                _scriptetre = false;
            if (!_scriptetre)
                return;
            m_ty = 600000;
            PhasingHandler::AddPhase(me, 180);
        }

        void DoAction(const int32 param) override
        {
            if (!_scriptetre || _start_event)
                return;
            m_ty = 2000;
            _prev_aura = param;
            Player* player = NULL;
            if (Unit* p = me->ToTempSummon()->GetSummoner())
            {
                player = p->ToPlayer();
                if (player)
                    if (param != ACTION_FAIL)
                        player->PlayDirectSound(11595, player);
            }
            if (_prev_aura != _will_aura)
            {
                if (player)
                    Talk(11, player);
                _count -= 1;
            }
            else
            {
                int res = (_count * 100) / 30 + 4;
                if (res < 0)
                {
                    _count = 0;
                    res = 0;
                }
                if (player)
                    if (player->GetVehicle())
                        if (player->GetVehicle()->GetBase())
                            player->GetVehicle()->GetBase()->SetPower(POWER_ENERGY, res); //DON'T WORK !
                std::ostringstream oss;
                oss << res;
                std::string result = oss.str();
                std::string _mess = "Bravo !\n Progress : ";
                _mess += result;
                _mess += "/100";
                if (player)
                {
                    Talk(10, player);
                    me->Whisper(_mess.c_str(), LANG_UNIVERSAL, player, true);
                }
                _count++;
            }
            if (param == ACTION_B)
                me->CastSpell(me, 90709, true);
            if (_count == 30)
            {
                if (player)
                {
                    Talk(12, player);
                    player->AddItem(46858, 1);
                }
                me->DespawnOrUnsummon();
                return ;
            }
            _will_aura = urand(1, 5);
            int sc_text = _will_aura;
            if (player)
                Talk(sc_text, player);
        }

        void SpellHit(Unit* /*caster*/, const SpellInfo* /*spell*/) override
        {
        }

        void JustReachedHome() override
        {
        }

        void UpdateAI(uint32 diff) override
        {
            if (!_scriptetre)
                return;
            if (_start_event)
            {
                if (mui_diag <= diff)
                {
                    Player *player = NULL;
                    if (Unit *p = me->ToTempSummon()->GetSummoner())
                        player = p->ToPlayer();
                    if (!player || player->GetVehicle() == NULL)
                        me->DespawnOrUnsummon();
                    switch (_diag)
                    {
                    case 0 :
                        if (player)
                            Talk(6, player);
                        ++_diag;
                        break;
                    case 1 :
                        if (player)
                            Talk(7, player);
                        ++_diag;
                        break;
                    case 2 :
                        if (player)
                            Talk(8, player);
                        ++_diag;
                        break;
                    case 3 :
                        if (player)
                            Talk(9, player);
                        ++_diag;
                        break;
                    default :
                        _start_event = false;
                        DoAction(ACTION_START);
                        break;
                    }
                    mui_diag = 10000;
                }
                else  mui_diag -= diff;
            }
            else
            {
                if (m_ty <= diff)
                {
                    m_ty = 600000;
                    DoAction(ACTION_FAIL);
                    if (_count < -4)
                        me->DespawnOrUnsummon();
                }
                else m_ty -= diff;
            }
        }

    private :
        uint32 m_ty;
        uint64 _prev_aura;
        uint64 _will_aura;
        int _count;
        bool _scriptetre;
        bool _start_event;
        uint32 mui_diag;
        int _diag;
    };
};

// 66298
class spell_klaxon : public SpellScriptLoader
{
public:
    spell_klaxon() : SpellScriptLoader("spell_klaxon") { }


    class spell_klaxonSpellScript : public SpellScript
    {
        PrepareSpellScript(spell_klaxonSpellScript);

        bool Validate(SpellInfo const* /*spellEntry*/) override
        {
            st = false;
            return true;
        }


        bool Load() override
        {
            return true;
        }

        void SummonPetAndValidQuest(uint32 spellEntry, Player *player, uint32 npcEntry, uint32 /*petEntry*/)
        {
            if (Creature *c = player->FindNearestCreature(npcEntry, 10))
            {
                c->DespawnOrUnsummon();
                player->CastSpell(player, spellEntry, true);
                player->KilledMonsterCredit(npcEntry);
            }
        }

        void HandleOnHit()
        {
            if (st)
                return;
            if (Unit* owner = GetCaster()->GetOwner())
                if (Player* player = owner->ToPlayer())
                {
                    GetCaster()->PlayDirectSound(22491, player);
                    st = true;
                    SummonPetAndValidQuest(66597, player, 934957, 34957);
                    SummonPetAndValidQuest(66599, player, 934958, 34958);
                    SummonPetAndValidQuest(66600, player, 934959, 34959);
                }
        }

    private :
        bool st;

        void Register() override
        {
            OnHit += SpellHitFn(spell_klaxonSpellScript::HandleOnHit);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_klaxonSpellScript();
    }
};

// 66299
class spell_radio : public SpellScriptLoader
{
public:
    spell_radio() : SpellScriptLoader("spell_radio") { }

    class  spell_radioSpellScript : public SpellScript
    {
        PrepareSpellScript(spell_radioSpellScript);

        bool Validate(SpellInfo const* /*spellEntry*/) override
        {
            st = false;
            return true;
        }

        bool Load() override
        {
            return true;
        }

        void SendPacketToPlayers(WorldPacket const* data, Player *player) const
        {
            player->GetSession()->SendPacket(data);
        }

        void HandleOnHit()
        {
            if (st)
                return;
            if (Unit* caster = GetCaster()->GetOwner())
            {
                if (caster->GetTypeId() == TYPEID_PLAYER)
                    GetCaster()->PlayDistanceSound(SOUND_RADIO, caster->ToPlayer());
                st = true;
            }
        }

    private :
        bool st;

        void Register() override
        {
            OnHit += SpellHitFn(spell_radioSpellScript::HandleOnHit);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_radioSpellScript();
    }
};

// 67495
class spell_bank_67495 : public SpellScriptLoader
{
public:
    spell_bank_67495() : SpellScriptLoader("spell_bank_67495") { }


    class  spell_bank_67495SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_bank_67495SpellScript);

        bool Validate(SpellInfo const* /*spellEntry*/) override
        {
            st = false;
            return true;
        }

        bool Load() override
        {
            return true;
        }

        void HandleOnHit()
        {
            if (st)
                return;
            if (Creature *t = GetCaster()->FindNearestCreature(850000, 5))
                t->GetAI()->DoAction(ACTION_A);
            st = true;
        }

    private :
        bool st;

        void Register() override
        {
            OnHit += SpellHitFn(spell_bank_67495SpellScript::HandleOnHit);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_bank_67495SpellScript();
    }
};

// 67496
class spell_bank_67496 : public SpellScriptLoader
{
public:
    spell_bank_67496() : SpellScriptLoader("spell_bank_67496") { }


    class  spell_bank_67496SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_bank_67496SpellScript);

        bool Validate(SpellInfo const* /*spellEntry*/) override
        {
            st = false;
            return true;
        }

        bool Load() override
        {
            return true;
        }

        void HandleOnHit()
        {
            if (st)
                return;
            if (Creature *t = GetCaster()->FindNearestCreature(850000, 5))
                t->GetAI()->DoAction(ACTION_B);

            st = true;
        }

    private :
        bool st;

        void Register() override
        {
            OnHit += SpellHitFn(spell_bank_67496SpellScript::HandleOnHit);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_bank_67496SpellScript();
    }
};

// 67497
class spell_bank_67497 : public SpellScriptLoader
{
public:
    spell_bank_67497() : SpellScriptLoader("spell_bank_67497") { }


    class  spell_bank_67497SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_bank_67497SpellScript);

        bool Validate(SpellInfo const* /*spellEntry*/) override
        {
            st = false;
            return true;
        }

        bool Load() override
        {
            return true;
        }

        void HandleOnHit()
        {
            if (st)
                return;
            if (Creature *t = GetCaster()->FindNearestCreature(850000, 5))
                t->GetAI()->DoAction(ACTION_C);
            st = true;
        }

    private :
        bool st;

        void Register() override
        {
            OnHit += SpellHitFn(spell_bank_67497SpellScript::HandleOnHit);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_bank_67497SpellScript();
    }
};

// 67498
class spell_bank_67498 : public SpellScriptLoader
{
public:
    spell_bank_67498() : SpellScriptLoader("spell_bank_67498") { }

    class  spell_bank_67498SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_bank_67498SpellScript);

        bool Validate(SpellInfo const* /*spellEntry*/) override
        {
            st = false;
            return true;
        }

        bool Load() override
        {
            return true;
        }

        void HandleOnHit()
        {
            if (st)
                return;
            if (Creature *t = GetCaster()->FindNearestCreature(850000, 5))
                t->GetAI()->DoAction(ACTION_D);
            st = true;
        }

    private :
        bool st;

        void Register() override
        {
            OnHit += SpellHitFn(spell_bank_67498SpellScript::HandleOnHit);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_bank_67498SpellScript();
    }
};

// 67499
class spell_bank_67499 : public SpellScriptLoader
{
public:
    spell_bank_67499() : SpellScriptLoader("spell_bank_67499") { }


    class  spell_bank_67499SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_bank_67499SpellScript);

        bool Validate(SpellInfo const* /*spellEntry*/) override
        {
            st = false;
            return true;
        }

        bool Load() override
        {
            return true;
        }

        void HandleOnHit()
        {
            if (st)
                return;
            if (Creature *t = GetCaster()->FindNearestCreature(850000, 5))
                t->GetAI()->DoAction(ACTION_E);
            st = true;
        }

    private :
        bool st;

        void Register() override
        {
            OnHit += SpellHitFn(spell_bank_67499SpellScript::HandleOnHit);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_bank_67499SpellScript();
    }
};

// player Script
class GobelinQuestEvent : public PlayerScript
{
public:
    GobelinQuestEvent() : PlayerScript("GobelinQuestEvent") { }

    void OnQuestComplete(Player* player, Quest const* quest)
    {
        switch (quest->GetQuestId())
        {
        case QUEST_NECESSARY_ROUGHNESS_24502:
        {
            if (Vehicle* veh = player->GetVehicle())
                if (Unit *unit = veh->GetBase())
                {
                    unit->ToCreature()->UpdateEntry(37213);
                    player->KilledMonsterCredit(37203, unit->GetGUID());
                    player->ExitVehicle();
                    player->CastCustomSpell(VEHICLE_SPELL_RIDE_HARDCODED, SPELLVALUE_BASE_POINT0, 1, unit, false);
                    unit->ToCreature()->SetSpeedRate(MOVE_RUN, 0.001f);
                }
            break;
        }
        }
    }
};

void AddSC_kezan()
{
    new npc_fbok_bank_teller_35120();
    new gob_bank_gobelin();
    new spell_kabummbomb();
    new npc_kezan_citizen_35075();
    new gob_canon_gobelin();
    new npc_prince_marchand_gallywix_352220();
    new npc_sumkaja_gob();
    new npc_meteor_gob();
    new spell_klaxon();
    new spell_radio();
    new spell_bank_67495();
    new spell_bank_67496();
    new spell_bank_67497();
    new spell_bank_67498();
    new spell_bank_67499();
    new npc_gls_gob();
    new GobelinQuestEvent();
}
