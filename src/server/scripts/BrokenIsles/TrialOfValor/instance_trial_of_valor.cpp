#include "ScriptMgr.h"
#include "InstanceScript.h"
#include "trial_of_valor.h"

const ObjectData creatureData[] = 
{
    { BOSS_ODYN, DATA_ODYN      },
    { BOSS_GUARM, DATA_GUARM    },
    { BOSS_HELYA, DATA_HELYA    },
    { 0, 0}
};

class instance_trial_of_valor : public InstanceMapScript
{
    public:
        instance_trial_of_valor() : InstanceMapScript(TOVScriptName, 1648)
        {}

        struct trial_of_valor_InstanceScript : public InstanceScript
        {
            explicit trial_of_valor_InstanceScript(InstanceMap* map) : InstanceScript(map)
            {
                SetHeaders(DataHeader);
                SetBossNumber(EncounterCount);
                LoadObjectData(creatureData, nullptr);
            }
        };

        InstanceScript* GetInstanceScript(InstanceMap* map) const override
        {
            return new trial_of_valor_InstanceScript(map);
        }
};

void AddSC_instance_trial_of_valor()
{
    new instance_trial_of_valor();
}
