#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "AreaTriggerTemplate.h"
#include "AreaTriggerAI.h"
#include "SpellMgr.h"
#include "Spell.h"
#include <iostream>
#include "AreaTrigger.h"
#include "GameObject.h"
#include "G3D/Vector3.h"
#include "trial_of_valor.h"

enum Spells
{
	// Hymdall
	SPELL_REVIVIFY = 228171,
	SPELL_DANCING_BLADE = 228003,
	SPELL_DANCING_BLADE_DMG = 228007,
	SPELL_HORN_OF_VALOR = 228012,
	SPELL_VALARJAR_BOND_AURA = 229529,

	// Hyrja
	SPELL_SHIELD_OF_LIGHT = 228162,
	SPELL_EXPEL_LIGHT = 228029,
	SPELL_EXPEL_LIGHT_DMG = 228030,

	// Odyn
	SPELL_ODYN_TELEPORT = 232580,
	SPELL_DRAW_POWER = 227503,
	SPELL_UNERRING_BLAST = 227629,
	SPELL_UNERRING_BLAST_TRIGGER = 230989,
	SPELL_SPEAR_OF_LIGHT = 227697,
	SPELL_SPEAR_OF_LIGHT_TRIGGER = 228870,
	SPELL_ODYN_TEST_AURA = 228911,
	SPELL_ODYN_TEST = 227626,
	SPELL_STORM_OF_JUSTICE_N = 227807,
	SPELL_STORM_OF_JUSTICE_H = 227959,
	SPELL_STORMFORGED_SPEAR = 228918,
	SPELL_STORMFORGED_SPEAR_DMG = 228914,
	SPELL_ARCING_STORM = 229254,
	SPELL_ARCING_STORM_DMG = 229255,
	SPELL_ARCING_STORM_DEBUFF = 229256,
	SPELL_CLEASING_FLAME_DMG = 227475,

	// Valarjar Runebearer
	SPELL_RUNIC_SHIELD_GREEN = 227594,
	SPELL_RUNIC_SHIELD_BLUE = 227595,
	SPELL_RUNIC_SHIELD_YELLOW = 227596,
	SPELL_RUNIC_SHIELD_ORANGE = 227597,
	SPELL_RUNIC_SHIELD_PURPLE = 227598,
	SPELL_BRANDED_GREEN = 227500,
	SPELL_BRANDED_BLUE = 227499,
	SPELL_BRANDED_YELLOW = 227498,
	SPELL_BRANDED_ORANGE = 227491,
	SPELL_BRANDED_PURPLE = 227490,

	// Spear of Light
	SPELL_SHATTER = 231016,
	SPELL_GLOWING_FRAGMENT = 227782,
	SPELL_GLOWING_FRAGMENT_DMG = 227781,

	// Runic Areatriggers
	SPELL_RUNIC_BRAND_PURPLE = 227483,
	SPELL_RUNIC_BRAND_ORANGE = 227484,
	SPELL_RUNIC_BRAND_YELLOW = 227485,
	SPELL_RUNIC_BRAND_BLUE = 227487,
	SPELL_RUNIC_BRAND_GREEN = 227488,
};

enum Events
{
	// Hymdall
	EVENT_HORN_OF_VALOR = 1,
	EVENT_DANCING_BLADE = 2,

	// Hyrja
	EVENT_EXPEL_LIGHT = 3,
	EVENT_SHIELD_OF_LIGHT = 4,
	EVENT_REVIVIFY = 5,
	EVENT_CHECK_TARGET = 6,

	// Odyn
	EVENT_DRAW_POWER = 7,
	EVENT_UNERRING_BLAST = 8,
	EVENT_INIT_PHASE_TWO = 9,
	EVENT_SPEAR_OF_LIGHT = 10,
	EVENT_INIT_PHASE_THREE = 11,
	EVENT_STORM_OF_JUSTICE = 12,
	EVENT_STORMFORGED_SPEAR = 13,

	// Spear of Light
	EVENT_SHATTER_SPEAR = 14,
};

enum Actions
{
	ACTION_COMBAT_FINISHED = 1,
	ACTION_COMBAT_STARTED = 2,
	ACTION_RESTORE_RUNE = 3,
};

enum Adds
{
	NPC_VALARJAR_RUNEBEARER = 114996,
	NPC_SPEAR_OF_LIGHT = 114467,
};

enum Data
{
	DATA_BRANDED_PLAYER = 1,
};

enum AreaTriggersIds
{
	AREA_RUNE_GREEN = 8788,
	AREA_RUNE_ORANGE = 8785,
	AREA_RUNE_PURPLE = 8784,
	AREA_RUNE_BLUE = 8787,
	AREA_RUNE_YELLOW = 8786
};

constexpr uint32 JUMP_CENTER_POINT = 1;

using RuneInfo = std::pair<uint32, uint32>;

const RuneInfo ValarjarRunes[] =
{
	RuneInfo(SPELL_BRANDED_GREEN, SPELL_RUNIC_SHIELD_GREEN),
	RuneInfo(SPELL_BRANDED_BLUE, SPELL_RUNIC_SHIELD_BLUE),
	RuneInfo(SPELL_BRANDED_YELLOW, SPELL_RUNIC_SHIELD_YELLOW),
	RuneInfo(SPELL_BRANDED_ORANGE, SPELL_RUNIC_SHIELD_ORANGE),
	RuneInfo(SPELL_BRANDED_PURPLE, SPELL_RUNIC_SHIELD_PURPLE),
};

const uint32 RunesAreaTriggers[] =
{
	SPELL_RUNIC_BRAND_BLUE,
	SPELL_RUNIC_BRAND_GREEN,
	SPELL_RUNIC_BRAND_ORANGE,
	SPELL_RUNIC_BRAND_YELLOW,
	SPELL_RUNIC_BRAND_PURPLE,
};

const Position CenterRoom = { 2428.874f, 528.676f, 748.996f };

struct RuneSelector : public std::unary_function<Unit*, bool>
{
	RuneSelector(Unit* src, bool isPlayer) : _src(src), _isPlayer(isPlayer)
	{}

	bool operator() (Unit const* target) const
	{
		if (!target)
			return false;

		if (_isPlayer && target->GetTypeId() != TYPEID_PLAYER)
			return false;

		if (!target->HasAura(SPELL_BRANDED_BLUE) && !target->HasAura(SPELL_BRANDED_GREEN)
			&& !target->HasAura(SPELL_BRANDED_ORANGE) && !target->HasAura(SPELL_BRANDED_YELLOW)
			&& !target->HasAura(SPELL_BRANDED_PURPLE))
			return true;

		return false;
	}

private:
	Unit * _src;
	bool _isPlayer;
};

// 114361
class boss_hymdall_tov : public CreatureScript
{
public:
	boss_hymdall_tov() : CreatureScript("boss_hymdall_tov")
	{}

	struct boss_hymdall_tov_AI : public BossAI
	{
		explicit boss_hymdall_tov_AI(Creature* me) : BossAI(me, DATA_ODYN)
		{}

		void DamageTaken(Unit* /**/, uint32 & /**/) override
		{
			if (me->HealthBelowPct(25) && !me->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_PC))
			{
				me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_PC);
				events.Reset();
				events.ScheduleEvent(EVENT_REVIVIFY, 100);
			}
		}

		void EnterCombat(Unit* /**/) override
		{
			instance->SendEncounterUnit(ENCOUNTER_FRAME_ENGAGE, me);
			_EnterCombat();

			if (Creature* odyn = me->FindNearestCreature(BOSS_ODYN, 250.f))
				DoZoneInCombat(odyn);
		}

		void EnterEvadeMode(EvadeReason why) override
		{
			me->setFaction(16);
			me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_PC);
			instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me);
			me->RemoveAllAreaTriggers();
			CreatureAI::EnterEvadeMode(why);
		}

		void ScheduleTasks() override
		{
			events.ScheduleEvent(EVENT_HORN_OF_VALOR, Seconds(30));
			events.ScheduleEvent(EVENT_DANCING_BLADE, Seconds(urand(10, 15)));
		}

		void DoAction(int32 action) override
		{
			if (action == ACTION_COMBAT_FINISHED)
			{
				me->SetReactState(REACT_PASSIVE);
				me->setFaction(35);
				me->SetFullHealth();
				me->CastStop();
				_EnterEvadeMode();
				me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_PC);
				events.Reset();
				me->GetMotionMaster()->MovePoint(0, me->GetHomePosition());
				instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me);

				if (Creature* odyn = me->FindNearestCreature(BOSS_ODYN, 250.f))
					odyn->GetAI()->DoAction(ACTION_COMBAT_STARTED);
			}
		}

		void SpellHit(Unit*, SpellInfo const* spell) override
		{
			if (!spell)
				return;

			if (spell->Id == SPELL_REVIVIFY)
			{
				me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_PC);
				ScheduleTasks();
			}
		}

		void ExecuteEvent(uint32 eventId) override
		{
			switch (eventId)
			{
			case EVENT_REVIVIFY:
			{
				if (Creature* hyrja = me->FindNearestCreature(BOSS_HYRJA, 125.f, true))
				{
					if (hyrja->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_PC))
						hyrja->GetAI()->DoAction(ACTION_COMBAT_FINISHED);
				}
				DoCastSelf(SPELL_REVIVIFY);
				break;
			}

			case EVENT_HORN_OF_VALOR:
			{
				DoCastSelf(SPELL_HORN_OF_VALOR);
				events.ScheduleEvent(EVENT_HORN_OF_VALOR, Seconds(35));
				break;
			}

			case EVENT_DANCING_BLADE:
			{
				if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 0, true))
					DoCast(target, SPELL_DANCING_BLADE);

				events.ScheduleEvent(EVENT_DANCING_BLADE, Seconds(urand(10, 20)));
				break;
			}
			}
		}
	};

	CreatureAI* GetAI(Creature* me) const override
	{
		return new boss_hymdall_tov_AI(me);
	}

};

// 114360
class boss_hyrja_tov : public CreatureScript
{
public:
	boss_hyrja_tov() : CreatureScript("boss_hyrja_tov")
	{}

	struct boss_hyrja_tov_AI : public BossAI
	{
		explicit boss_hyrja_tov_AI(Creature* me) : BossAI(me, DATA_ODYN)
		{}

		void JustDied(Unit*) override
		{
		}

		void EnterEvadeMode(EvadeReason why) override
		{
			me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_PC);
			me->setFaction(16);
			instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me);
			me->RemoveAllAreaTriggers();
			CreatureAI::EnterEvadeMode(why);
		}

		void EnterCombat(Unit*) override
		{
			instance->SendEncounterUnit(ENCOUNTER_FRAME_ENGAGE, me);
			_EnterCombat();
		}

		void DoAction(int32 action) override
		{
			if (action == ACTION_COMBAT_FINISHED)
			{
				me->setFaction(35);
				me->CastStop();
				me->SetReactState(REACT_PASSIVE);
				me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_PC);
				events.Reset();
				me->GetMotionMaster()->MovePoint(0, me->GetHomePosition());
				instance->SendEncounterUnit(ENCOUNTER_FRAME_DISENGAGE, me);
				me->SetFullHealth();
			}
		}

		void ScheduleTasks() override
		{
			events.ScheduleEvent(EVENT_EXPEL_LIGHT, Seconds(urand(10, 15)));
			events.ScheduleEvent(EVENT_SHIELD_OF_LIGHT, Seconds(35));
		}

		void DamageTaken(Unit*, uint32 &) override
		{
			if (me->HealthBelowPct(25) && !me->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_PC))
			{
				me->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_PC);
				events.Reset();
				events.ScheduleEvent(EVENT_REVIVIFY, 100);
			}
		}

		void SpellHit(Unit*, SpellInfo const* spell) override
		{
			if (!spell)
				return;

			if (spell->Id == SPELL_REVIVIFY)
			{
				me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_PC);
				ScheduleTasks();
			}
		}

		void ExecuteEvent(uint32 eventId) override
		{
			switch (eventId)
			{
			case EVENT_REVIVIFY:
			{
				if (Creature* hymdall = me->FindNearestCreature(BOSS_HYMDALL, 125.f, true))
				{
					if (hymdall->HasFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_PC))
					{
						hymdall->GetAI()->DoAction(ACTION_COMBAT_FINISHED);
						DoAction(ACTION_COMBAT_FINISHED);
					}
					else
						DoCastSelf(SPELL_REVIVIFY);
				}
				break;
			}

			case EVENT_SHIELD_OF_LIGHT:
			{
				if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 0, true))
					DoCast(target, SPELL_SHIELD_OF_LIGHT);

				events.ScheduleEvent(EVENT_SHIELD_OF_LIGHT, Seconds(35));
				break;
			}

			case EVENT_EXPEL_LIGHT:
			{
				if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0, 0, true))
					DoCast(target, SPELL_EXPEL_LIGHT);

				events.ScheduleEvent(EVENT_EXPEL_LIGHT, Seconds(urand(10, 15)));
				break;
			}
			}
		}
	};

	CreatureAI* GetAI(Creature* me) const override
	{
		return new boss_hyrja_tov_AI(me);
	}
};

// 114263
class boss_odyn_tov : public CreatureScript
{
public:
	boss_odyn_tov() : CreatureScript("boss_odyn_tov")
	{}

	struct boss_odyn_tov_AI : public BossAI
	{
		explicit boss_odyn_tov_AI(Creature* me) : BossAI(me, DATA_ODYN)
		{
			me->SetReactState(REACT_PASSIVE);
			me->AddUnitState(UNIT_STATE_ROOT);
		}

		void JustReachedHome() override
		{
			me->SetReactState(REACT_PASSIVE);
			me->AddUnitState(UNIT_STATE_ROOT);
			_JustReachedHome();
		}

		void EnterCombat(Unit* /**/) override
		{
			_phaseThree = false;
			_EnterCombat();
			events.ScheduleEvent(EVENT_DRAW_POWER, Seconds(30));
		}

		void EnterEvadeMode(EvadeReason why) override
		{
			_EnterEvadeMode();
			me->RemoveAllAreaTriggers();
			me->NearTeleportTo(me->GetHomePosition());
			Reset();
		}

		void DoAction(int32 action) override
		{
			if (action == ACTION_COMBAT_STARTED)
			{
				events.Reset();
				me->CastStop();
				me->ClearUnitState(UNIT_STATE_ROOT);
				DoCastSelf(SPELL_ODYN_TEST_AURA, true);
				me->GetMotionMaster()->MoveJump(CenterRoom, 15.f, 15.f, JUMP_CENTER_POINT);
				events.ScheduleEvent(EVENT_INIT_PHASE_TWO, Seconds(10));
			}
		}

		void DamageTaken(Unit* /**/, uint32 & /**/) override
		{
			if (me->HealthBelowPct(55) && !_phaseThree)
			{
				_phaseThree = true;
				events.Reset();
				DoCastSelf(SPELL_ARCING_STORM, true);
				events.ScheduleEvent(EVENT_STORMFORGED_SPEAR, Seconds(10));
				events.ScheduleEvent(EVENT_STORM_OF_JUSTICE, Seconds(urand(10, 13)));
			}
			else if (me->HealthBelowPct(10))
				FinishEncounter();
		}

		void FinishEncounter()
		{
			_JustDied();
			me->RemoveAllAreaTriggers();
			me->setFaction(35);
		}

		void ActivateRunes(bool state)
		{
			std::list<GameObject*> runes;
			me->GetGameObjectListWithEntryInGrid(runes, GO_RUNE_BLUE);
			me->GetGameObjectListWithEntryInGrid(runes, GO_RUNE_GREEN);
			me->GetGameObjectListWithEntryInGrid(runes, GO_RUNE_ORANGE);
			me->GetGameObjectListWithEntryInGrid(runes, GO_RUNE_YELLOW);
			me->GetGameObjectListWithEntryInGrid(runes, GO_RUNE_PURPLE);

			SpellCastTargets targets;
			uint32 spellId = 0;

			for (auto & it : runes)
			{
				if (state)
				{
					targets.SetDst(it->GetPosition());

					SpellInfo const* spell = sSpellMgr->GetSpellInfo(RunesAreaTriggers[spellId++]);

					if (spell)
						me->CastSpell(targets, spell, nullptr, TRIGGERED_FULL_MASK);
				}

				it->SetGoState(state ? GO_STATE_ACTIVE : GO_STATE_READY);
			}
		}

		void SummonValarjars()
		{
			for (uint8 i = 0; i < 5; ++i)
			{
				auto* ptr = DoSummon(NPC_VALARJAR_RUNEBEARER, me->GetRandomPoint(CenterRoom, 40.f), 5000, TEMPSUMMON_CORPSE_DESPAWN);

				if (!ptr)
					continue;

				ptr->GetAI()->SetData(DATA_BRANDED_PLAYER, i);
			}
		}

		void ExecuteEvent(uint32 eventId) override
		{
			switch (eventId)
			{
			case EVENT_DRAW_POWER:
			{
				DoCastSelf(SPELL_DRAW_POWER);

				uint8 i = 0;

				ActivateRunes(true);
				SummonValarjars();
				events.ScheduleEvent(EVENT_UNERRING_BLAST, Seconds(30));
				break;
			}

			case EVENT_INIT_PHASE_TWO:
			{
				me->SetReactState(REACT_AGGRESSIVE);
				me->setFaction(16);
				me->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_PC);
				events.ScheduleEvent(EVENT_SPEAR_OF_LIGHT, Seconds(10));
				events.ScheduleEvent(EVENT_DRAW_POWER, Seconds(30));
				break;
			}

			case EVENT_SPEAR_OF_LIGHT:
			{
				DoCast(me, SPELL_SPEAR_OF_LIGHT);
				events.ScheduleEvent(EVENT_SPEAR_OF_LIGHT, Seconds(urand(15, 20)));
				break;
			}

			case EVENT_STORMFORGED_SPEAR:
			{
				DoCastVictim(SPELL_STORMFORGED_SPEAR);
				events.ScheduleEvent(EVENT_STORMFORGED_SPEAR, Seconds(10));
				break;
			}

			case EVENT_UNERRING_BLAST:
			{
				ActivateRunes(false);
				DoCastSelf(SPELL_UNERRING_BLAST);
				events.ScheduleEvent(EVENT_DRAW_POWER, Seconds(30));
				break;
			}

			case EVENT_STORM_OF_JUSTICE:
			{
				DoCastSelf(IsHeroic() ? SPELL_STORM_OF_JUSTICE_H : SPELL_STORM_OF_JUSTICE_N);
				events.ScheduleEvent(EVENT_STORM_OF_JUSTICE, Seconds(urand(12, 14)));
				break;
			}
			}
		}

	private:
		bool _phaseThree;
	};

	CreatureAI* GetAI(Creature* me) const override
	{
		return new boss_odyn_tov_AI(me);
	}
};

// 114996
class npc_tov_valarjar_runebearer : public CreatureScript
{
public:
	npc_tov_valarjar_runebearer() : CreatureScript("npc_tov_valarjar_runebearer")
	{}

	struct npc_tov_valarjar_runebearer_AI : public ScriptedAI
	{
		explicit npc_tov_valarjar_runebearer_AI(Creature* me) : ScriptedAI(me)
		{}

		void SetData(uint32 id, uint32 value) override
		{
			if (id == DATA_BRANDED_PLAYER)
			{
				Unit*&& target = SelectTarget(SELECT_TARGET_RANDOM, 0, RuneSelector(me, true));

				if (!target)
					return;

				_runeId = value;
				DoCast(target, ValarjarRunes[value].first, true);
				DoCastSelf(ValarjarRunes[value].second, true);
				_brandedGUID = target->GetGUID();
			}
		}

		void DoAction(int32 action) override
		{
			if (action == ACTION_RESTORE_RUNE)
				DoCastSelf(ValarjarRunes[_runeId].second, true);
		}

		void DoMeleeAttackIfReady()
		{
			if (!me->GetVictim())
				return;

			if (me->GetVictim()->GetGUID() != _brandedGUID)
			{
				Unit* target = ObjectAccessor::GetPlayer(*me, _brandedGUID);

				if (target)
				{
					me->AddThreat(target, 100000.f);
					me->Attack(target, true);
				}
			}

			UnitAI::DoMeleeAttackIfReady();
		}

	private:
		uint32 _runeId;
		ObjectGuid _brandedGUID;
	};

	CreatureAI* GetAI(Creature* me) const override
	{
		return new npc_tov_valarjar_runebearer_AI(me);
	}
};

// 114467
class npc_tov_spear_of_light : public CreatureScript
{
public:
	npc_tov_spear_of_light() : CreatureScript("npc_tov_spear_of_light")
	{}

	struct npc_spear_of_light_AI : public ScriptedAI
	{
		explicit npc_spear_of_light_AI(Creature* me) : ScriptedAI(me)
		{
			_timerDespawn = 0;
		}

		void IsSummonedBy(Unit* /**/) override
		{
			_borned = false;
			_timerDespawn = 0;
		}

		void UpdateAI(uint32 diff) override
		{
			if (_borned)
				return;

			_timerDespawn += diff;

			if (_timerDespawn >= 2 * IN_MILLISECONDS)
			{
				_borned = true;
				Creature* odyn = me->FindNearestCreature(BOSS_ODYN, 500.f, true);

				for (uint8 i = 0; i < 5; ++i)
				{
					if (odyn)
						odyn->CastSpell(me, SPELL_GLOWING_FRAGMENT, true);
				}

				DoCastSelf(SPELL_SHATTER);
				me->DespawnOrUnsummon(500);
			}
		}

	private:
		bool _borned;
		uint32 _timerDespawn;
	};

	CreatureAI* GetAI(Creature* me) const override
	{
		return new npc_spear_of_light_AI(me);
	}
};

// 229530
class spell_odyn_valarjar_bond : public SpellScriptLoader
{
public:
	spell_odyn_valarjar_bond() : SpellScriptLoader("spell_odyn_valarjar_bond")
	{}

	class spell_valarjar_bond_SpellScript : public SpellScript
	{
	public:
		PrepareSpellScript(spell_valarjar_bond_SpellScript);

		void HandleDummy(SpellEffIndex)
		{
			if (!GetHitUnit())
				return;

			GetHitUnit()->CastSpell(GetHitUnit(), SPELL_VALARJAR_BOND_AURA, true);
			GetCaster()->CastSpell(GetCaster(), SPELL_VALARJAR_BOND_AURA, true);
		}

		void HandleTargets(SpellTargets & targets)
		{
			if (targets.empty())
				return;

			targets.remove_if([](WorldObject*& target)
			{
				return target->GetEntry() != BOSS_HYRJA && target->GetEntry() != BOSS_ODYN;
			});
		}

		void Register() override
		{
			OnObjectAreaTargetSelect += SpellObjectAreaTargetSelectFn(spell_valarjar_bond_SpellScript::HandleTargets, EFFECT_0, TARGET_UNIT_SRC_AREA_ENTRY);
			OnEffectHitTarget += SpellEffectFn(spell_valarjar_bond_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
		}
	};

	SpellScript* GetSpellScript() const override
	{
		return new spell_valarjar_bond_SpellScript();
	}
};

// 228029
class spell_odyn_expel_light : public SpellScriptLoader
{
public:
	spell_odyn_expel_light() : SpellScriptLoader("spell_odyn_expel_light")
	{}

	class spell_expel_light_AuraScript : public AuraScript
	{
	public:
		PrepareAuraScript(spell_expel_light_AuraScript);

		void HandlePeriodic(AuraEffect const* /**/)
		{
			if (!GetUnitOwner())
				return;

			GetUnitOwner()->CastSpell(GetUnitOwner(), SPELL_EXPEL_LIGHT_DMG, true);
		}

		void Register() override
		{
			OnEffectPeriodic += AuraEffectPeriodicFn(spell_expel_light_AuraScript::HandlePeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
		}
	};

	AuraScript* GetAuraScript() const override
	{
		return new spell_expel_light_AuraScript();
	}
};

// 227697
class spell_odyn_spear_of_light_tov : public SpellScriptLoader
{
public:
	spell_odyn_spear_of_light_tov() : SpellScriptLoader("spell_odyn_spear_of_light_tov")
	{}

	class spell_odyn_spear_of_light_tov_SpellScript : public SpellScript
	{
	public:
		PrepareSpellScript(spell_odyn_spear_of_light_tov_SpellScript);

		void HandleDummy(SpellEffIndex /**/)
		{
			if (!GetHitUnit())
				return;

			GetCaster()->CastSpell(GetHitUnit(), SPELL_SPEAR_OF_LIGHT_TRIGGER, true);
		}

		void Register() override
		{
			OnEffectHitTarget += SpellEffectFn(spell_odyn_spear_of_light_tov_SpellScript::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
		}
	};

	SpellScript* GetSpellScript() const override
	{
		return new spell_odyn_spear_of_light_tov_SpellScript();
	}
};

// 228911
class spell_odyn_odyn_test : public SpellScriptLoader
{
public:
	spell_odyn_odyn_test() : SpellScriptLoader("spell_odyn_odyn_test")
	{}

	class spell_odyn_test_AuraScript : public AuraScript
	{
	public:
		PrepareAuraScript(spell_odyn_test_AuraScript);

		bool Load() override
		{
			_victimGUID = ObjectGuid::Empty;
			return true;
		}

		void HandleOnProc(ProcEventInfo & proc)
		{
			if (proc.GetProcTarget())
				std::cout << "Proc Target: " << proc.GetProcTarget()->GetEntry() << std::endl;

			if (proc.GetActor())
				std::cout << "Actor Target: " << proc.GetActor()->GetEntry() << std::endl;

			if (proc.GetActionTarget())
				std::cout << "Action Target: " << proc.GetActionTarget()->GetEntry() << std::endl;

			if (proc.GetProcTarget())
			{
				if (_victimGUID == ObjectGuid::Empty)
					_victimGUID = proc.GetProcTarget()->GetGUID();
				else
				{
					if (_victimGUID != proc.GetProcTarget()->GetGUID())
					{
						GetUnitOwner()->RemoveAura(SPELL_ODYN_TEST);
						_victimGUID = proc.GetProcTarget()->GetGUID();
					}
					else
						GetUnitOwner()->CastSpell(GetUnitOwner(), SPELL_ODYN_TEST, true);
				}
			}
		}

		void Register() override
		{
			OnProc += AuraProcFn(spell_odyn_test_AuraScript::HandleOnProc);
		}

	private:
		ObjectGuid _victimGUID;
	};

	AuraScript* GetAuraScript() const override
	{
		return new spell_odyn_test_AuraScript();
	}
};

// 228918
class spell_odyn_stormforged_spear : public SpellScriptLoader
{
public:
	spell_odyn_stormforged_spear() : SpellScriptLoader("spell_odyn_stormforged_spear")
	{}

	class spell_stormforged_spear_AuraScript : public AuraScript
	{
	public:
		PrepareAuraScript(spell_stormforged_spear_AuraScript);

		void HandleOnRemove(AuraEffect const* /**/, AuraEffectHandleModes /**/)
		{
			GetCaster()->CastSpell(GetUnitOwner(), SPELL_STORMFORGED_SPEAR_DMG, true);
		}

		void Register() override
		{
			OnEffectRemove += AuraEffectRemoveFn(spell_stormforged_spear_AuraScript::HandleOnRemove, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY, AURA_EFFECT_HANDLE_REAL);
		}
	};

	AuraScript* GetAuraScript() const override
	{
		return new spell_stormforged_spear_AuraScript();
	}
};

// 228915  (not 228914)
class spell_odyn_stormforged_spear_dmg : public SpellScriptLoader
{
public:
	spell_odyn_stormforged_spear_dmg() : SpellScriptLoader("spell_odyn_stormforged_spear_dmg")
	{}

	class spell_stormforged_spear_dmg_SpellScript : public SpellScript
	{
	public:
		PrepareSpellScript(spell_stormforged_spear_dmg_SpellScript);

		void HandleDmg(SpellEffIndex /**/)
		{
			if (!GetHitUnit())
				return;

			int32 damage = GetHitDamage();
			float dist = GetCaster()->GetDistance2d(GetHitUnit());

			if (dist >= 5.f)
			{
				dist /= 100.f;
				damage -= (damage * dist);
				SetHitDamage(damage);
			}
		}

		void Register() override
		{
			OnEffectLaunchTarget += SpellEffectFn(spell_stormforged_spear_dmg_SpellScript::HandleDmg, EFFECT_0, SPELL_EFFECT_SCHOOL_DAMAGE);
		}
	};

	SpellScript* GetSpellScript() const override
	{
		return new spell_stormforged_spear_dmg_SpellScript();
	}
};

// 229254
class spell_odyn_arcing_storm : public SpellScriptLoader
{
public:
	spell_odyn_arcing_storm() : SpellScriptLoader("spell_odyn_arcing_storm")
	{}

	struct spell_arcing_storm_AuraScript : public AuraScript
	{
		PrepareAuraScript(spell_arcing_storm_AuraScript);

		void OnPeriodic(AuraEffect const* /**/)
		{
			std::list<Player*> targets;

			GetCaster()->GetPlayerListInGrid(targets, 250.f);

			if (targets.empty())
				return;

			for (auto & it : targets)
			{
				if (it)
				{
					GetCaster()->CastSpell(it, SPELL_ARCING_STORM_DMG, true);
					GetCaster()->CastSpell(it, SPELL_ARCING_STORM_DEBUFF, true);
				}
			}
		}

		void Register() override
		{
			OnEffectPeriodic += AuraEffectPeriodicFn(spell_arcing_storm_AuraScript::OnPeriodic, EFFECT_0, SPELL_AURA_PERIODIC_DUMMY);
		}
	};

	AuraScript* GetAuraScript() const override
	{
		return new spell_arcing_storm_AuraScript();
	}
};

// 227629
class spell_odyn_unerring_blast : public SpellScriptLoader
{
public:
	spell_odyn_unerring_blast() : SpellScriptLoader("spell_odyn_unerring_blast")
	{}

	class spell_unerring_blast_SpellScript : public SpellScript
	{
		PrepareSpellScript(spell_unerring_blast_SpellScript);

		void OnHit(SpellEffIndex /**/)
		{
			if (!GetHitUnit())
				return;

			GetCaster()->CastSpell(GetHitUnit(), SPELL_UNERRING_BLAST_TRIGGER, true);
		}

		void Register() override
		{
			OnEffectHitTarget += SpellEffectFn(spell_unerring_blast_SpellScript::OnHit, EFFECT_0, SPELL_EFFECT_DUMMY);
		}
	};

	SpellScript* GetSpellScript() const override
	{
		return new spell_unerring_blast_SpellScript();
	}
};

// 8825
class at_tov_dancing_blade : public AreaTriggerEntityScript
{
public:
	at_tov_dancing_blade() : AreaTriggerEntityScript("at_tov_dancing_blade")
	{}

	struct at_tov_dancing_blade_AI : public AreaTriggerAI
	{
		explicit at_tov_dancing_blade_AI(AreaTrigger* at) : AreaTriggerAI(at)
		{}

		void OnUnitEnter(Unit* target) override
		{
			if (target && target->GetTypeId() == TYPEID_PLAYER)
				target->CastSpell(target, SPELL_DANCING_BLADE_DMG, true);
		}

		void OnUnitExit(Unit* target) override
		{
			if (target && target->GetTypeId() == TYPEID_PLAYER)
				target->RemoveAurasDueToSpell(SPELL_DANCING_BLADE_DMG);
		}
	};

	AreaTriggerAI* GetAI(AreaTrigger* at) const override
	{
		return new at_tov_dancing_blade_AI(at);
	}
};

// 8810
class at_tov_glowing_fragment : public AreaTriggerEntityScript
{
public:
	at_tov_glowing_fragment() : AreaTriggerEntityScript("at_tov_glowing_fragment")
	{}

	struct at_glowing_fragment_AI : public AreaTriggerAI
	{
		explicit at_glowing_fragment_AI(AreaTrigger* at) : AreaTriggerAI(at)
		{}

		void SetupSpline()
		{
			if (!at->GetCaster())
				return;

			std::vector<G3D::Vector3> points;
			float dist = 100.f;

			G3D::Vector3 src = { at->GetPositionX(), at->GetPositionY(), at->GetPositionZ() };
			G3D::Vector3 tgt;

			float ori = frand(0, 2 * float(M_PI));
			at->SetOrientation(ori);

			tgt.x = src.x + (dist * cosf(at->GetOrientation()));
			tgt.y = src.y + (dist * sinf(at->GetOrientation()));
			tgt.z = src.z;

			float dx = (tgt.x - src.x);
			float dy = (tgt.y - src.y);
			float dz = (tgt.z - src.z);

			for (uint32 i = 0; i < 100; ++i)
			{
				src.x += (dx / dist);
				src.y += (dy / dist);

				points.push_back(src);
			}

			at->InitSplines(points, at->GetDuration());
		}

		void OnInitialize() override
		{
			SetupSpline();
		}

		void OnUnitEnter(Unit* target) override
		{
			if (target && target->GetTypeId() == TYPEID_PLAYER)
			{
				target->CastSpell(target, SPELL_GLOWING_FRAGMENT_DMG, true);
				at->Remove();
			}
		}
	};

	AreaTriggerAI* GetAI(AreaTrigger* at) const override
	{
		return new at_glowing_fragment_AI(at);
	}
};

// 8787, 8788, 8784, 8786, 8785
class at_runic_brand : public AreaTriggerEntityScript
{
public:
	at_runic_brand() : AreaTriggerEntityScript("at_runic_brand")
	{}

	struct at_runic_brand_AI : public AreaTriggerAI
	{
		at_runic_brand_AI(AreaTrigger* at) : AreaTriggerAI(at)
		{}

		void OnUnitEnter(Unit* target) override
		{
			if (!target)
				return;

			if (target->GetEntry() == NPC_VALARJAR_RUNEBEARER)
			{
				switch (at->GetEntry())
				{
				case AREA_RUNE_GREEN:
					target->RemoveAurasDueToSpell(SPELL_RUNIC_SHIELD_GREEN);
					break;

				case AREA_RUNE_ORANGE:
					target->RemoveAurasDueToSpell(SPELL_RUNIC_SHIELD_ORANGE);
					break;

				case AREA_RUNE_PURPLE:
					target->RemoveAurasDueToSpell(SPELL_RUNIC_SHIELD_PURPLE);
					break;

				case AREA_RUNE_BLUE:
					target->RemoveAurasDueToSpell(SPELL_RUNIC_SHIELD_BLUE);
					break;

				case AREA_RUNE_YELLOW:
					target->RemoveAurasDueToSpell(SPELL_RUNIC_SHIELD_YELLOW);
					break;
				}
			}
		}

		void OnUnitExit(Unit* target) override
		{
			if (!target)
				return;

			if (target->GetEntry() == NPC_VALARJAR_RUNEBEARER)
				target->GetAI()->DoAction(ACTION_RESTORE_RUNE);
		}
	};

	AreaTriggerAI* GetAI(AreaTrigger* at) const override
	{
		return new at_runic_brand_AI(at);
	}
};

// ???
class at_tov_cleasing_flames : public AreaTriggerEntityScript
{
public:
	at_tov_cleasing_flames() : AreaTriggerEntityScript("at_tov_cleasing_flames")
	{}

	struct at_tov_cleasing_flames_AI : public AreaTriggerAI
	{
		explicit at_tov_cleasing_flames_AI(AreaTrigger* at) : AreaTriggerAI(at)
		{}

		void OnUnitEnter(Unit* target) override
		{
			if (!target)
				return;

			if (target->GetTypeId() == TYPEID_PLAYER)
				target->CastSpell(target, SPELL_CLEASING_FLAME_DMG, true);
		}

		void OnUnitExit(Unit* target) override
		{
			if (!target)
				return;

			if (target->GetTypeId() == TYPEID_PLAYER)
				target->RemoveAurasDueToSpell(SPELL_CLEASING_FLAME_DMG);
		}
	};

	AreaTriggerAI* GetAI(AreaTrigger* at) const override
	{
		return new at_tov_cleasing_flames_AI(at);
	}
};

void AddSC_new_boss_odyn()
{
	new boss_hymdall_tov();
	new boss_hyrja_tov();
	new boss_odyn_tov();
	new npc_tov_spear_of_light();
	new npc_tov_valarjar_runebearer();
	new spell_odyn_valarjar_bond();
	new spell_odyn_odyn_test();
	new spell_odyn_expel_light();
	new spell_odyn_spear_of_light_tov();
	new spell_odyn_stormforged_spear();
	new spell_odyn_stormforged_spear_dmg();
	new spell_odyn_arcing_storm();
	new spell_odyn_unerring_blast();
	new at_tov_dancing_blade();
	new at_runic_brand();
	new at_tov_glowing_fragment();
}
