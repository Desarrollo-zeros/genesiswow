#ifndef DEF_TRIAL_OF_VALOR_H
#define DEF_TRIAL_OF_VALOR_H

#include "Map.h"
#include "Creature.h"
#include "ObjectMgr.h"

#define DataHeader "TOV"
#define TOVScriptName "instance_trial_of_valor"

constexpr uint32 EncounterCount = 3;

enum TOVDataIds
{
    DATA_ODYN       = 1,
    DATA_GUARM      = 2,
    DATA_HELYA      = 3,
    DATA_HYRJA      = 4,
    DATA_HYMDALL    = 5,
};

enum TOVCreaturesIds
{
    BOSS_ODYN   = 114263,
    BOSS_HYRJA  = 114360,
    BOSS_HYMDALL= 114361,
    BOSS_GUARM  = 114344,
    BOSS_HELYA  = 114537,
};

enum TOVObjectsIds
{
    GO_RUNE_ORANGE  = 245697,
    GO_RUNE_YELLOW  = 245695,
    GO_RUNE_BLUE    = 245696,
    GO_RUNE_GREEN   = 245699,
    GO_RUNE_PURPLE  = 245698,
};

using SpellTargets = std::list<WorldObject*>;

#endif
