/*
* Copyright (C) 2019 PPA-Core.
* Copyright (C) 2011-2016 ArkCORE <http://www.arkania.net/>
*
* This program is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation; either version 2 of the License, or (at your
* option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "CreatureAIImpl.h"
#include "GameObjectAI.h"
#include "GameObject.h"
#include "Item.h"
#include "MotionMaster.h"
#include "ObjectAccessor.h"
#include "ObjectMgr.h"
#include "Player.h"
#include "ScriptMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "ScriptedEscortAI.h"
#include "SpellScript.h"
#include "TemporarySummon.h"
#include "Transport.h"
#include "TransportMgr.h"
#include "Vehicle.h"
#include "VehicleDefines.h"

enum NorthernBarrens
{
    AT_GROLDOMS_FARM = 5465,

    NPC_HALGA_BLOODEYE                  = 34258,
    NPC_TRAPPED_WOLF                    = 34285,
    NPC_CHAIN_ORIGIN                    = 34287,
    NPC_LEAD_CARAVAN_KODO               = 34430,
    NPC_BALGOR_WHIPSHANK                = 34431,
    NPC_FAR_WATCH_CARAVAN_KODO          = 34432,
    NPC_RIDING_SHOTGUN                  = 34438,
    NPC_RAZORMANE_RAIDER                = 34487,
    NPC_HEAD_CARAVAN_KODO               = 34577,
    NPC_TOGRIK_34513                    = 34513,
    NPC_THE_HOTSEAT                     = 34582,
    NPC_CROSSROADS_CARAVAN_KODO         = 34576,
    NPC_CROSSROADS_CARAVAN_DELIVERY_CREDIT1     = 53572,
    NPC_CROSSROADS_CARAVAN_DELIVERY_KILL_CREDIT = 65892,
    NPC_QUESTGIVER                      = 3464,
    NPC_ROCCO_WHIPSHANK                 = 34578,
    NPC_BURNING_BLADE_MOUNT             = 34593,
    NPC_BURNING_BLADE_RAIDER            = 34594,
    NPC_CAPTURED_RAZORMANE              = 34523,
    NPC_TOGRIK                          = 34513,
    NPC_DAVES_INDUSTRIAL_LIGHT_AND_MAGIC_BUNNY  = 34527,

    QUEST_THROUGH_FIRE_AND_FLAMES       = 13878,
    QUEST_CROSSROADS_CARAVAN_PICKUP     = 13949,
    QUEST_DRAG_IT_OUT_OF_THEM           = 13961,
    QUEST_INVESTIGATE_THE_WRECKAGE      = 14066,
    QUEST_TO_TRACK_A_THIEF              = 869,
    QUEST_CROSSROADS_CARAVAN_DELIVERY   = 13975,
    QUEST_BY_HOOK_OR_BY_CROOK           = 13963,
    QUEST_ANIMAL_SERVICES               = 13970,
    QUEST_THE_KODOS_RETURN              = 13971,

    PATH_FREE_TRAPPED_WOLF              = 3428500,
    PATH_LEAD_CARAVAN_KODO              = 3443001,
    PATH_RAZORMANE                      = 3452301,

    SPELL_SUMMON_KODO_PART_1            = 65486,
    SPELL_MOUNT_UP_65487                = 65487,
    SPELL_GIDDYAP                       = 65493,
    SPELL_CALL_A_PACK_KODO              = 65494,
    SPELL_SUMMON_ANGRY_RAZORMANE_RAIDER = 65496,
    SPELL_DISMOUNT_CARAVAN_KODO_CUE1    = 65557,
    SPELL_QUEST_COMPLETE_13949          = 65561,
    SPELL_RIDE_VEHICLE3                 = 63314,
    SPELL_RIDE_VEHICLE4                 = 63315,
    SPELL_RIDE_VEHICLE5                 = 63316,
    SPELL_RIDE_VEHICLE2                 = 56687,
    SPELL_RIDE_VEHICLE1                 = 52391,
    SPELL_WORLD_GENERIC_DISMOUNT        = 61286,
    SPELL_CHAINED_TO_STABLES            = 65072,
    SPELL_RIDE_CARAVAN_KODO             = 65466,
    SPELL_GROLDOM_NET                   = 65580,
    SPELL_SNARED_IN_NET                 = 65581,
    SPELL_GET_A_HOGTIED_RAZORMANE       = 65599,
    SPELL_PERMANENT_FEIGN_DEATH         = 29266,
    SPELL_SUMMON_HOGTIED_RAZORMANE      = 65595,
    SPELL_DEAD_SOLDIER                  = 45801,
    SPELL_HOGTIED_RAZORMANE_ROPE        = 65596,
    SPELL_DRAGGING_A_RAZORMANE          = 65601,
    SPELL_RAZORMANE_DRAGGING_AURA       = 65608,
    SPELL_RAZORMANE_DRAGGING_TRIGGER    = 65610,
    SPELL_CALL_A_PACK_KODO2             = 65663,
    SPELL_DISMOUNT_CARAVAN_KODO_CUE2    = 65695,
    SPELL_EJECT_ALL_PASSENGERS          = 50630,
    SPELL_MOUNT_UP_65671                = 65671,
    SPELL_SUMMON_BURNING_BLADE          = 65692,
    SPELL_DELIVER_HOGTIED_RAZORMANE     = 65603,
    SPELL_COSMETIC_CHAINS               = 65612,
    SPELL_SUMMON_KODO_PART2             = 65662,
    SPELL_WORLD_GENERIC_DISMOUNT_CANCEL_SHAPESHIFT = 61286,

    AAAA = 1000,

    ACTION_CREATE_CHAIN,
    ACTION_GOSSIP_HELLO,
    ACTION_GOSSIP_SELECT,
    ACTION_MOUNT_PLAYER,
    ACTION_MOVE_FOLLOW,
    ACTION_RESET,
    ACTION_START_ANIM,
    ACTION_START_CARAVAN,

    DATA_ACTION_ID,
    DATA_IS_ANIM_STARTED,
    DATA_MENU_ID,

    EVENT_ARRIVED_HOME,
    EVENT_CHECK_PLAYER,
    EVENT_CHECK_CREATURE,
    EVENT_CREATURE_TALK,
    EVENT_CREATURE_TALK_MORE,
    EVENT_FOLLOW_PLAYER,
    EVENT_INIT_CHAIN,
    EVENT_INIT_SUMMON,
    EVENT_MASTER_RESET,
    EVENT_MAX_TIME,
    EVENT_MOUNT_GUNNER,
    EVENT_MOUNT_HOTSEAT,
    EVENT_MOUNT_PLAYER,
    EVENT_MOVE_TO_ENDPOINT,
    EVENT_MOVE_TO_POINT_1,
    EVENT_MOVE_TO_POINT_2,
    EVENT_MOVE_TO_POINT_3,
    EVENT_MOVE_TO_STARTPOINT,
    EVENT_MOVEPOINT_1,
    EVENT_MOVEPOINT_2,
    EVENT_MOVEPOINT_3,
    EVENT_MOVEPOINT_4,
    EVENT_MOVEPOINT_5,
    EVENT_MOVEPOINT_6,
    EVENT_MOVEPOINT_7,
    EVENT_PERFORM_ATTACK,
    EVENT_RIDE_KODO,
    EVENT_SAY_END,
    EVENT_SHOT_ONCE,
    EVENT_SPAWN_ATTACKER,
    EVENT_START_ANIM,
    EVENT_START_CARAVAN,
    EVENT_SUMMON_CARAVAN,
    EVENT_SUMMON_KODO_PART1,
    EVENT_SUMMON_NEXT,
    EVENT_SUMMON_PACKKODO,
    EVENT_TALK_BALGOR,
    EVENT_TALK_GUNNER,
    EVENT_TALK_01,
    EVENT_TALK_02,
    EVENT_TALK_03,
    EVENT_TALK_04,
    EVENT_TALK_05,
    EVENT_TALK_06,

    PLAYER_GUID = 99999,
};

// 34285
class npc_trapped_wolf_34285 : public CreatureScript
{
public:
    npc_trapped_wolf_34285() : CreatureScript("npc_trapped_wolf_34285") { }

    struct npc_trapped_wolf_34285AI : public ScriptedAI
    {
        npc_trapped_wolf_34285AI(Creature* creature) : ScriptedAI(creature) { }

        EventMap    _events;
        bool      _isFree;

        void Reset() override
        {
            _isFree = false;
            _events.RescheduleEvent(EVENT_INIT_CHAIN, 1000);
        }

        void MovementInform(uint32 type, uint32 id) override
        {
            if (type == 2 && id == 2)
                me->DespawnOrUnsummon(10);
        }

        void DoAction(int32 param) override
        {
            if (param == 1 && _isFree == false)
            {
                _isFree = true;
                me->RemoveAura(SPELL_CHAINED_TO_STABLES);
                me->GetMotionMaster()->MovePath(PATH_FREE_TRAPPED_WOLF, false);
            }
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_INIT_CHAIN:
                {
                    if (Creature* chain = me->FindNearestCreature(NPC_CHAIN_ORIGIN, 6.0f))
                        chain->CastSpell(me, SPELL_CHAINED_TO_STABLES);
                    break;
                }
                }
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_trapped_wolf_34285AI(creature);
    }
};

// 195001, 195003, 195004
class go_wolf_chains : public GameObjectScript
{
public:
    go_wolf_chains() : GameObjectScript("go_wolf_chains") { }

    struct go_wolf_chainsAI : public GameObjectAI
    {
        go_wolf_chainsAI(GameObject* go) : GameObjectAI(go) { }

        void OnStateChanged(uint32 state, Unit* unit)
        {
            if (unit)
                if (Player* player = unit->ToPlayer())
                    if (player->GetQuestStatus(QUEST_THROUGH_FIRE_AND_FLAMES) == QUEST_STATUS_INCOMPLETE)
                        if (Creature* wolf = go->FindNearestCreature(NPC_TRAPPED_WOLF, 7.0f))
                        {
                            wolf->AI()->DoAction(1);
                            player->KilledMonsterCredit(NPC_TRAPPED_WOLF);
                        }
        }
    };

    GameObjectAI* GetAI(GameObject* go) const override
    {
        return new go_wolf_chainsAI(go);
    }
};

// quest 13949

// 34258
class npc_halga_bloodeye_34258 : public CreatureScript
{
public:
    npc_halga_bloodeye_34258() : CreatureScript("npc_halga_bloodeye_34258") { }

    bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 /*action*/) override
    {
        if (player->GetQuestStatus(QUEST_CROSSROADS_CARAVAN_PICKUP) == QUEST_STATUS_INCOMPLETE)
            if (!creature->GetAI()->GetData(DATA_IS_ANIM_STARTED))
            {
                creature->GetAI()->SetGUID(player->GetGUID(), PLAYER_GUID);
                creature->GetAI()->DoAction(ACTION_START_ANIM);
                return true;
            }
        return false;
    }

    struct npc_halga_bloodeye_34258AI : public ScriptedAI
    {
        npc_halga_bloodeye_34258AI(Creature* creature) : ScriptedAI(creature) { }

        EventMap    _events;
        ObjectGuid  _playerGUID;
        ObjectGuid  _kodoLeadGUID;
        uint32      _animIsStarted;
        uint32      _playerIsMounted;

        void Reset() override
        {
            _playerGUID = ObjectGuid::Empty;
            _kodoLeadGUID = ObjectGuid::Empty;
            _animIsStarted = 0;
            _playerIsMounted = 0;
            me->setActive(false);
            _events.RescheduleEvent(EVENT_CREATURE_TALK, 1000);
        }

        void JustSummoned(Creature* summon) override
        {
            switch (summon->GetEntry())
            {
            case NPC_LEAD_CARAVAN_KODO:
                Reset();
                break;
            }
        }

        void SummonedCreatureDespawn(Creature* summon) override
        {
            switch (summon->GetEntry())
            {
            case NPC_LEAD_CARAVAN_KODO:
                Reset();
                break;
            }
        }

        void DoAction(int32 param) override
        {
            switch (param)
            {
            case ACTION_START_ANIM: // from self OnGossipSelect
                _events.ScheduleEvent(EVENT_SUMMON_KODO_PART1, 10);
                _events.RescheduleEvent(EVENT_MASTER_RESET, 180000);
                break;
            }
        }

        void SetGUID(ObjectGuid guid, int32 id) override
        {
            switch (id)
            {
            case PLAYER_GUID: // from self
                _playerGUID = guid;
                break;
            }
        }

        uint32 GetData(uint32 id) const override
        {
            switch (id)
            {
            case DATA_IS_ANIM_STARTED: // from self
                return _animIsStarted;
            }

            return 0;
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_CHECK_PLAYER:
                {
                    if (Creature* kodo = ObjectAccessor::GetCreature(*me, _kodoLeadGUID))
                        if (kodo->IsInWorld())
                            if (Vehicle* vehicle = kodo->GetVehicleKit())
                                if (Unit* unit = vehicle->GetPassenger(1))
                                    if (Vehicle* gunner = unit->GetVehicleKit())
                                        if (Unit* pass = gunner->GetPassenger(0))
                                            if (Player* player = pass->ToPlayer())
                                                if (player->GetGUID() == _playerGUID)
                                                {
                                                    _playerIsMounted = 1;
                                                    _events.ScheduleEvent(EVENT_CHECK_PLAYER, 5000);
                                                    return;
                                                }
                    if (_playerIsMounted == 1)
                        Reset();
                    else
                        _events.ScheduleEvent(EVENT_CHECK_PLAYER, 5000);

                    break;
                }
                case EVENT_MASTER_RESET:
                {
                    Reset();
                    break;
                }
                case EVENT_CREATURE_TALK:
                {
                    std::list<Player*> pList = me->FindNearestPlayers(30.0f);
                    for (auto player : pList)
                        if (player->GetQuestStatus(QUEST_CROSSROADS_CARAVAN_PICKUP) == QUEST_STATUS_INCOMPLETE)
                            Talk(0, player);
                        else
                            Talk(3, player);

                    _events.ScheduleEvent(EVENT_CREATURE_TALK, urand(20000, 25000));
                    break;
                }
                case EVENT_CREATURE_TALK_MORE:
                {
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        Talk(2, player);
                    break;
                }
                case EVENT_SUMMON_KODO_PART1:
                {
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                    {
                        if (Pet* pet = player->GetPet())
                            player->RemovePet(pet, PET_SAVE_CURRENT_STATE, true);
                        me->CastSpell(317.4778f, -3685.98f, 27.0156f, 1.5559f, SPELL_SUMMON_KODO_PART_1, true);
                        me->CastSpell(player, SPELL_WORLD_GENERIC_DISMOUNT, true);
                        Talk(1, player);
                        _events.ScheduleEvent(EVENT_CREATURE_TALK_MORE, 10000);
                        _events.ScheduleEvent(EVENT_CHECK_PLAYER, 5000);
                    }
                    break;
                }
                }
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_halga_bloodeye_34258AI(creature);
    }
};

// 34430
class npc_lead_caravan_kodo_34430 : public CreatureScript
{
public:
    npc_lead_caravan_kodo_34430() : CreatureScript("npc_lead_caravan_kodo_34430") { }

    struct npc_lead_caravan_kodo_34430AI : public ScriptedAI
    {
        npc_lead_caravan_kodo_34430AI(Creature* creature) : ScriptedAI(creature) { }

        EventMap    _events;
        bool        _IsArrived;
        int         _razorCounter;
        ObjectGuid _halgaGUID;
        ObjectGuid _playerGUID;
        ObjectGuid _gunnerGUID;
        ObjectGuid _balgorGUID;
        ObjectGuid _kodo2GUID;

        void Reset() override
        {
            _IsArrived = false;
            _razorCounter = 0;
            _halgaGUID = ObjectGuid::Empty;
            _playerGUID = ObjectGuid::Empty;
            _gunnerGUID = ObjectGuid::Empty;
            _balgorGUID = ObjectGuid::Empty;
            _kodo2GUID = ObjectGuid::Empty;
            _events.RescheduleEvent(EVENT_MOVE_TO_STARTPOINT, 1000);
            _events.RescheduleEvent(EVENT_MASTER_RESET, 180000);
            me->SetUInt32Value(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_NPC | UNIT_FLAG_IMMUNE_TO_PC);
            // me->SetFlag(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_SPELLCLICK); // geht nicht.....  both seat are occupied.. so core has removed spellclick, and we add it again to use the spellclick..
            me->setActive(true);
        }

        void JustSummoned(Creature* summon) override
        {
            switch (summon->GetEntry())
            {
            case NPC_BALGOR_WHIPSHANK:
                _balgorGUID = summon->GetGUID();
                //summon->AI()->SetGUID(me->GetGUID(), me->GetEntry());
                break;
            case NPC_FAR_WATCH_CARAVAN_KODO:
                _kodo2GUID = summon->GetGUID();
                _events.ScheduleEvent(EVENT_INIT_SUMMON, 1000);
                break;
            case NPC_RIDING_SHOTGUN:
                _gunnerGUID = summon->GetGUID();
                // summon->AI()->SetGUID(me->GetGUID(), me->GetEntry());
                break;
            case NPC_RAZORMANE_RAIDER:
                _razorCounter += 1;
                break;
            }
        }

        void SummonedCreatureDespawn(Creature* summon) override
        {
            switch (summon->GetEntry())
            {
            case NPC_RAZORMANE_RAIDER:
                _razorCounter -= 1;
                break;
            }
        }

        void SummonedCreatureDies(Creature* summon, Unit* killer) override
        {
            switch (summon->GetEntry())
            {
            case NPC_RAZORMANE_RAIDER:
                _razorCounter -= 1;
                break;
            }
        }

        ObjectGuid GetGUID(int32 id) const override
        {
            switch (id)
            {
           // case NPC_BALGOR_WHIPSHANK:
           //     return _balgorGUID;
            case NPC_FAR_WATCH_CARAVAN_KODO:  // ask from 34487 
                return _kodo2GUID;
            case NPC_RIDING_SHOTGUN:    // ask from 34487 
                return _gunnerGUID;
            case PLAYER_GUID:           // ask from 34487 
                return _playerGUID;
            case NPC_LEAD_CARAVAN_KODO: // ask from 34487 
                return me->GetGUID();
            }
            return ObjectGuid::Empty;
        }

        void DoAction(int32 param) override
        {
            switch (param)
            {
            case ACTION_START_CARAVAN: // << from 34438 ok
                _events.ScheduleEvent(EVENT_START_CARAVAN, 2000);
                break;
            }
        }

        void PassengerBoarded(Unit* passenger, int8 seatId, bool apply) override
        {
            if (apply)
            {
                if (Player* player = passenger->ToPlayer())
                {
                    // step 1: when player klick on kodo, he will take place on seat 1.
                    // but we need gunner between player and seat 1.. and the gunner give us possibility to sit backward on kodo
                    _playerGUID = passenger->GetGUID();
                    if (!_gunnerGUID)
                        passenger->ExitVehicle();
                }
                else if (passenger->GetEntry() == NPC_RIDING_SHOTGUN)
                {
                    // step 3: the gunner is mount seat 1, now we mount the player
                    _gunnerGUID = passenger->GetGUID();
                    _events.RescheduleEvent(EVENT_MOUNT_PLAYER, 10);
                }
            }
            else
            {
                if (passenger->GetEntry() == NPC_RIDING_SHOTGUN && !_IsArrived)
                {
                    me->CastSpell(me, SPELL_DISMOUNT_CARAVAN_KODO_CUE1);
                    me->DespawnOrUnsummon(10);
                }
                else if (passenger->ToPlayer())
                {
                    // step 2: when player leave and the gunner is not present, we use this as starting event
                    // we spawn the gunner and let then player mount the gunner
                    if (!_gunnerGUID)
                    {
                        _playerGUID = passenger->GetGUID();
                        _events.RescheduleEvent(EVENT_MOUNT_GUNNER, 10);
                    }
                }
            }
        }

        void MovementInform(uint32 type, uint32 id) override
        {
            switch (type)
            {
            case WAYPOINT_MOTION_TYPE:
            {
                if (id == 1)
                    _events.ScheduleEvent(EVENT_TALK_GUNNER, 100);
                else if (id > 2 && id < 18)
                    _events.ScheduleEvent(EVENT_SPAWN_ATTACKER, 100);
                else if (id == 21)
                    _events.ScheduleEvent(EVENT_TALK_BALGOR, 100);
                else if (id == 22)
                    _events.ScheduleEvent(EVENT_ARRIVED_HOME, 100);
            }
            }
            _events.RescheduleEvent(EVENT_MASTER_RESET, 60000);
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_MASTER_RESET:
                {
                    if (Vehicle* vehicle = me->GetVehicleKit())
                        vehicle->RemoveAllPassengers();

                    me->DespawnOrUnsummon(10);
                    break;
                }
                case EVENT_MOVE_TO_STARTPOINT:
                {
                    me->GetMotionMaster()->MovePoint(121, 318.54f, -3670.49f, 27.18f);
                    me->CastSpell(316.0f, -3700.0f, 27.0268f, 1.5559f, SPELL_CALL_A_PACK_KODO, true);
                    break;
                }
                case EVENT_MOUNT_GUNNER:
                {
                    TempSummon* accessory = me->SummonCreature(NPC_RIDING_SHOTGUN, *me, TempSummonType::TEMPSUMMON_CORPSE_TIMED_DESPAWN, 180000);
                    accessory->AddUnitTypeMask(UNIT_MASK_ACCESSORY);
                    me->HandleSpellClick(accessory, 1);
                    break;
                }
                case EVENT_MOUNT_PLAYER:
                {
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        if (Creature* gunner = ObjectAccessor::GetCreature(*me, _gunnerGUID))
                            gunner->CastSpell(player, SPELL_MOUNT_UP_65487, true);
                    break;
                }
                case EVENT_START_CARAVAN:
                {
                    if (Creature* gunner = ObjectAccessor::GetCreature(*me, _gunnerGUID))
                        gunner->CastSpell(me, SPELL_GIDDYAP, true);
                    if (Creature* kodo2 = ObjectAccessor::GetCreature(*me, _kodo2GUID))
                    {
                        kodo2->GetMotionMaster()->MoveFollow(me, 8.0f, 3.14f);
                        kodo2->SetFlag(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_SPELLCLICK);
                    }
                    break;
                }
                case EVENT_INIT_SUMMON:
                {
                    if (Creature* kodo2 = ObjectAccessor::GetCreature(*me, _kodo2GUID))
                    {
                        kodo2->SetReactState(ReactStates::REACT_PASSIVE);
                        kodo2->RemoveFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_REMOVE_CLIENT_CONTROL);
                        kodo2->RemoveFlag(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_SPELLCLICK);
                        kodo2->SetUInt32Value(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_NPC | UNIT_FLAG_IMMUNE_TO_PC);
                        kodo2->GetMotionMaster()->MovePoint(123, 317.36f, -3687.41f, 27.129f);
                    }
                    break;
                }
                case EVENT_TALK_GUNNER:
                {
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        if (Creature* gunner = ObjectAccessor::GetCreature(*me, _gunnerGUID))
                            gunner->AI()->Talk(0, player);
                    break;
                }
                case EVENT_TALK_BALGOR:
                {
                    if (Creature* balgor = ObjectAccessor::GetCreature(*me, _balgorGUID))
                        balgor->AI()->Talk(0);
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        if (Creature* gunner = ObjectAccessor::GetCreature(*me, _gunnerGUID))
                            gunner->CastSpell(player, SPELL_QUEST_COMPLETE_13949);
                    break;
                }
                case EVENT_SPAWN_ATTACKER:
                {
                    if (Creature* gunner = ObjectAccessor::GetCreature(*me, _gunnerGUID))
                        if (_razorCounter < irand(3, 5))
                        {
                            me->CastSpell(*gunner, SPELL_SUMMON_ANGRY_RAZORMANE_RAIDER, true);
                            _events.RescheduleEvent(EVENT_SPAWN_ATTACKER, urand(250, 5000));
                        }

                    break;
                }
                case EVENT_ARRIVED_HOME:
                {
                    _IsArrived = true;
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        player->ExitVehicle();
                    me->DespawnOrUnsummon(10);
                    break;
                }
                }
            }

            if (!UpdateVictim())
                return;

            DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_lead_caravan_kodo_34430AI(creature);
    }
};

// 34438
class vehicle_riding_shotgun_34438 : public VehicleScript
{
public:
    vehicle_riding_shotgun_34438() : VehicleScript("npc_riding_shotgun_34438") { }

    void OnAddPassenger(Vehicle* veh, Unit* passenger, int8 /*seatId*/) override
    {
        passenger->SetFacingTo(3.14f);
        veh->RelocatePassengers();
        if (Creature* gunner = veh->GetBase()->ToCreature())
            gunner->AI()->SetGUID(passenger->GetGUID(), PLAYER_GUID);
    }

    void OnRemovePassenger(Vehicle* veh, Unit* passenger) override
    {
        if (Creature* gunner = veh->GetBase()->ToCreature())
            gunner->DespawnOrUnsummon(100);
    }

    struct vehicle_riding_shotgun_34438AI : public ScriptedAI
    {
        vehicle_riding_shotgun_34438AI(Creature* creature) : ScriptedAI(creature) { }

        EventMap    _events;
        ObjectGuid _playerGuid;
        ObjectGuid _kodo1Guid;

        void Reset() override
        {
            _playerGuid = ObjectGuid::Empty;
            _kodo1Guid = ObjectGuid::Empty;
        }

        void IsSummonedBy(Unit* summoner) override
        {
            if (summoner->GetEntry() == NPC_LEAD_CARAVAN_KODO)
                _kodo1Guid = summoner->GetGUID();
        }

        void SetGUID(ObjectGuid guid, int32 id) override
        {
            switch (id)
            {
            case PLAYER_GUID: // self : OnAddPassenger
                _playerGuid = guid;
                if (Creature* kodo1 = ObjectAccessor::GetCreature(*me, _kodo1Guid))
                    kodo1->AI()->DoAction(ACTION_START_CARAVAN);
                _events.ScheduleEvent(EVENT_INIT_SUMMON, 500);
                break;
            }
        }

        void DoAction(int32 param) override
        {
            switch (param)
            {
            case 11: // << from 34487 ok
                me->SetHealth(me->GetHealth() - 1);
                break;
            }
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_INIT_SUMMON:
                {
                    // place for insert visual swap for weapon: Halga's Street Sweeper
                    break;
                }
                }
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new vehicle_riding_shotgun_34438AI(creature);
    }
};

// 34487
class npc_razormane_raider_34487 : public CreatureScript
{
public:
    npc_razormane_raider_34487() : CreatureScript("npc_razormane_raider_34487") { }

    struct npc_razormane_raider_34487AI : public ScriptedAI
    {
        npc_razormane_raider_34487AI(Creature* creature) : ScriptedAI(creature) { }

        EventMap    _events;
        bool        _isRiding;
        float       _angle;
        float       _dist;
        int32       _counter;
        uint32      _spellHit;
        ObjectGuid  _kodo1GUID;
        ObjectGuid  _kodo2GUID;
        ObjectGuid  _playerGUID;
        ObjectGuid  _gunnerGUID;

        void Reset() override
        {
            _isRiding = false;
            _spellHit = 0;
            _angle = 0;
            _dist = 0;
            _counter = 0;
            _kodo1GUID = ObjectGuid::Empty;
            _kodo2GUID = ObjectGuid::Empty;
            _gunnerGUID = ObjectGuid::Empty;
            _playerGUID = ObjectGuid::Empty;
            _dist = frand(8.0f, 12.0f);
            _angle = RAND(1.2f, 5.08f);
            float x, y, z;
            if (Creature* kodo1 = ObjectAccessor::GetCreature(*me, _kodo1GUID))
            {
                me->GetNearPoint(kodo1, x, y, z, 20.0f, _dist, _angle); // the razor is spawn on kodo1 position 
                me->NearTeleportTo(x, y, z, 0.0f); // and we teleport him to start position, a bit behind the player visual point..
            }
        }

        void IsSummonedBy(Unit* summoner) override
        {
            if (Creature* npc = summoner->ToCreature())
            {
                _kodo1GUID = npc->AI()->GetGUID(NPC_LEAD_CARAVAN_KODO);
                _kodo2GUID = npc->AI()->GetGUID(NPC_FAR_WATCH_CARAVAN_KODO);
                _playerGUID = npc->AI()->GetGUID(PLAYER_GUID);
                _gunnerGUID = npc->AI()->GetGUID(NPC_RIDING_SHOTGUN);
                _events.ScheduleEvent(EVENT_INIT_SUMMON, 10);
                _events.ScheduleEvent(EVENT_CHECK_CREATURE, 1000);
            }
        }

        void DamageDealt(Unit* victim, uint32& damage, DamageEffectType /*damageType*/, SpellInfo const* /*spellInfo*/) override
        {
            if (victim->GetEntry() == NPC_RIDING_SHOTGUN)
                damage = 1;
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_INIT_SUMMON:
                {
                    if (Creature* kodo2 = ObjectAccessor::GetCreature(*me, _kodo2GUID))
                    {
                        if (_angle < 3.14f)
                            _angle = frand(0.9f, 2.1f);
                        else
                            _angle = frand(4.0f, 5.2f);
                        _dist = frand(0.1f, 1.5f);
                        me->SetSpeedRate(MOVE_WALK, 8.1f);
                        me->SetSpeedRate(MOVE_RUN, 8.2f);
                        me->SetWalk(false);
                        me->GetMotionMaster()->MoveFollow(kodo2, _dist, _angle);
                        _events.RescheduleEvent(EVENT_PERFORM_ATTACK, 100);
                        return;
                    }

                    me->DespawnOrUnsummon(10);
                    break;
                }
                case EVENT_CHECK_CREATURE:
                {
                    if (Creature* kodo2 = ObjectAccessor::GetCreature(*me, _kodo2GUID))
                    {
                     if (kodo2->GetDistance(me->GetPosition()) > 30.0f)
                        me->DespawnOrUnsummon(10);
                    }
                    else
                        me->DespawnOrUnsummon(10);

                    _events.ScheduleEvent(EVENT_CHECK_CREATURE, 1000);
                    break;
                }
                case EVENT_PERFORM_ATTACK:
                {
                    if (Creature* kodo2 = ObjectAccessor::GetCreature(*me, _kodo2GUID))
                    {
                        if (_isRiding)
                        {
                            _counter -= 1;
                            if (_counter > 0)
                                break;

                            if (GetEmptySeat(kodo2))
                            {
                                me->ExitVehicle();
                                _isRiding = false;
                            }
                        }

                        float dist = kodo2->GetDistance(me->GetPosition());
                        if (dist < 7.0f)
                        {
                            uint8 seat = GetEmptySeat(kodo2);
                            switch (seat)
                            {
                            case 0:
                                me->CastSpell(kodo2, SPELL_RIDE_VEHICLE1, true);
                                _isRiding = true;
                                break;
                            case 1:
                                me->CastSpell(kodo2, SPELL_RIDE_VEHICLE2, true);
                                _isRiding = true;
                                break;
                            case 2:
                                me->CastSpell(kodo2, SPELL_RIDE_VEHICLE3, true);
                                _isRiding = true;
                                break;
                            case 3:
                                me->CastSpell(kodo2, SPELL_RIDE_VEHICLE4, true);
                                _isRiding = true;
                                break;
                            case 4:
                                me->CastSpell(kodo2, SPELL_RIDE_VEHICLE5, true);
                                _isRiding = true;
                                break;
                            }
                            me->SetReactState(ReactStates::REACT_AGGRESSIVE);
                            _counter = urand(4, 15);
                        }
                    }
                    _events.ScheduleEvent(EVENT_PERFORM_ATTACK, 500);
                    break;
                }
                }
            }

            if (!UpdateVictim())
                return;

            DoMeleeAttackIfReady();
        }

        uint8 GetEmptySeat(Creature* kodo2)
        {
            if (Vehicle* vehicle = kodo2->GetVehicleKit())
                for (auto itr : vehicle->Seats)
                    if (itr.second.Passenger.Guid.IsEmpty())
                        return itr.first;

            return 255;
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_razormane_raider_34487AI(creature);
    }
};

// 65493
class spell_giddyap_65493 : public SpellScriptLoader
{
public:
    spell_giddyap_65493() : SpellScriptLoader("spell_giddyap_65493") { }

    class spell_giddyap_65493_SpellScript : public SpellScript
    {
        PrepareSpellScript(spell_giddyap_65493_SpellScript);

        bool Validate(SpellInfo const* /*spellInfo*/) override
        {
            return ValidateSpellInfo({ 65493 });
        }

        void HandleEffectDummy(SpellEffIndex /*effIndex*/) 
        {
            if (!GetCaster() || !GetHitUnit())
                return;

            GetHitUnit()->GetMotionMaster()->MovePath(PATH_LEAD_CARAVAN_KODO, false);
        }

        void Register() override
        {
            OnEffectHitTarget += SpellEffectFn(spell_giddyap_65493_SpellScript::HandleEffectDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
        }
    };

    SpellScript* GetSpellScript() const override
    {
        return new spell_giddyap_65493_SpellScript();
    }
};

/* ######################################### */

// 34503 
class npc_razormane_pillager_34503 : public CreatureScript
{
public:
    npc_razormane_pillager_34503() : CreatureScript("npc_razormane_pillager_34503") { }

    bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action) override
    {
        if (player->GetQuestStatus(QUEST_DRAG_IT_OUT_OF_THEM) == QUEST_STATUS_INCOMPLETE)
            if (action == 1)
            {
                creature->SetHomePosition(creature->GetPosition());
                player->CastSpell(creature, SPELL_GET_A_HOGTIED_RAZORMANE);
                creature->AI()->SetGUID(player->GetGUID(), PLAYER_GUID);
                creature->AI()->DoAction(1);
                return true;
            }

        return false;
    }

    struct npc_razormane_pillager_34503AI : public ScriptedAI
    {
        npc_razormane_pillager_34503AI(Creature* creature) : ScriptedAI(creature) { }

        uint32 m_timer;
        uint32 m_phase;
        ObjectGuid m_playerGUID;

        void Reset() override
        {
            m_timer = 0;
            m_phase = 0;
            m_playerGUID = ObjectGuid::Empty;
        }

        void SpellHit(Unit* caster, SpellInfo const* spell) override
        {
            if (spell->Id == SPELL_GROLDOM_NET)
            {
                me->setFaction(1933);
                me->SetUInt32Value(UNIT_NPC_FLAGS, UNIT_NPC_FLAGS);
                me->SetUInt32Value(UNIT_FIELD_FLAGS, UNIT_FLAG_RENAME || UNIT_FLAG_STUNNED || UNIT_FLAG_IN_COMBAT || UNIT_FLAG_REMOVE_CLIENT_CONTROL);
                me->CastSpell(me, SPELL_SNARED_IN_NET, true);
                me->SetDisplayId(1344);
                Talk(1);
            }
        }

        void SetGUID(ObjectGuid guid, int32 id = 0) override
        {
            switch (id)
            {
            case 99999:
                if (!m_playerGUID)
                    m_playerGUID = guid;
                break;
            }
        }

        ObjectGuid GetGUID(int32 id = 0) const override
        {
            switch (id)
            {
            case 99999:
                return m_playerGUID;
            default:
                return ObjectGuid::Empty;
            }
        }

        void DoAction(int32 param) override
        {
            switch (param)
            {
            case 1:
                m_phase = 1;
                m_timer = 1000;
                break;
            default:
                break;
            }
        }

        void UpdateAI(uint32 diff) override
        {
            if (m_timer < diff)
            {
                m_timer = 1000;
                if (m_phase)
                    DoWork();
            }
            else
                m_timer -= diff;

            if (!UpdateVictim())
                return;
            else
                DoMeleeAttackIfReady();
        }

        void DoWork()
        {
            switch (m_phase)
            {
            case 1:
            {
                me->CastSpell(me, SPELL_PERMANENT_FEIGN_DEATH);
                if (Player* player = ObjectAccessor::GetPlayer(*me, m_playerGUID))
                {
                    player->CastSpell(player, SPELL_SUMMON_HOGTIED_RAZORMANE);

                }
                m_phase = 2;
                m_timer = 50;
                break;
            }
            case 2:
            {
                if (Creature* raz = me->FindNearestCreature(34514, 10.0f))
                    raz->AI()->SetGUID(m_playerGUID, PLAYER_GUID);
                me->DespawnOrUnsummon(200);
                m_phase = 0;
                m_timer = 0;
                break;
            }
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_razormane_pillager_34503AI(creature);
    }
};

// 34514
class npc_hogtied_razormane_34514 : public CreatureScript
{
public:
    npc_hogtied_razormane_34514() : CreatureScript("npc_hogtied_razormane_34514") { }

    struct npc_hogtied_razormane_34514AI : public ScriptedAI
    {
        npc_hogtied_razormane_34514AI(Creature* creature) : ScriptedAI(creature) { Initialize(); }

        uint32 m_timer;
        uint32 m_phase;
        ObjectGuid m_playerGUID;

        void Initialize()
        {
            m_timer = 0;
            m_phase = 0;
            m_playerGUID = ObjectGuid::Empty;
        }

        void IsSummonedBy(Unit* summoner) override
        {
            if (Player* player = summoner->ToPlayer())
            {
                m_playerGUID = player->GetGUID();
                me->CastSpell(me, SPELL_DEAD_SOLDIER);
                me->CastSpell(player, SPELL_DRAGGING_A_RAZORMANE);
                me->CastSpell(me, SPELL_RAZORMANE_DRAGGING_AURA, true);
                player->AddAura(SPELL_DRAGGING_A_RAZORMANE, me);
                player->AddAura(SPELL_RAZORMANE_DRAGGING_AURA, me);
                player->CastSpell(me, SPELL_HOGTIED_RAZORMANE_ROPE);
                m_phase = 1;
                m_timer = 1000;
            }
        }

        void SetGUID(ObjectGuid guid, int32 id = 0) override
        {
            switch (id)
            {
            case PLAYER_GUID:
                if (!m_playerGUID)
                    m_playerGUID = guid;
                break;
            }
        }

        ObjectGuid GetGUID(int32 id = 0) const override
        {
            switch (id)
            {
            case PLAYER_GUID:
                return m_playerGUID;
            default:
                return ObjectGuid::Empty;
            }
        }

        void UpdateAI(uint32 diff) override
        {
            if (m_timer < diff)
            {
                m_timer = 1000;
                if (m_phase)
                    DoWork();
            }
            else
                m_timer -= diff;

            if (!UpdateVictim())
                return;
            else
                DoMeleeAttackIfReady();
        }

        void DoWork()
        {
            switch (m_phase)
            {
                case 1:
                {
                    if (Player* player = ObjectAccessor::GetPlayer(*me, m_playerGUID))
                    {
                        if (player->GetQuestStatus(QUEST_DRAG_IT_OUT_OF_THEM) == QUEST_STATUS_COMPLETE)
                        {
                            player->RemoveAura(SPELL_HOGTIED_RAZORMANE_ROPE);
                            player->RemoveAura(SPELL_DRAGGING_A_RAZORMANE);
                            me->DespawnOrUnsummon(0);
                            m_phase = 0;
                            m_timer = 0;
                        }
                        else
                        {
                            if (!me->HasAura(SPELL_HOGTIED_RAZORMANE_ROPE))
                                player->CastSpell(me, SPELL_HOGTIED_RAZORMANE_ROPE);
                            me->CastSpell(me, SPELL_RAZORMANE_DRAGGING_TRIGGER);
                        }
                    }
                    break;
                }
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_hogtied_razormane_34514AI(creature);
    }
};

// 5465  AreaTrigger
class at_groldoms_farm_5465 : public AreaTriggerScript
{
public:
    at_groldoms_farm_5465() : AreaTriggerScript("at_groldoms_farm_5465") { }

    bool OnTrigger(Player* player, AreaTriggerEntry const* trigger, bool /*entered*/) 
    //bool OnTrigger(Player* player, const AreaTriggerEntry* at) override
    {
        if (trigger->ID == AT_GROLDOMS_FARM)
            if ((player->GetQuestStatus(QUEST_DRAG_IT_OUT_OF_THEM) == QUEST_STATUS_INCOMPLETE))
            {
                player->CompleteQuest(QUEST_DRAG_IT_OUT_OF_THEM);
                player->CastSpell(player, SPELL_DELIVER_HOGTIED_RAZORMANE);

            }

        return false;
    }
};

/* ################################################# */

// 34513 
class npc_togrik_34513 : public CreatureScript
{
public:
    npc_togrik_34513() : CreatureScript("npc_togrik_34513") { }

    bool OnGossipHello(Player* player, Creature* creature) override
    {
        if (player->GetQuestStatus(QUEST_BY_HOOK_OR_BY_CROOK) == QUEST_STATUS_INCOMPLETE)
        {
            creature->AI()->SetGUID(player->GetGUID(), PLAYER_GUID);
            creature->AI()->DoAction(ACTION_GOSSIP_HELLO);
            return true;
        }
        else
        {
            SendGossipMenuFor(player, 14554, creature->GetGUID());
        }

        return false;
    }

    bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action) override
    {
        if (action >= 1000)
        {
            creature->AI()->SetData(DATA_MENU_ID, action);
            creature->AI()->DoAction(ACTION_GOSSIP_SELECT);
            return true;
        }

        return false;
    }

    bool OnQuestAccept(Player* player, Creature* creature, Quest const* quest) override
    {
        if (quest->GetQuestId() == QUEST_BY_HOOK_OR_BY_CROOK)
        {
            if (creature->AI()->GetData(NPC_TOGRIK) == 0)
            {
                creature->AI()->SetGUID(player->GetGUID(), PLAYER_GUID);
                creature->AI()->DoAction(1);
            }
            else
            {
                creature->AI()->DoAction(2);
            }
        }

        return false;
    }

    struct npc_togrik_34513AI : public ScriptedAI
    {
        npc_togrik_34513AI(Creature* creature) : ScriptedAI(creature) { Initialize(); }

        EventMap m_events;
        uint32   m_phase;
        uint32   m_action_id;
        ObjectGuid   m_playerGUID;
        ObjectGuid   m_razorGUID;
        uint8    m_encounter;

        void Initialize()
        {
            m_encounter = 0;
        }

        void Reset() override
        {
            m_events.Reset();
            m_phase = 0;
            m_action_id = 0;
            m_playerGUID = ObjectGuid::Empty;
            m_razorGUID = ObjectGuid::Empty;
        }

        void SetData(uint32 id, uint32 value) override
        {
            switch (id)
            {
            case DATA_MENU_ID:
                m_action_id = value;
                break;
            }
        }

        uint32 GetData(uint32 id) const override
        {
            switch (id)
            {
            case NPC_TOGRIK:
                return m_encounter;
            default:
                return 0;
            }
        }

        void SetGUID(ObjectGuid guid, int32 id = 0) override
        {
            switch (id)
            {
            case PLAYER_GUID:
                if (!m_playerGUID)
                    m_playerGUID = guid;
                break;
            }
        }

        ObjectGuid GetGUID(int32 id = 0) const override
        {
            switch (id)
            {
            case PLAYER_GUID:
                return m_playerGUID;
            default:
                return ObjectGuid::Empty;
            }
        }

        void DoAction(int32 param) override
        {
            switch (param)
            {
            case ACTION_CREATE_CHAIN:
            {
                CreateRazor();
                break;
            }
            case ACTION_RESET:
            {
                Initialize();
                Reset();
                break;
            }
            case ACTION_GOSSIP_HELLO:
            {
                if (Player* player = ObjectAccessor::GetPlayer(*me, m_playerGUID))
                {
                    AddGossipItemFor(player, 10520, 0, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);
                    SendGossipMenuFor(player, 14554, me->GetGUID());
                }
                break;
            }
            case ACTION_GOSSIP_SELECT:
            {
                switch (m_action_id)
                {
                case 1002:
                {
                    Creature* razor = ObjectAccessor::GetCreature(*me, m_razorGUID);
                    if (!razor)
                    {
                        CreateRazor();
                    }
                    break;
                }
                }
                break;
            }
            }
        }

        void UpdateAI(uint32 diff) override
        {
            m_events.Update(diff);

            while (uint32 eventId = m_events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_MAX_TIME:
                    if (Creature* razor = ObjectAccessor::GetCreature(*me, m_razorGUID))
                        razor->DespawnOrUnsummon();
                    Initialize();
                    Reset();
                    break;
                default:
                    break;
                }
            }

            if (!UpdateVictim())
                return;
            else
                DoMeleeAttackIfReady();
        }

        void CreateRazor()
        {
            m_encounter = 1;
            m_events.ScheduleEvent(EVENT_MAX_TIME, 600000);
            if (Creature* razor = me->SummonCreature(NPC_CAPTURED_RAZORMANE, 283.4774f, -3050.653f, 95.93713f, 3.490659f, TEMPSUMMON_TIMED_DESPAWN, 600000))
            {
                m_razorGUID = razor->GetGUID();
                razor->AI()->SetGUID(m_playerGUID);
                razor->AI()->DoAction(ACTION_CREATE_CHAIN);
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_togrik_34513AI(creature);
    }
};

// 34523 
class npc_captured_razormane_34523 : public CreatureScript
{
public:
    npc_captured_razormane_34523() : CreatureScript("npc_captured_razormane_34523") { }

    bool OnGossipHello(Player* player, Creature* creature) override
    {
        creature->AI()->SetGUID(player->GetGUID(), PLAYER_GUID);
        creature->AI()->DoAction(ACTION_GOSSIP_HELLO);
        return true;
    }

    bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action) override
    {
        creature->AI()->SetData(DATA_ACTION_ID, action);
        creature->AI()->DoAction(ACTION_GOSSIP_SELECT);
        return true;
    }

    struct npc_captured_razormane_34523AI : public ScriptedAI
    {
        npc_captured_razormane_34523AI(Creature* creature) : ScriptedAI(creature) { }

        EventMap m_events;
        uint32   m_phase;
        uint32   m_action_id;
        ObjectGuid   m_playerGUID;
        ObjectGuid   m_bunnyGUID;
        ObjectGuid   m_togrikGUID;

        void Reset() override
        {
            m_events.Reset();
            m_phase = 0;
            m_action_id = 0;
            m_playerGUID = ObjectGuid::Empty;
            m_togrikGUID = ObjectGuid::Empty;

            if (Creature* bunny = me->FindNearestCreature(NPC_DAVES_INDUSTRIAL_LIGHT_AND_MAGIC_BUNNY, 25.0f))
                m_bunnyGUID = bunny->GetGUID();
            else
                m_bunnyGUID = ObjectGuid::Empty;
        }

        void JustDied(Unit* /*killer*/) override
        {
            if (Creature* togrik = ObjectAccessor::GetCreature(*me, m_togrikGUID))
                togrik->AI()->DoAction(ACTION_RESET);
        }

        void SetData(uint32 id, uint32 value) override
        {
            switch (id)
            {
            case DATA_ACTION_ID:
                m_action_id = value;
                break;
            }
        }

        void SetGUID(ObjectGuid guid, int32 id = 0) override
        {
            switch (id)
            {
            case NPC_DAVES_INDUSTRIAL_LIGHT_AND_MAGIC_BUNNY:
                m_bunnyGUID = guid;
                break;
            case NPC_TOGRIK:
                m_togrikGUID = guid;
                break;
            case PLAYER_GUID:
                if (!m_playerGUID)
                    m_playerGUID = guid;
                break;
            }
        }

        ObjectGuid GetGUID(int32 id = 0) const override
        {
            switch (id)
            {
            case NPC_DAVES_INDUSTRIAL_LIGHT_AND_MAGIC_BUNNY:
                return m_bunnyGUID;
            case NPC_TOGRIK:
                return m_togrikGUID;
            case PLAYER_GUID:
                return m_playerGUID;
            default:
                return ObjectGuid::Empty;
            }
        }

        void DoAction(int32 param) override
        {
            switch (param)
            {
            case ACTION_CREATE_CHAIN:
            {
                me->SetUInt32Value(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_GOSSIP);
                if (Creature* bunny = ObjectAccessor::GetCreature(*me, m_bunnyGUID))
                {
                    bunny->CastSpell(me, SPELL_COSMETIC_CHAINS);
                    me->SetSpeedRate(MOVE_WALK, 0.5f);
                    me->GetMotionMaster()->MovePath(PATH_RAZORMANE, true);
                }
                break;
            }
            case ACTION_GOSSIP_HELLO:
            {
                Player* player = ObjectAccessor::GetPlayer(*me, m_playerGUID);
                if (!player)
                    return;

                switch (m_phase)
                {
                case 0:
                {
                    AddGossipItemFor(player, 10521, 0, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 10);
                    AddGossipItemFor(player, 10521, 1, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 11);
                    AddGossipItemFor(player, 10521, 2, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 12);
                    AddGossipItemFor(player, 10521, 3, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 13);
                    AddGossipItemFor(player, 10521, 4, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 14);
                    SendGossipMenuFor(player, 14555, me->GetGUID());
                    break;
                }
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                {
                    AddGossipItemFor(player, 10521, 0, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 10);
                    AddGossipItemFor(player, 10521, 1, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 11);
                    AddGossipItemFor(player, 10521, 2, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 12);
                    SendGossipMenuFor(player, 14555, me->GetGUID());
                    break;
                }
                }
                break;
            }
            case ACTION_GOSSIP_SELECT:
            {
                Player* player = ObjectAccessor::GetPlayer(*me, m_playerGUID);
                if (!player)
                    return;

                switch (m_action_id)
                {
                case 1010:
                    player->CastSpell(me, 65618);
                    break;
                case 1011:
                    player->CastSpell(me, 65620);
                    break;
                case 1012:
                    player->CastSpell(me, 65619);
                    break;
                case 1013:
                    player->CastSpell(me, 65618);
                    break;
                case 1014:
                    player->CastSpell(me, 65621);
                    break;
                }

                Talk(m_phase);
                me->HandleEmoteCommand(34);
                player->PlayerTalkClass->SendCloseGossip();
                m_phase++;

                if (m_phase >= 5)
                {
                    player->KilledMonsterCredit(34529);
                    m_events.RescheduleEvent(EVENT_SAY_END, 5000);
                }
                break;
            }
            }
        }

        void UpdateAI(uint32 diff) override
        {
            m_events.Update(diff);

            while (uint32 eventId = m_events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_SAY_END:
                {
                    Talk(m_phase);
                    me->DespawnOrUnsummon(10000);
                    break;
                }
                }
            }

            if (!UpdateVictim())
                return;
            else
                DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_captured_razormane_34523AI(creature);
    }
};

/* ################################################# */

// 34547 
class npc_groldom_kodo_34547 : public CreatureScript
{
public:
    npc_groldom_kodo_34547() : CreatureScript("npc_groldom_kodo_34547") { }

    bool OnQuestAccept(Player* player, Creature* creature, Quest const* quest) override
    {
        if (quest->GetQuestId() == QUEST_THE_KODOS_RETURN)
        {
            creature->RemoveAura(54852);
            creature->SetUInt32Value(UNIT_FIELD_BYTES_1, 0);
            creature->SetUInt32Value(UNIT_FIELD_BYTES_2, 0);
            creature->GetMotionMaster()->MovePoint(1, 50.17f, -3129.29f, 96.67f, true);
            creature->DespawnOrUnsummon(10000);
            return true;
        }

        return false;
    }

    bool OnQuestReward(Player* player, Creature* creature, Quest const* quest, uint32 /*opt*/) override
    {
        if (quest->GetQuestId() == QUEST_ANIMAL_SERVICES)
            creature->AI()->Talk(0, player);

        return false;
    }

};

// 34543 Tortusk is not minion, so when we kill Fez, we have to stay in fight against tortusk.
// when later Fez respawn, Tortusk is new spawned as accewssory, so we must despawn each living Tortusk around.
class npc_fez_34543 : public CreatureScript
{
public:
    npc_fez_34543() : CreatureScript("npc_fez_34543") { }

    struct npc_fez_34543AI : public ScriptedAI
    {
        npc_fez_34543AI(Creature* creature) : ScriptedAI(creature) {  }

        EventMap m_events;

        void Reset() override
        {
            m_events.Reset();
            m_events.RescheduleEvent(1, 1000);
        }

        void UpdateAI(uint32 diff) override
        {
            m_events.Update(diff);

            while (uint32 eventId = m_events.ExecuteEvent())
            {
                switch (eventId)
                {
                case 1:
                    m_events.ScheduleEvent(1, 2500);
                    if (me->IsInCombat())
                        break;

                    CreatureList tortusks = me->FindNearestCreatures(34544, 50.0f);
                    if (!tortusks.size())
                        break;

                    Unit* passenger = nullptr;
                    if (Vehicle* fez = me->GetVehicleKit())
                        passenger = fez->GetPassenger(0);

                    if (!passenger)
                        break;

                    for (Creature* tortusk : tortusks)
                        if (passenger->GetGUID() != tortusk->GetGUID())
                            tortusk->DespawnOrUnsummon(10);

                    break;
                }
            }

            if (!UpdateVictim())
                return;
            else
                DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_fez_34543AI(creature);
    }
};

/* ################################################# */

// 34578
class npc_rocco_whipshank_34578 : public CreatureScript
{
public:
    npc_rocco_whipshank_34578() : CreatureScript("npc_rocco_whipshank_34578") { }

    bool OnGossipHello(Player* player, Creature* creature) override
    {
        if (player->GetQuestStatus(QUEST_CROSSROADS_CARAVAN_DELIVERY) == QUEST_STATUS_INCOMPLETE)
        {
            creature->AI()->SetGUID(player->GetGUID(), PLAYER_GUID);
            creature->AI()->DoAction(ACTION_GOSSIP_HELLO);
            return true;
        }

        return false;
    }

    bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action) override
    {
        if (action == 1002)
        {
            creature->AI()->SetGUID(player->GetGUID(), PLAYER_GUID);
            creature->AI()->DoAction(ACTION_GOSSIP_SELECT);
            return true;
        }

        return false;
    }

    struct npc_rocco_whipshank_34578AI : public ScriptedAI
    {
        npc_rocco_whipshank_34578AI(Creature* creature) : ScriptedAI(creature) { }

        EventMap m_events;
        uint32   m_phase;
        uint32   m_is_anim_started;
        ObjectGuid   m_playerGUID;
        ObjectGuid   m_kodoLeader;
        ObjectGuid   m_kodoPack;

        void Reset() override
        {
            m_phase = 0;
            m_is_anim_started = false;
            m_playerGUID = ObjectGuid::Empty;
            m_kodoLeader = ObjectGuid::Empty;
            m_kodoPack = ObjectGuid::Empty;
            me->setActive(false);
        }

        void JustSummoned(Creature* summon) override
        {
            switch (summon->GetEntry())
            {
            case NPC_HEAD_CARAVAN_KODO:
                m_kodoLeader = summon->GetGUID();
                summon->AI()->SetGUID(m_playerGUID, PLAYER_GUID);
                summon->AI()->SetGUID(me->GetGUID(), me->GetEntry());
                //summon->SummonCreature(NPC_BALGOR_WHIPSHANK, 220.5f, -2964.5f, 91.82f);
                break;
            }
        }

        void SetData(uint32 id, uint32 value) override
        {
            switch (id)
            {
            case DATA_IS_ANIM_STARTED:
                m_is_anim_started = value;
                break;
            }
        }

        uint32 GetData(uint32 id) const override
        {
            switch (id)
            {
            case DATA_IS_ANIM_STARTED:
                return m_is_anim_started;
            default:
                return 0;
            }
        }

        void SetGUID(ObjectGuid guid, int32 id = 0) override
        {
            switch (id)
            {
            case PLAYER_GUID:
                m_playerGUID = guid;
                break;
            }
        }

        ObjectGuid GetGUID(int32 id = 0) const override
        {
            switch (id)
            {
            case PLAYER_GUID:
                return m_playerGUID;
            default:
                return ObjectGuid::Empty;
            }
        }

        void DoAction(int32 param) override
        {
            switch (param)
            {
            case ACTION_RESET: // when kodo is leave visual
            {
                Reset();
                break;
            }
            case ACTION_GOSSIP_HELLO: // from self
            {
                if (Player* player = ObjectAccessor::GetPlayer(*me, m_playerGUID))
                    if (!m_is_anim_started)
                    {
                        AddGossipItemFor(player, 10528, 0, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 2);
                        SendGossipMenuFor(player, 14566, me->GetGUID());
                    }
                break;
            }
            case ACTION_GOSSIP_SELECT: // from self
            {
                if (Player* player = ObjectAccessor::GetPlayer(*me, m_playerGUID))
                {
                    me->setActive(true);
                    m_is_anim_started = true;
                    m_events.ScheduleEvent(EVENT_MAX_TIME, 240000);
                    me->CastSpell(player, SPELL_SUMMON_KODO_PART2);
                    me->CastSpell(player, SPELL_WORLD_GENERIC_DISMOUNT_CANCEL_SHAPESHIFT);
                }
                break;
            }
            }
        }

        void UpdateAI(uint32 diff) override
        {
            m_events.Update(diff);

            while (uint32 eventId = m_events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_MAX_TIME:
                    // delete kodoleader mm
                    Reset();
                    break;
                default:
                    break;
                }
            }

            if (!UpdateVictim())
                return;
            else
                DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_rocco_whipshank_34578AI(creature);
    }
};

// 34577
class npc_head_caravan_kodo_34577 : public CreatureScript
{
public:
    npc_head_caravan_kodo_34577() : CreatureScript("npc_head_caravan_kodo_34577") { }

    struct npc_head_caravan_kodo_34577AI : public ScriptedAI
    {
        npc_head_caravan_kodo_34577AI(Creature* creature) : ScriptedAI(creature) { }

        uint32      _phase;
        EventMap    _events;
        bool        _IsArrived;
        uint32      _razorCounter;
        ObjectGuid  _playerGUID;
        ObjectGuid  _roccoGUID;
        ObjectGuid  _balgorGUID;
        ObjectGuid  _kodo2GUID;
        ObjectGuid  _hotseatGUID;

        void Reset() override
        {
            _phase = 1;
            _events.Reset();
            _IsArrived = false;
            _razorCounter = 0;
            _playerGUID = ObjectGuid::Empty;
            _roccoGUID = ObjectGuid::Empty;
            _balgorGUID = ObjectGuid::Empty;
            _kodo2GUID = ObjectGuid::Empty;
            _hotseatGUID = ObjectGuid::Empty;
            _events.RescheduleEvent(EVENT_MOVE_TO_POINT_1, 1000);
            _events.RescheduleEvent(EVENT_MASTER_RESET, 180000);
            me->SetUInt32Value(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_NPC | UNIT_FLAG_IMMUNE_TO_PC);
            me->setActive(true);
        }

        void IsSummonedBy(Unit* summoner) override
        {
            if (summoner->GetEntry() == NPC_ROCCO_WHIPSHANK)
                _roccoGUID = summoner->GetGUID();
        }

        void MovementInform(uint32 type, uint32 id) override
        {
            if (type == POINT_MOTION_TYPE)
            {
                switch (id)
                {
                case 21:
                    _events.ScheduleEvent(EVENT_MOVE_TO_POINT_2, 100);
                    break;
                case 22:
                    _events.ScheduleEvent(EVENT_MOVE_TO_STARTPOINT, 100);
                    break;
                }
            }
            else if (type == WAYPOINT_MOTION_TYPE)
            {
                switch (id)
                {
                case 1:
                    _events.ScheduleEvent(EVENT_MOVEPOINT_1, 10); // Greetings();
                    break;
                case 2:
                    if (Creature* rocco = ObjectAccessor::GetCreature(*me, _roccoGUID))
                        rocco->AI()->DoAction(ACTION_RESET);
                    break;
                case 20:
                    _events.ScheduleEvent(EVENT_MOVEPOINT_4, 10); // MadeIt();
                    break;
                case 24:
                    _events.ScheduleEvent(EVENT_MOVEPOINT_5, 10); // CaravanArrived();
                    break;
                }
                if (id > 5 && id < 20)
                    _events.ScheduleEvent(EVENT_PERFORM_ATTACK, 10); // CreateAttack();
            }
        }

        void JustSummoned(Creature* summon) override
        {
            switch (summon->GetEntry())
            {
            case NPC_BALGOR_WHIPSHANK:
                _balgorGUID = summon->GetGUID();
                //summon->AI()->SetGUID(me->GetGUID(), me->GetEntry());
                break;
            case NPC_CROSSROADS_CARAVAN_KODO:
                _kodo2GUID = summon->GetGUID();
                _events.ScheduleEvent(EVENT_INIT_SUMMON, 1000);
                break;
            case NPC_THE_HOTSEAT:
                _hotseatGUID = summon->GetGUID();
                break;
            case NPC_BURNING_BLADE_MOUNT:
                _razorCounter += 2;
                break;
            }
        }

        void SummonedCreatureDespawn(Creature* summon) override
        {
            switch (summon->GetEntry())
            {
            case NPC_BURNING_BLADE_MOUNT:
            case NPC_BURNING_BLADE_RAIDER:
                _razorCounter -= 1;
                break;
            }
        }

        void SummonedCreatureDies(Creature* summon, Unit* killer) override
        {
            switch (summon->GetEntry())
            {
            case NPC_BURNING_BLADE_MOUNT:
            case NPC_BURNING_BLADE_RAIDER:
                _razorCounter -= 1;
                break;
            }
        }

        ObjectGuid GetGUID(int32 id) const override
        {
            switch (id)
            {
            case NPC_CROSSROADS_CARAVAN_KODO:  // ask from 34593/34594
                return _kodo2GUID;
            case NPC_THE_HOTSEAT:       // ask from 34593/34594 
                return _hotseatGUID;
            case PLAYER_GUID:           // ask from 34593/34594 
                return _playerGUID;
            case NPC_HEAD_CARAVAN_KODO: // ask from 34593/34594 
                return me->GetGUID();
            }
            return ObjectGuid::Empty;
        }

        void DoAction(int32 param) override
        {
            switch (param)
            {
            case ACTION_START_CARAVAN: // from hotSeat
                _events.ScheduleEvent(EVENT_START_CARAVAN, 2000);
                break;
            }
        }

        void PassengerBoarded(Unit* passenger, int8 seatId, bool apply) override
        {
            if (apply)
            {
                if (Player* player = passenger->ToPlayer())
                {
                    // step 1: when player klick on kodo, he will take place on seat 1.
                    // but we need gunner between player and seat 1.. and the gunner give us possibility to sit backward on kodo
                    _playerGUID = passenger->GetGUID();
                    if (!_hotseatGUID)
                        passenger->ExitVehicle();
                }
                else if (passenger->GetEntry() == NPC_BALGOR_WHIPSHANK)
                {
                    _balgorGUID = passenger->GetGUID();
                }
                else if (passenger->GetEntry() == NPC_THE_HOTSEAT)
                {
                    // step 3: the gunner is mount seat 1, now we mount the player
                    _hotseatGUID = passenger->GetGUID();
                    _events.RescheduleEvent(EVENT_MOUNT_PLAYER, 10);
                }
            }
            else
            {
                if (passenger->GetEntry() == NPC_THE_HOTSEAT && !_IsArrived)
                {
                    if (Creature* kodo2 = ObjectAccessor::GetCreature(*me, _kodo2GUID))
                        kodo2->DespawnOrUnsummon(10);
                    me->DespawnOrUnsummon(10);
                }
                else if (passenger->ToPlayer())
                {
                    // step 2: when player leave and the gunner is not present, we use this as starting event
                    // we spawn the gunner and let then player mount the gunner
                    if (!_hotseatGUID)
                    {
                        _playerGUID = passenger->GetGUID();
                        _events.RescheduleEvent(EVENT_MOUNT_HOTSEAT, 10);
                    }
                }
            }
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_MASTER_RESET:
                {
                    me->DespawnOrUnsummon(10);
                    break;
                }
                case EVENT_MOVE_TO_POINT_1:
                {
                    me->GetMotionMaster()->MovePoint(21, 201.97f, -2954.85f, 93.41f, true);
                    break;
                }
                case EVENT_MOVE_TO_POINT_2:
                {
                    me->CastSpell(202.7f, -2970.6f, 91.85f, 1.22f, SPELL_CALL_A_PACK_KODO2, true);
                    me->GetMotionMaster()->MovePoint(22, 205.1f, -2935.985f, 92.66f, true);
                    break;
                }
                case EVENT_MOVE_TO_STARTPOINT:
                {
                    // we are now ready for walk
                    break;
                }
                case EVENT_SUMMON_CARAVAN:
                {
                    printf("");
                    break;
                }
                case EVENT_INIT_SUMMON: // kodo2
                {
                    if (Creature* kodo2 = ObjectAccessor::GetCreature(*me, _kodo2GUID))
                    {
                        kodo2->GetMotionMaster()->MoveFollow(me, 8.0f, 3.14f);
                        kodo2->SetFlag(UNIT_FIELD_FLAGS, UNIT_FLAG_IMMUNE_TO_PC || UNIT_FLAG_IMMUNE_TO_NPC);
                    }
                    break;
                }
                case EVENT_START_CARAVAN:
                {
                    me->GetMotionMaster()->MovePath(NPC_HEAD_CARAVAN_KODO, false);
                    break;
                }
                case EVENT_MOUNT_PLAYER:
                {
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        if (Creature* hotseat = ObjectAccessor::GetCreature(*me, _hotseatGUID))
                            player->EnterVehicle(hotseat, 0);
                    break;
                }
                case EVENT_MOUNT_HOTSEAT:
                {
                    TempSummon* accessory = me->SummonCreature(NPC_THE_HOTSEAT, *me, TempSummonType::TEMPSUMMON_CORPSE_TIMED_DESPAWN, 180000);
                    accessory->AddUnitTypeMask(UNIT_MASK_ACCESSORY);
                    me->HandleSpellClick(accessory, 1);
                    break;
                }
                case EVENT_MOVEPOINT_1: // Greetings()
                {
                    // Music ID : 24676
                    if (Creature* rocco = ObjectAccessor::GetCreature(*me, _roccoGUID))
                        rocco->AI()->Talk(1);
                    _events.ScheduleEvent(EVENT_MOVEPOINT_2, 4000);
                    break;
                }
                case EVENT_MOVEPOINT_2: // Warning1()
                {
                    if (Creature* balgor = ObjectAccessor::GetCreature(*me, _balgorGUID))
                        balgor->AI()->Talk(1);
                    _events.ScheduleEvent(EVENT_MOVEPOINT_3, 4000);
                    break;
                }
                case EVENT_MOVEPOINT_3: // Warning2()
                {
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        if (Creature* hotseat = ObjectAccessor::GetCreature(*me, _hotseatGUID))
                            hotseat->AI()->Talk(0, player);
                    if (Creature* rocco = ObjectAccessor::GetCreature(*me, _roccoGUID))
                        rocco->AI()->DoAction(ACTION_RESET);
                    break;
                }
                case EVENT_MOVEPOINT_4: // MadeIt()
                {
                    if (Creature* balgor = ObjectAccessor::GetCreature(*me, _balgorGUID))
                        balgor->AI()->Talk(2, me);
                    break;
                }
                case EVENT_MOVEPOINT_5: // CaravanArrived()
                {
                    _IsArrived = true;
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                    {
                        player->ExitVehicle();
                        player->KilledMonsterCredit(NPC_CROSSROADS_CARAVAN_DELIVERY_KILL_CREDIT);
                        player->CompleteQuest(QUEST_CROSSROADS_CARAVAN_DELIVERY);
                    }
                    if (Creature* hotseat = ObjectAccessor::GetCreature(*me, _hotseatGUID))
                        hotseat->DespawnOrUnsummon(10);
                    if (Creature* kodo2 = ObjectAccessor::GetCreature(*me, _kodo2GUID))
                        kodo2->DespawnOrUnsummon(10);
                    // Sound ID : 1731
                    me->DespawnOrUnsummon(10);
                    break;
                }
                case EVENT_PERFORM_ATTACK: // CreateAttack()
                {
                    std::list<Creature*> cList = me->FindNearestCreatures(34594, 50.0f);
                    for (uint32 i = cList.size(); i < 5; i++)
                        me->CastSpell(me, SPELL_SUMMON_BURNING_BLADE);
                    break;
                }
                }
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_head_caravan_kodo_34577AI(creature);
    }
};

// 34582
class vehicle_hotseat_34582 : public VehicleScript
{
public:
    vehicle_hotseat_34582() : VehicleScript("vehicle_hotseat_34582") { }

    void OnAddPassenger(Vehicle* veh, Unit* passenger, int8 /*seatId*/) override
    {
        passenger->SetFacingTo(3.14f);
        veh->RelocatePassengers();
        if (Creature* hotseat = veh->GetBase()->ToCreature())
            hotseat->AI()->SetGUID(passenger->GetGUID(), PLAYER_GUID);
    }

    void OnRemovePassenger(Vehicle* veh, Unit* passenger) override
    {
        passenger->RemoveAura(SPELL_RIDE_CARAVAN_KODO);
        if (Creature* hotseat = veh->GetBase()->ToCreature())
            hotseat->DespawnOrUnsummon(100);
    }

    struct vehicle_hotseat_34582AI : public ScriptedAI
    {
        vehicle_hotseat_34582AI(Creature* creature) : ScriptedAI(creature) { }

        EventMap    _events;
        ObjectGuid  _playerGUID;
        ObjectGuid  _kodo1GUID;
        ObjectGuid  _kodo2GUID;

        void Reset() override
        {
            _playerGUID = ObjectGuid::Empty;
            _kodo1GUID = ObjectGuid::Empty;
            _kodo2GUID = ObjectGuid::Empty;
            _events.RescheduleEvent(EVENT_CHECK_PLAYER, 5000);
        }

        void IsSummonedBy(Unit* summoner) override
        {
            if (summoner->GetEntry() == NPC_HEAD_CARAVAN_KODO)
            {
                _kodo1GUID = summoner->GetGUID();
                _kodo2GUID = summoner->GetAI()->GetGUID(NPC_CROSSROADS_CARAVAN_KODO);
            }
        }

        void SetGUID(ObjectGuid guid, int32 id) override
        {
            switch (id)
            {
            case PLAYER_GUID: // self : OnAddPassenger
                _playerGUID = guid;
                if (Creature* kodo1 = ObjectAccessor::GetCreature(*me, _kodo1GUID))
                    kodo1->AI()->DoAction(ACTION_START_CARAVAN);
                _events.ScheduleEvent(EVENT_INIT_SUMMON, 500);
                break;
            }
        }

        ObjectGuid GetGUID(int32 id) const override
        {
            switch (id)
            {
            case NPC_THE_HOTSEAT:       // ask from 34593/34594 
                return me->GetGUID();
            case PLAYER_GUID:           // ask from 34593/34594  
                return _playerGUID;
            case NPC_HEAD_CARAVAN_KODO: // ask from 34593/34594  
                return _kodo1GUID;
            }
            return ObjectGuid::Empty;
        }

        void DoAction(int32 param) override
        {
            switch (param)
            {
            case 11: // << todo: attack from blade raider
                me->SetHealth(me->GetHealth() - 1);
                break;
            }
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_CHECK_PLAYER:
                {
                    bool ok = false;
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        if (Creature* kodo1 = ObjectAccessor::GetCreature(*me, _kodo1GUID))
                            if (player->GetQuestStatus(QUEST_CROSSROADS_CARAVAN_DELIVERY) == QUEST_STATUS_INCOMPLETE)
                                ok = true;

                    if (!ok)
                        me->DespawnOrUnsummon(10);

                    _events.RescheduleEvent(EVENT_CHECK_PLAYER, 5000);
                    break;
                }
                case EVENT_INIT_SUMMON:
                {
                    // place for insert visual swap for weapon: Halga's Street Sweeper
                    break;
                }
                }
            }
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new vehicle_hotseat_34582AI(creature);
    }
};

// 34593 Reittier der Brennenden Klinge
class vehicle_burning_blade_mount_34593 : public VehicleScript 
{
public:
    vehicle_burning_blade_mount_34593() : VehicleScript("vehicle_burning_blade_mount_34593") { }

    void OnAddPassenger(Vehicle* veh, Unit* passenger, int8 /*seatId*/) override
    {
        if (Creature* mount = veh->GetBase()->ToCreature())
            if (Creature* kodo2 = mount->FindNearestCreature(NPC_CROSSROADS_CARAVAN_KODO, 50.0f))
                mount->GetMotionMaster()->MoveFollow(kodo2, frand(0.5f, 2.0f), frand(0.0f, 6.28f));
    }

    void OnRemovePassenger(Vehicle* veh, Unit* passenger) override
    {
        if (Creature* mount = veh->GetBase()->ToCreature())
        {
            mount->SetSpeedRate(MOVE_WALK, 0.4f);
            mount->GetMotionMaster()->MoveConfused();
            mount->DespawnOrUnsummon(10000);
        }
    }

    struct vehicle_burning_blade_mountAI : public ScriptedAI
    {
        vehicle_burning_blade_mountAI(Creature* creature) : ScriptedAI(creature) { }

        EventMap    _events;
        ObjectGuid  _playerGUID;
        ObjectGuid  _kodo1GUID;
        ObjectGuid  _kodo2GUID;
        ObjectGuid  _hotseatGUID;
        float       _angle;
        float       _dist;

        void Reset() override
        {
            _playerGUID = ObjectGuid::Empty;
            _kodo1GUID = ObjectGuid::Empty;
            _kodo2GUID = ObjectGuid::Empty;
            _hotseatGUID = ObjectGuid::Empty;
            _dist = frand(8.0f, 12.0f);
            _angle = RAND(1.2f, 5.08f);
            float x, y, z;
            if (Creature* kodo1 = ObjectAccessor::GetCreature(*me, _kodo1GUID))
            {
                me->GetNearPoint(kodo1, x, y, z, 20.0f, _dist, _angle); // the razor is spawn on kodo1 position 
                me->NearTeleportTo(x, y, z, 0.0f); // and we teleport him to start position, a bit behind the player visual point..
            }
            me->setActive(true);
            _events.RescheduleEvent(EVENT_CHECK_PLAYER, 5000);
            _events.ScheduleEvent(EVENT_FOLLOW_PLAYER, 1000);
        }

        void IsSummonedBy(Unit* summoner) override
        {
            if (summoner->GetEntry() == NPC_THE_HOTSEAT)
            {
                _hotseatGUID = summoner->GetGUID();
                _playerGUID = summoner->GetAI()->GetGUID(PLAYER_GUID);
                _kodo1GUID = summoner->GetAI()->GetGUID(NPC_HEAD_CARAVAN_KODO);
                _kodo2GUID = summoner->GetAI()->GetGUID(NPC_CROSSROADS_CARAVAN_KODO);
            }
        }

        void SetGUID(ObjectGuid guid, int32 id = 0) override
        {
            switch (id)
            {
            case PLAYER_GUID:
                _playerGUID = guid;
                break;
            }
        }

        void PassengerBoarded(Unit* passenger, int8 seatId, bool apply) override
        {
            if (!apply)
                passenger->GetAI()->DoAction(ACTION_MOVE_FOLLOW);
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_CHECK_PLAYER:
                {
                    bool ok = false;
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        if (Creature* kodo1 = ObjectAccessor::GetCreature(*me, _kodo1GUID))
                            if (player->GetQuestStatus(QUEST_CROSSROADS_CARAVAN_DELIVERY) == QUEST_STATUS_INCOMPLETE)
                                if (player->GetPosition().GetExactDist(me->GetPosition()) < 40.0f)
                                    ok = true;

                    if (!ok)
                        me->DespawnOrUnsummon(10);

                    _events.RescheduleEvent(EVENT_CHECK_PLAYER, 5000);
                    break;
                }
                case EVENT_FOLLOW_PLAYER:
                {
                    if (Creature* kodo1 = ObjectAccessor::GetCreature(*me, _kodo1GUID))
                    {
                        if (Creature* kodo2 = ObjectAccessor::GetCreature(*me, _kodo2GUID))
                        {
                            if (urand(0, 100) < 30)
                                me->GetMotionMaster()->MoveFollow(kodo1, _dist, _angle);
                            else
                                me->GetMotionMaster()->MoveFollow(kodo2, _dist, _angle);
                        }
                    }
                    break;
                }
                }
            }

            if (!UpdateVictim())
                return;

            DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new vehicle_burning_blade_mountAI(creature);
    }
};

// 34594
class npc_burning_blade_raider_34594 : public CreatureScript
{
public:
    npc_burning_blade_raider_34594() : CreatureScript("npc_burning_blade_raider_34594") { }

    struct npc_burning_blade_raider_34594AI : public ScriptedAI
    {
        npc_burning_blade_raider_34594AI(Creature* creature) : ScriptedAI(creature) { }

        EventMap    _events;
        ObjectGuid  _playerGUID;
        ObjectGuid  _kodo1GUID;
        ObjectGuid  _kodo2GUID;
        ObjectGuid  _hotseatGUID;
        float       _angle;
        float       _dist;

        void Reset() override
        {
            _playerGUID = ObjectGuid::Empty;
            _kodo1GUID = ObjectGuid::Empty;
            _kodo2GUID = ObjectGuid::Empty;
            _hotseatGUID = ObjectGuid::Empty;
            _dist = frand(8.0f, 12.0f);
            _angle = RAND(1.2f, 5.08f);
            float x, y, z;
            if (Creature* kodo1 = ObjectAccessor::GetCreature(*me, _kodo1GUID))
            {
                me->GetNearPoint(kodo1, x, y, z, 20.0f, _dist, _angle); // the razor is spawn on kodo1 position 
            }
            me->setActive(true);
            _events.RescheduleEvent(EVENT_CHECK_PLAYER, 5000);
        }

        void IsSummonedBy(Unit* summoner) override
        {
            if (summoner->GetEntry() == NPC_THE_HOTSEAT)
            {
                _hotseatGUID = summoner->GetGUID();
                _playerGUID = summoner->GetAI()->GetGUID(PLAYER_GUID);
                _kodo1GUID = summoner->GetAI()->GetGUID(NPC_HEAD_CARAVAN_KODO);
                _kodo2GUID = summoner->GetAI()->GetGUID(NPC_CROSSROADS_CARAVAN_KODO);
            }
        }

        void DoAction(int32 param) override
        {
            _events.ScheduleEvent(EVENT_FOLLOW_PLAYER, 1000);
        }

        void UpdateAI(uint32 diff) override
        {
            _events.Update(diff);

            while (uint32 eventId = _events.ExecuteEvent())
            {
                switch (eventId)
                {
                case EVENT_CHECK_PLAYER:
                {
                    bool ok = false;
                    if (Player* player = ObjectAccessor::GetPlayer(*me, _playerGUID))
                        if (Creature* kodo1 = ObjectAccessor::GetCreature(*me, _kodo1GUID))
                            if (player->GetQuestStatus(QUEST_CROSSROADS_CARAVAN_DELIVERY) == QUEST_STATUS_INCOMPLETE)
                                if (player->GetPosition().GetExactDist(me->GetPosition()) < 40.0f)
                                    ok = true;

                    if (!ok)
                        me->DespawnOrUnsummon(10);

                    _events.RescheduleEvent(EVENT_CHECK_PLAYER, 5000);
                    break;
                }
                case EVENT_FOLLOW_PLAYER:
                {
                    if (Creature* kodo1 = ObjectAccessor::GetCreature(*me, _kodo1GUID))
                    {
                        if (Creature* kodo2 = ObjectAccessor::GetCreature(*me, _kodo2GUID))
                        {
                            if (urand(0, 100) < 30)
                                me->GetMotionMaster()->MoveFollow(kodo1, _dist, _angle);
                            else
                                me->GetMotionMaster()->MoveFollow(kodo2, _dist, _angle);
                        }
                    }
                    break;
                }
                }
            }

            if (!UpdateVictim())
                return;

            DoMeleeAttackIfReady();
        }
    };

    CreatureAI* GetAI(Creature* creature) const override
    {
        return new npc_burning_blade_raider_34594AI(creature);
    }
};

/* ################################################# */

// 5482 AreaTrigger - quest 14066
class at_caravan_scene_5482 : public AreaTriggerScript
{
public:
    at_caravan_scene_5482() : AreaTriggerScript("at_caravan_scene_5482") { }

    bool OnTrigger(Player* player, const AreaTriggerEntry* at, bool /*entered*/) override
    {
        if ((player->GetQuestStatus(QUEST_INVESTIGATE_THE_WRECKAGE) == QUEST_STATUS_INCOMPLETE))
        {
            player->KilledMonsterCredit(NPC_QUESTGIVER);
        }

        return false;
    }
};

// 5483 AreaTrigger - quest 869
class at_follow_the_tracks_5483 : public AreaTriggerScript
{
public:
    at_follow_the_tracks_5483() : AreaTriggerScript("at_follow_the_tracks_5483") { }

    bool OnTrigger(Player* player, const AreaTriggerEntry* at, bool /*entered*/) override
    {
        if ((player->GetQuestStatus(QUEST_TO_TRACK_A_THIEF) == QUEST_STATUS_INCOMPLETE))
        {
            player->KilledMonsterCredit(NPC_QUESTGIVER);
        }

        return false;
    }
};


void AddSC_zone_northern_barrens()
{
    new npc_trapped_wolf_34285();
    new go_wolf_chains();
    new npc_halga_bloodeye_34258();
    new npc_lead_caravan_kodo_34430();
    new vehicle_riding_shotgun_34438();
    new npc_razormane_raider_34487();
    new spell_giddyap_65493();

    new npc_razormane_pillager_34503();
    new npc_hogtied_razormane_34514();
    new at_groldoms_farm_5465();
    new npc_togrik_34513();
    new npc_captured_razormane_34523();
    new npc_groldom_kodo_34547();
    new npc_fez_34543();
    new npc_rocco_whipshank_34578();
    new npc_head_caravan_kodo_34577();
    new vehicle_hotseat_34582();
    new vehicle_burning_blade_mount_34593();
    new at_caravan_scene_5482();
    new at_follow_the_tracks_5483();

}
